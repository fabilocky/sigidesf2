<?php
namespace Sistema\UserBundle\Services;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class SecurityRole
{
    private $container;
    private $em;

    public function __construct($container, $em)
    {
        $this->container = $container;
        $this->em = $em;
    }

    /**
     * Controla los roles del usuario con los roles del menu.
     */
    public function controlRolesUser()
    {
        $sc = $this->container->get('security.context');
        $user = $sc->getToken()->getUser();
        if (false === $this->container->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            $menues = $this->em->getRepository('SistemaUserBundle:Menu')->GetMenuByUser($user);
        } else {
            $menues = $this->em->getRepository('SistemaUserBundle:Menu')->GetMenuBySuperuser();
        }
        $route = $this->container->get('request')->get('_route');
        if (is_null($menues)) {
            $control = false;
        } else {
            foreach ($menues as $menu) {                
                $pos = strpos($route, $menu['ruta']);
                if (false === $pos) {
                    $control = false;
                } else {
                    $control = true;
                    break;
                }
            }
        }
        if (false === $control) {
            throw new AccessDeniedException();
        }

        return true;
    }
}
