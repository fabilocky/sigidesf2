<?php

namespace Sistema\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/hello/{name}")
     * @Template()
     */
    public function indexAction($name)
    {        
        return array('name' => $name);
    }
    
    public function actualizarusuarios(){
        $em = $this->getDoctrine()->getManager();
        $config = $em->getRepository('SistemaAdminBundle:Config')->findAll();
        $connectionFactory = $this->container->get('doctrine.dbal.connection_factory');
        $connection = $connectionFactory->createConnection(
            array('pdo' => new \PDO("mysql:host=".$config[0]->getHost().";dbname=".$config[0]->getDbname(), $config[0]->getUser(), $config[0]->getPass()))
        );
        $query = $connection->executeQuery("SELECT * FROM ".$config[0]->getTabla());
        $results = $query->fetchAll();
        foreach ($results as $user){            
            $entity = $em->getRepository('SistemaUserBundle:User')->findOneByUsername($user["usuario"]);
            if (!$entity){
                $usuario = new \Sistema\UserBundle\Entity\User();
                $usuario->setUsername($user["usuario"]);
                $usuario->setIsActive(true);
                $usuario->setSalt(md5(time()));
                $usuario->setPassword($user["usuario"]);
                $localidad = $em->getRepository('SistemaAdminBundle:Localidad')->findOneByName($user["localidad"]);
                $usuario->setLocalidad($localidad);
                $encoder = $this->container->get('security.encoder_factory')->getEncoder($usuario);
                $passwordCodificado = $encoder->encodePassword(
                    $usuario->getPassword(),
                    $usuario->getSalt()
                );
                $usuario->setPassword($passwordCodificado);
                // Guardar el nuevo usuario en la base de datos
                $em->persist($usuario);
                $em->flush();
            } else {
                $entity->setSalt(md5(time()));
                $entity->setPassword($user["usuario"]);
                $encoder = $this->container->get('security.encoder_factory')->getEncoder($entity);
                $passwordCodificado = $encoder->encodePassword(
                    $entity->getPassword(),
                    $entity->getSalt()
                );
                $entity->setPassword($passwordCodificado);
            }
        }
    }
}
