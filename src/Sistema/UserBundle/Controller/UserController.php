<?php

namespace Sistema\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\UserBundle\Entity\User;
use Sistema\UserBundle\Form\UserType;
use Sistema\UserBundle\Form\UserEditType;
use Sistema\UserBundle\Form\UserEditPerfilType;
use Sistema\UserBundle\Form\UserFilterType;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * User controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/user")
 */
class UserController extends Controller
{
    /**
     * Lists all User entities.
     *
     * @Route("/admin", name="user")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->get('security_role')->controlRolesUser();
        list($filterForm, $queryBuilder) = $this->filter();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder,
            $this->get('request')->query->get('page', 1),
            10
        );

        return array(
            'entities'   => $pagination,
            'filterForm' => $filterForm->createView(),
        );
    }

    /**
    * Process filter request.
    *
    * @return array
    */
    protected function filter()
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        $filterForm = $this->createFilterForm();
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository('SistemaUserBundle:User')
            ->createQueryBuilder('a')
            ->orderBy('a.id', 'DESC')
        ;
        // Bind values from the request
        $filterForm->handleRequest($request);
        // Reset filter
        if ($filterForm->get('reset')->isClicked()) {
            $session->remove('UserControllerFilter');
            $filterForm = $this->createFilterForm();
        }

        // Filter action
        if ($filterForm->get('filter')->isClicked()) {
            if ($filterForm->isValid()) {                
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('UserControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('UserControllerFilter')) {
                $filterData = $session->get('UserControllerFilter');
                $filterForm = $this->createFilterForm($filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }
    /**
    * Create filter form.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createFilterForm($filterData = null)
    {
        $form = $this->createForm(new UserFilterType(), $filterData, array(
            'action' => $this->generateUrl('user'),
            'method' => 'GET',
        ));

        $form
            ->add('filter', 'submit', array(
                'translation_domain' => 'MWSimpleAdminCrudBundle',
                'label'              => 'views.index.filter',
                'attr'               => array('class' => 'btn btn-success'),
            ))
            ->add('reset', 'submit', array(
                'translation_domain' => 'MWSimpleAdminCrudBundle',
                'label'              => 'views.index.reset',
                'attr'               => array('class' => 'btn btn-danger'),
            ))
        ;

        return $form;
    }
    /**
     * Creates a new User entity.
     *
     * @Route("/admin", name="user_create")
     * @Method("POST")
     * @Template("SistemaUserBundle:User:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $this->get('security_role')->controlRolesUser();
        $entity = new User();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $this->setSecurePassword($entity);

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            $nextAction = $form->get('saveAndAdd')->isClicked()
                    ? $this->generateUrl('user_new')
                    : $this->generateUrl('user_show', array('id' => $entity->getId()));

            return $this->redirect($nextAction);

        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a User entity.
    *
    * @param User $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(User $entity)
    {
        $form = $this->createForm(new UserType(), $entity, array(
            'action' => $this->generateUrl('user_create'),
            'method' => 'POST',
        ));

        $form
            ->add(
                'save', 'submit', array(
                'translation_domain' => 'MWSimpleAdminCrudBundle',
                'label'              => 'views.new.save',
                'attr'               => array('class' => 'col-lg-2 btn btn-success')
                )
            )
            ->add(
                'saveAndAdd', 'submit', array(
                'translation_domain' => 'MWSimpleAdminCrudBundle',
                'label'              => 'views.new.saveAndAdd',
                'attr'               => array('class' => 'col-lg-2 btn btn-primary')
                )
            )
        ;

        return $form;
    }

    /**
     * Displays a form to create a new User entity.
     *
     * @Route("/admin/new", name="user_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->get('security_role')->controlRolesUser();
        $entity = new User();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a User entity.
     *
     * @Route("/admin/{id}", name="user_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaUserBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/admin/{id}/edit", name="user_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaUserBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a User entity.
    *
    * @param User $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(User $entity)
    {
        $form = $this->createForm(new UserEditType(), $entity, array(
            'action' => $this->generateUrl('user_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form
            ->add(
                'save', 'submit', array(
                'translation_domain' => 'MWSimpleAdminCrudBundle',
                'label'              => 'views.new.save',
                'attr'               => array('class' => 'col-lg-2 btn btn-success')
                )
            )
            ->add(
                'saveAndAdd', 'submit', array(
                'translation_domain' => 'MWSimpleAdminCrudBundle',
                'label'              => 'views.new.saveAndAdd',
                'attr'               => array('class' => 'col-lg-2 btn btn-primary')
                )
            )
        ;

        return $form;
    }
    /**
     * Edits an existing User entity.
     *
     * @Route("/admin/{id}", name="user_update")
     * @Method("PUT")
     * @Template("SistemaUserBundle:User:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $this->get('security_role')->controlRolesUser();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaUserBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);

        $current_pass = $entity->getPassword();
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            if ($entity->getPassword() == null ) {
                $entity->setPassword($current_pass);
            }

            if ($current_pass != $entity->getPassword()) {
                $this->setSecurePassword($entity);
            }
            
            $sc = $this->container->get('security.context');
            $user = $sc->getToken()->getUser();
            
            $token = new UsernamePasswordToken($entity, null, 'main', $entity->getRoles());
            $this->get('security.token_storage')->setToken($token);
            
            $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
            $this->get('security.token_storage')->setToken($token);            
            

            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $editForm->get('saveAndAdd')->isClicked()
                        ? $this->generateUrl('user_new')
                        : $this->generateUrl('user_show', array('id' => $id));

            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a User entity.
     *
     * @Route("/admin/{id}", name="user_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $this->get('security_role')->controlRolesUser();
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SistemaUserBundle:User')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find User entity.');
            }

            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.delete.success');
        }

        return $this->redirect($this->generateUrl('user'));
    }

    /**
     * Creates a form to delete a User entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        $mensaje = $this->get('translator')->trans('views.recordactions.confirm', array(), 'MWSimpleAdminCrudBundle');
        $onclick = 'return confirm("'.$mensaje.'");';

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('user_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array(
                'translation_domain' => 'MWSimpleAdminCrudBundle',
                'label'              => 'views.recordactions.delete',
                'attr'               => array(
                    'class'   => 'btn btn-danger',
                    'onclick' => $onclick,
                )
            ))
            ->getForm()
        ;
    }

    private function setSecurePassword($entity)
    {
        $factory = $this->get('security.encoder_factory');
        $entity->setSalt(md5(time()));
        $encoder = $factory->getEncoder($entity);
        $password = $encoder->encodePassword($entity->getPassword(), $entity->getSalt());
        $entity->setPassword($password);
    }
    
    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/perfil/edit", name="user_perfil_edit")
     * @Method("GET")
     * @Template("SistemaUserBundle:User:editPerfil.html.twig")
     */
    public function editPerfilAction()
    {
        //$this->get('security_role')->controlRolesUser();
        
        $sc = $this->container->get('security.context');
        $entity = $sc->getToken()->getUser();
        

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $editForm = $this->createForm(new UserEditPerfilType(), $entity, array(
            'action' => $this->generateUrl('user_perfil_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $editForm
            ->add(
                'save', 'submit', array(
                'translation_domain' => 'MWSimpleAdminCrudBundle',
                'label'              => 'views.new.save',
                'attr'               => array('class' => 'col-lg-2 btn btn-success')
                )
            )            
        ;

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),            
        );
    }
    
    /**
     * Edits an existing User entity.
     *
     * @Route("/perfil/edit/{id}", name="user_perfil_update")
     * @Method("PUT")
     * @Template("SistemaUserBundle:User:editPerfil.html.twig")
     */
    public function updatePerfilAction(Request $request, $id)
    {        
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SistemaUserBundle:User')->find($id);
        $activo = $entity->getisActive();
        $roles = $entity->getRoles();
        
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);

        $current_pass = $entity->getPassword();
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            if ($entity->getPassword() == null ) {
                $entity->setPassword($current_pass);
            }

            if ($current_pass != $entity->getPassword()) {
                $this->setSecurePassword($entity);
            }
            
            
            foreach ($roles as $role){
            $entity->addRole($role);
            }            
            $entity->setisActive($activo);
            
            $sc = $this->container->get('security.context');
            $user = $sc->getToken()->getUser();
            
            $token = new UsernamePasswordToken($entity, null, 'main', $entity->getRoles());
            $this->get('security.token_storage')->setToken($token);
            
            $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
            $this->get('security.token_storage')->setToken($token);            
            

            
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $this->generateUrl('user_perfil_edit');

            return $this->redirect($nextAction);
        }
        
        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
}
