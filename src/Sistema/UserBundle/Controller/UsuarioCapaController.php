<?php

namespace Sistema\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\UserBundle\Entity\UsuarioCapa;
use Sistema\UserBundle\Form\UsuarioCapaType;
use Sistema\UserBundle\Form\UsuarioCapaFilterType;

/**
 * UsuarioCapa controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/usuario/usuariocapa")
 */
class UsuarioCapaController extends Controller
{
    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/UserBundle/Resources/config/UsuarioCapa.yml',
    );

    /**
     * Lists all UsuarioCapa entities.
     *
     * @Route("/", name="admin_usuariocapa")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->config['filterType'] = new UsuarioCapaFilterType();
        $config = $this->getConfig();
        $sc = $this->container->get('security.context');
        $user = $sc->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository("SistemaUserBundle:User")->find($user->getId());
        list($filterForm, $queryBuilder) = $this->filter($config);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder,
            $this->get('request')->query->get('page', 1),
            ($this->container->hasParameter('knp_paginator.page_range')) ? $this->container->getParameter('knp_paginator.page_range'):10
        );
        //remove the form to return to the view
        unset($config['filterType']);

        return array(
        	'config'     => $config,
            'entities'   => $pagination,
            'filterForm' => $filterForm->createView(),
        );
    }

    /**
     * Creates a new UsuarioCapa entity.
     *
     * @Route("/", name="admin_usuariocapa_create")
     * @Method("POST")
     * @Template("SistemaUserBundle:UsuarioCapa:new.html.twig")
     */
    public function createAction()
    {
        $this->config['newType'] = new UsuarioCapaType();
        $config = $this->getConfig();
    	$request = $this->getRequest();
        $entity = new $config['entity']();
        $form   = $this->createCreateForm($config, $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->useACL($entity, 'create');

            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            $nextAction = $form->get('saveAndAdd')->isClicked()
                ? $this->generateUrl($config['new'])
                : $this->generateUrl($config['show'], array('id' => $entity->getId()));
            return $this->redirect($nextAction);

        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config'     => $config,
            'entity'     => $entity,
            'form'       => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new UsuarioCapa entity.
     *
     * @Route("/new", name="admin_usuariocapa_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->config['newType'] = new UsuarioCapaType();
        $config = $this->getConfig();
        $entity = new $config['entity']();
        $sc = $this->container->get('security.context');
        $user = $sc->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository("SistemaUserBundle:User")->findOneByUsername($user->getUsername());
        
        $entity->setUsuario($usuario);
        $form   = $this->createCreateForm($config, $entity);

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config'     => $config,
            'entity'     => $entity,
            'form'       => $form->createView(),
        );
    }

    /**
     * Finds and displays a UsuarioCapa entity.
     *
     * @Route("/{id}", name="admin_usuariocapa_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing UsuarioCapa entity.
     *
     * @Route("/{id}/edit", name="admin_usuariocapa_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->config['editType'] = new UsuarioCapaType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing UsuarioCapa entity.
     *
     * @Route("/{id}", name="admin_usuariocapa_update")
     * @Method("PUT")
     * @Template("SistemaUserBundle:UsuarioCapa:edit.html.twig")
     */
    public function updateAction($id)
    {
        $this->config['editType'] = new UsuarioCapaType();
        $config = $this->getConfig();
    	$request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($id);
        $entity->setEstado("En revision");
        $entity->setInforme("En revision por modificacion");
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find '.$config['entityName'].' entity.');
        }
        $this->useACL($entity, 'update');
        $deleteForm = $this->createDeleteForm($config, $id);
        $editForm = $this->createEditForm($config, $entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $editForm->get('saveAndAdd')->isClicked()
                        ? $this->generateUrl($config['new'])
                        : $this->generateUrl($config['show'], array('id' => $id));
            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config'      => $config,
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a UsuarioCapa entity.
     *
     * @Route("/{id}", name="admin_usuariocapa_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $response = parent::deleteAction($id);

        return $response;
    }
    
    /**
     * Create query.
     * @param string $repository
     * @return Doctrine\ORM\QueryBuilder $queryBuilder
     */
    protected function createQuery($repository)
    {
        $sc = $this->container->get('security.context');
        $user = $sc->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository($repository)
            ->createQueryBuilder('a')
            ->where('a.usuario = :usuario')
            ->setParameter('usuario', $user->getId())
            ->orderBy('a.id', 'DESC')
        ;

        return $queryBuilder;
    }
}