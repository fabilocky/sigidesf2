<?php

namespace Sistema\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\UserBundle\Entity\Menu;
use Sistema\UserBundle\Form\MenuType;
use Sistema\UserBundle\Form\MenuFilterType;
use Symfony\Component\HttpFoundation\Response;

/**
 * Menu controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/menu")
 */
class MenuController extends Controller
{
    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/UserBundle/Resources/config/Menu.yml',
    );

    /**
     * Lists all Menu entities.
     *
     * @Route("/", name="admin_menu")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->config['filterType'] = new MenuFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Menu entity.
     *
     * @Route("/create/", name="admin_menu_create")
     * @Method("POST")
     * @Template("SistemaUserBundle:Menu:new.html.twig")
     */
    public function createAction()
    {
        $this->config['newType'] = new MenuType();
        $config = $this->getConfig();
    	$request = $this->getRequest();
        $entity = new $config['entity']();
        $form   = $this->createCreateForm($config, $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {           
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->useACL($entity, 'create');

            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            $nextAction = $form->get('saveAndAdd')->isClicked()
                ? $this->generateUrl($config['new'])
                : $this->generateUrl($config['show'], array('id' => $entity->getId()));
            return $this->redirect($nextAction);

        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config'     => $config,
            'entity'     => $entity,
            'form'       => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Menu entity.
     *
     * @Route("/new", name="admin_menu_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->config['newType'] = new MenuType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Menu entity.
     *
     * @Route("/{id}", name="admin_menu_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Menu entity.
     *
     * @Route("/{id}/edit", name="admin_menu_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->config['editType'] = new MenuType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Menu entity.
     *
     * @Route("/{id}", name="admin_menu_update")
     * @Method("PUT")
     * @Template("SistemaUserBundle:Menu:edit.html.twig")
     */
    public function updateAction($id)
    {
        $this->config['editType'] = new MenuType();
        $config = $this->getConfig();
    	$request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find '.$config['entityName'].' entity.');
        }
        $this->useACL($entity, 'update');
        $deleteForm = $this->createDeleteForm($config, $id);
        $editForm = $this->createEditForm($config, $entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $editForm->get('saveAndAdd')->isClicked()
                        ? $this->generateUrl($config['new'])
                        : $this->generateUrl($config['show'], array('id' => $id));
            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config'      => $config,
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Menu entity.
     *
     * @Route("/{id}", name="admin_menu_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Autocomplete a Menu entity.
     *
     * @Route("/autocomplete-forms/get-menu_roles", name="Menu_autocomplete_menu_roles")
     */
    public function getAutocompleteRole()
    {
        $options = array(
            'repository' => "SistemaUserBundle:Role",
            'field'      => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }
    
    /**
     * Autocomplete a Menu entity.
     *
     * @Route("/autocomplete-forms/get-ruta", name="Menu_autocomplete_ruta")
     * @Method("GET")
     */
    public function getAutocompleteRuta()
    {            
        $id = $this->getRequest()->get('id');
        $router = $this->get('router');
        $route = $router->match($id)['_route'];        

        return new Response($route);
    }
    
    public function builderAction()
    {
        $em = $this->container->get('doctrine')->getManager();        

        $isAuthenticated = $this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY');

        if ($isAuthenticated != false) {
            if (false === $this->container->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
                $user = $this->container->get('security.context')->getToken()->getUser();
                $entities = $em->getRepository('SistemaUserBundle:Menu')->GetMenuByUser($user);
            } else {
                $entities = $em->getRepository('SistemaUserBundle:Menu')->GetMenuBySuperuser();
            }            
        } 

        return $this->render('SistemaUserBundle:Menu:menu.html.twig',
            array('menues' => $entities)
        );
    }
}