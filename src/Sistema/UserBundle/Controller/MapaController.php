<?php

namespace Sistema\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\UserBundle\Entity\Mapa;
use Sistema\UserBundle\Form\MapaType;
use Sistema\UserBundle\Form\MapaFilterType;
use Symfony\Component\HttpFoundation\Response;

/**
 * Mapa controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/usuario/mapa")
 */
class MapaController extends Controller
{
    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/UserBundle/Resources/config/Mapa.yml',
    );

    /**
     * Lists all Mapa entities.
     *
     * @Route("/", name="admin_mapa")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->config['filterType'] = new MapaFilterType();
        $config = $this->getConfig();
        $sc = $this->container->get('security.context');
        $user = $sc->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository("SistemaUserBundle:User")->find($user->getId());
        $grupos = $usuario->getGrupos();
        $mapasm = array(); //array de mapas de otros usuarios
        foreach ($grupos as $grupo){
            $mapas = $grupo->getMapas();
            foreach ($mapas as $mapa){
                $mapasm[]=$mapa;
            }
        }
        
        list($filterForm, $queryBuilder) = $this->filter($config);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder,
            $this->get('request')->query->get('page', 1),
            ($this->container->hasParameter('knp_paginator.page_range')) ? $this->container->getParameter('knp_paginator.page_range'):10
        );
        //remove the form to return to the view
        unset($config['filterType']);

        return array(
            'config'     => $config,
            'mapas'     => $mapasm,
            'entities'   => $pagination,
            //'filterForm' => $filterForm->createView(),
        );
    }

    /**
     * Creates a new Mapa entity.
     *
     * @Route("/", name="admin_mapa_create")
     * @Method("POST")
     * @Template("SistemaUserBundle:Mapa:new.html.twig")
     */
    public function createAction()
    {
        $this->config['newType'] = new MapaType();
        $config = $this->getConfig();
    	$request = $this->getRequest();
        $entity = new $config['entity']();
        $form   = $this->createCreateForm($config, $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if ($entity->getFileName() == null){
                $sc = $this->container->get('security.context');
                $user = $sc->getToken()->getUser();
                $archivo = fopen('files/mapas/'. $user->getUsername(). "_" .$entity->getNombre(). ".json", "a");
                fclose($archivo);
                $entity->setFileName($user->getUsername(). "_" .$entity->getNombre(). ".json");
                $entity->setUpdatedAt(new \DateTime("now"));
            }            
            $em->persist($entity);
            $em->flush();
            $this->useACL($entity, 'create');

            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            $nextAction = $form->get('saveAndAdd')->isClicked()
                ? $this->generateUrl($config['new'])
                : $this->generateUrl($config['show'], array('id' => $entity->getId()));
            return $this->redirect($nextAction);

        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config'     => $config,
            'entity'     => $entity,
            'form'       => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Mapa entity.
     *
     * @Route("/new", name="admin_mapa_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->config['newType'] = new MapaType();
        $config = $this->getConfig();
        $entity = new $config['entity']();
        $sc = $this->container->get('security.context');
        $user = $sc->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository("SistemaUserBundle:User")->findOneByUsername($user->getUsername());
        
        $entity->setUsuario($usuario);
        $entity->setActivo(true);
        $form   = $this->createCreateForm($config, $entity);

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config'     => $config,
            'entity'     => $entity,
            'form'       => $form->createView(),
        );
    }

    /**
     * Finds and displays a Mapa entity.
     *
     * @Route("/{id}", name="admin_mapa_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Mapa entity.
     *
     * @Route("/{id}/edit", name="admin_mapa_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->config['editType'] = new MapaType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Mapa entity.
     *
     * @Route("/{id}", name="admin_mapa_update")
     * @Method("PUT")
     * @Template("SistemaUserBundle:Mapa:edit.html.twig")
     */
    public function updateAction($id)
    {
        $this->config['editType'] = new MapaType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a Mapa entity.
     *
     * @Route("/{id}", name="admin_mapa_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $response = parent::deleteAction($id);

        return $response;
    }
    
    /**
     * Finds and displays a Presupuesto entity.
     *
     * @Route("/procesarmapa/{id}", name="procesar_mapa")
     * @Method("GET")
     * @Template()
     */
    public function ProcesarmapaAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $mapa = $em->getRepository("SistemaUserBundle:Mapa")->find($id);
        $session = $request->getSession();
 
    // guarda un atributo para reutilizarlo durante una
    // petición posterior del usuario
        $session->set('mapa', $id);
//        if ($mapa->getPermalink()){
//            return $this->redirect($mapa->getPermalink());
//        } else {
            return $this->redirect($this->generateUrl('home'));
//        }
        
    }
    
    /**
     * Displays a form to create a new Capa entity.
     *
     * @Route("/guardar/mapa", name="guardar_mapa")
     * @Method("POST")     
     */
    public function guardarMapaAction() {        
        $em = $this->getDoctrine()->getManager();
        $mapa = $em->getRepository("SistemaUserBundle:Mapa")->find($this->getRequest()->get('id'));
        $open = fopen('files/mapas/'. $mapa->getFileName(), "w+");
        $permalink = $this->getRequest()->get('permalink');
        $mapa->setPermalink($permalink);
        $em->flush();
        fwrite($open, $this->getRequest()->get('features')); 
        fclose($open);
        return new Response("exito");
    }
    
    /**
     * Process filter request.
     * @param array $config
     * @return array
     */
    protected function filter($config)
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        $filterForm = $this->createFilterForm($config);
        $queryBuilder = $this->createQuery($config['repository']);
        // Bind values from the request
        $filterForm->handleRequest($request);
        // Reset filter
        if ($filterForm->get('reset')->isClicked()) {
            $session->remove($config['sessionFilter']);
            $filterForm = $this->createFilterForm($config);
        }

        // Filter action
        if ($filterForm->get('filter')->isClicked()) {
            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set($config['sessionFilter'], $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has($config['sessionFilter'])) {
                $filterData = $session->get($config['sessionFilter']);
                $filterForm = $this->createFilterForm($config, $filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }
    
    /**
     * Create query.
     * @param string $repository
     * @return Doctrine\ORM\QueryBuilder $queryBuilder
     */
    protected function createQuery($repository)
    {
        $sc = $this->container->get('security.context');
        $user = $sc->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository($repository)
            ->createQueryBuilder('a')
            ->where('a.usuario = :usuario')
            ->setParameter('usuario', $user->getId())
            ->orderBy('a.id', 'DESC')
        ;

        return $queryBuilder;
    }
}