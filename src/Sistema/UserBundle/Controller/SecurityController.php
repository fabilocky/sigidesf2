<?php

namespace Sistema\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Security controller.
 *
 * @Route("/")
 *
 */
class SecurityController extends Controller {

    /**
     * Definimos las rutas para el login:
     * @Route("/login", name="login")
     */
    public function loginAction(Request $request) {
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $client_ip = (!empty($_SERVER['REMOTE_ADDR']) ) ?
                    $_SERVER['REMOTE_ADDR'] :
                    ( (!empty($_ENV['REMOTE_ADDR']) ) ?
                            $_ENV['REMOTE_ADDR'] :
                            "unknown" );

// los proxys van añadiendo al final de esta cabecera
// las direcciones ip que van "ocultando". Para localizar la ip real
// del usuario se comienza a mirar por el principio hasta encontrar
// una dirección ip que no sea del rango privado. En caso de no
// encontrarse ninguna se toma como valor el REMOTE_ADDR

            $entries = split('[, ]', $_SERVER['HTTP_X_FORWARDED_FOR']);

            reset($entries);
            while (list(, $entry) = each($entries)) {
                $entry = trim($entry);
                if (preg_match("/^([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)/", $entry, $ip_list)) {
                    $private_ip = array(
                        '/^0\./',
                        '/^127\.0\.0\.1/',
                        '/^192\.168\..*/',
                        '/^172\.((1[6-9])|(2[0-9])|(3[0-1]))\..*/',
                        '/^10\..*/');

                    $found_ip = preg_replace($private_ip, $client_ip, $ip_list[1]);

                    if ($client_ip != $found_ip) {
                        $client_ip = $found_ip;
                        break;
                    }
                }
            }
        } else {
            $client_ip = (!empty($_SERVER['REMOTE_ADDR']) ) ?
                    $_SERVER['REMOTE_ADDR'] :
                    ( (!empty($_ENV['REMOTE_ADDR']) ) ?
                            $_ENV['REMOTE_ADDR'] :
                            "unknown" );
        }
//echo $client_ip;
        $session = $request->getSession();

        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $request->getSession()->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        return $this->render(
                        'SistemaUserBundle:Default:index.html.twig', array(
                    'last_username' => $request->getSession()->get(SecurityContext::LAST_USERNAME),
                    'error' => $error,
                        )
        );
    }

    /**
     * Definimos las rutas para el login:
     * @Route("/checkear", name="checkear")
     */
    public function checkearAction(Request $request) {
        $connectionFactory = $this->container->get('doctrine.dbal.connection_factory');
        $connection = $connectionFactory->createConnection(
                array('pdo' => new \PDO("mysql:host=localhost;dbname=usuarios", "root", "root"))
        );
        $query = $connection->executeQuery('SELECT * FROM usuarios');
        $results = $query->fetchAll();
        var_dump($results);
        die();
    }

    /**
     * Finds and displays a precio Tipo Producto entity.
     *
     * @Route("/check/user", name="ajax_check")
     */
    public function ajaxCheck() {
        $this->actualizarusuarios();
//        $user = $this->getRequest()->get('user');
        $pass = $this->getRequest()->get('pass');
        $passmd5 = md5($pass);
//        $connectionFactory = $this->container->get('doctrine.dbal.connection_factory');
//        $connection = $connectionFactory->createConnection(
//            array('pdo' => new \PDO("mysql:host=localhost;dbname=usuarios", "root", "root"))
//        );
//        $query = $connection->executeQuery("SELECT * FROM usuarios where usuario = '".$user."'");
//        $results = $query->fetchAll();
//        var_dump($pass);
//        var_dump(md5($pass));
//        var_dump($results[0]["pass"]);die();
//        $em = $this->getDoctrine()->getManager();        
//        return new Response($results);
        return new Response($passmd5);
    }

}
