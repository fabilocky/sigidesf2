<?php

namespace Sistema\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\UserBundle\Entity\Grupo;
use Sistema\UserBundle\Form\GrupoType;
use Sistema\UserBundle\Form\GrupoFilterType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Grupo controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/usuario/grupo")
 */
class GrupoController extends Controller
{
    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/UserBundle/Resources/config/Grupo.yml',
    );

    /**
     * Lists all Grupo entities.
     *
     * @Route("/", name="admin_grupo")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->config['filterType'] = new GrupoFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Grupo entity.
     *
     * @Route("/", name="admin_grupo_create")
     * @Method("POST")
     * @Template("SistemaUserBundle:Grupo:new.html.twig")
     */
    public function createAction()
    {
        $this->config['newType'] = new GrupoType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new Grupo entity.
     *
     * @Route("/new", name="admin_grupo_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->config['newType'] = new GrupoType();
        $config = $this->getConfig();
        $entity = new $config['entity']();
        $sc = $this->container->get('security.context');
        $user = $sc->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository("SistemaUserBundle:User")->findOneByUsername($user->getUsername());
        
        $entity->setOwner($usuario);
        $form   = $this->createCreateForm($config, $entity);

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config'     => $config,
            'entity'     => $entity,
            'form'       => $form->createView(),
        );
    }

    /**
     * Finds and displays a Grupo entity.
     *
     * @Route("/{id}", name="admin_grupo_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Grupo entity.
     *
     * @Route("/{id}/edit", name="admin_grupo_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->config['editType'] = new GrupoType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Grupo entity.
     *
     * @Route("/{id}", name="admin_grupo_update")
     * @Method("PUT")
     * @Template("SistemaUserBundle:Grupo:edit.html.twig")
     */
    public function updateAction($id)
    {
        $this->config['editType'] = new GrupoType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a Grupo entity.
     *
     * @Route("/{id}", name="admin_grupo_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Autocomplete a Grupo entity.
     *
     * @Route("/autocomplete-forms/get-owner", name="Grupo_autocomplete_owner")
     */
    public function getAutocompleteUser()
    {
        $options = array(
            'repository' => "SistemaUserBundle:User",
            'field'      => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Grupo entity.
     *
     * @Route("/autocomplete-forms/get-usuarios", name="Grupo_autocomplete_usuarios")
     */
    public function getAutocompleteUserGroup()
    {
        $options = array(
            'repository' => "SistemaUserBundle:User",
            'fielda'      => "username",
            'fieldb'      => "nombre",
        );
        $request = $this->getRequest();
        $term = $request->query->get('q', null);

        $em = $this->getDoctrine()->getManager();

        $qb = $em->getRepository($options['repository'])->createQueryBuilder('a');
        $qb
            ->add('where', "a.".$options['fielda']." LIKE ?1")
            ->add('where', "a.".$options['fieldb']." LIKE ?1")
            ->add('orderBy', "a.".$options['fielda']." ASC")
            ->setParameter(1, "%" . $term . "%")
        ;
        $entities = $qb->getQuery()->getResult();

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id'   => $entity->getId(),
                'text' => $entity->__toString(),
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * Autocomplete a Grupo entity.
     *
     * @Route("/autocomplete-forms/get-mapas", name="Grupo_autocomplete_mapas")
     */
    public function getAutocompleteMapa()
    {
        $options = array(
            'repository' => "SistemaUserBundle:Mapa",
            'field'      => "nombre",
        );
        $sc = $this->container->get('security.context');
        $user = $sc->getToken()->getUser();
        
        $request = $this->getRequest();
        $term = $request->query->get('q', null);

        $em = $this->getDoctrine()->getManager();
        
        $queryBuilder = $em->getRepository($options['repository'])
            ->createQueryBuilder('a')
            ->where('a.usuario = :usuario')
            ->andWhere("a.".$options['field']." LIKE ?1")
            ->setParameter('usuario', $user->getId())
            ->setParameter(1, "%" . $term . "%")
            ->orderBy('a.id', 'DESC')
        ;        

        $entities = $queryBuilder->getQuery()->getResult();

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id'   => $entity->getId(),
                'text' => $entity->__toString(),
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }
    
    /**
     * Create query.
     * @param string $repository
     * @return Doctrine\ORM\QueryBuilder $queryBuilder
     */
    protected function createQuery($repository)
    {        
        $sc = $this->container->get('security.context');
        $user = $sc->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository($repository)
            ->createQueryBuilder('a')
            ->where('a.owner = :owner')
            ->setParameter('owner', $user->getId())
            ->orderBy('a.id', 'DESC')
        ;

        return $queryBuilder;
    }
}