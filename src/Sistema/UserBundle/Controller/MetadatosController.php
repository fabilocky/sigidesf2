<?php

namespace Sistema\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\UserBundle\Entity\Metadatos;
use Sistema\UserBundle\Form\MetadatosType;
use Sistema\UserBundle\Form\MetadatosFilterType;

/**
 * Metadatos controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/usuario/metadatos")
 */
class MetadatosController extends Controller
{
    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/UserBundle/Resources/config/Metadatos.yml',
    );

    /**
     * Lists all Metadatos entities.
     *
     * @Route("/", name="admin_metadatos")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->config['filterType'] = new MetadatosFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Metadatos entity.
     *
     * @Route("/", name="admin_metadatos_create")
     * @Method("POST")
     * @Template("SistemaUserBundle:Metadatos:new.html.twig")
     */
    public function createAction()
    {
        $request = $this->getRequest();
        $user = $this->container->get('security.context')->getToken()->getUser();
        $metadatos = $request->get("sistema_userbundle_metadatos");
        //ladybug_dump($metadatos);die();
        $em = $this->getDoctrine()->getManager();
        $capa = $em->getRepository("SistemaUserBundle:UsuarioCapa")->find($metadatos["usuariocapa"]);
        $this->config['newType'] = new \Sistema\UserBundle\Form\MetadatosCapaType($user, $capa);
        $config = $this->getConfig();
    	
        
        $xml = $this->guardarXml($metadatos);
                
        $entity = new $config['entity']();
        $entity->setUsuarioCapa($capa);
        if ($entity->getFileName() == null){
                $sc = $this->container->get('security.context');
                $user = $sc->getToken()->getUser();
                $archivo = fopen('files/metadatos/'. $user->getUsername(). "_" .$entity->getUsuariocapa(). ".xml", "w");
                fwrite($archivo, $xml);
                fclose($archivo);
                $entity->setFileName($user->getUsername(). "_" .$entity->getUsuariocapa(). ".xml");
                $entity->setUpdatedAt(new \DateTime("now"));
            }
        $form   = $this->createFsForm($config, $entity);
        $form->handleRequest($request);        
        if ($form->isValid()) {
            $entity->getUsuarioCapa()->setEstado("En revision");
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->useACL($entity, 'create');

            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');
            return $this->redirect($this->generateUrl('admin_usuariocapa'));

        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config'     => $config,
            'entity'     => $entity,
            'form'       => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Metadatos entity.
     *
     * @Route("/new", name="admin_metadatos_new")
     * @Method("GET")
     * @Template("SistemaUserBundle:Metadatos:new.html.twig")
     */
    public function newAction()
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        $this->config['newType'] = new \Sistema\UserBundle\Form\MetadatosCapaType($user);
        $config = $this->getConfig();
        $entity = new $config['entity']();       
        
        $form = $this->createFsForm($config, $entity);

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config'     => $config,
            'entity'     => $entity,
            'form'       => $form->createView(),
        );
    }
    
    /**
     * Displays a form to create a new Metadatos entity.
     *
     * @Route("/nuevo/{idCapa}", name="admin_metadatos_nuevo", defaults={"idCapa" = null}))
     * @Method("GET")
     * @Template("SistemaUserBundle:Metadatos:new.html.twig")
     */
    public function nuevoAction($idCapa)
    {
        $em = $this->getDoctrine()->getManager();
        $capa = $em->getRepository("SistemaUserBundle:UsuarioCapa")->find($idCapa);        
        
        $user = $this->container->get('security.context')->getToken()->getUser();
        $this->config['newType'] = new \Sistema\UserBundle\Form\MetadatosCapaType($user, $capa);
        $config = $this->getConfig();
        $entity = new $config['entity']();        
        $entity->setUsuarioCapa($capa);
        $form = $this->createFsForm($config, $entity);

        // remove the form to return to the view
        unset($config['newType']);
        $configmet = $em->getRepository('SistemaAdminBundle:Config')->findAll();
        return array(
            'config'     => $config,
            'entity'     => $entity,
            'form'       => $form->createView(),
            'configmet'  => $configmet[0]
        );
    }

    /**
     * Finds and displays a Metadatos entity.
     *
     * @Route("/{id}", name="admin_metadatos_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Metadatos entity.
     *
     * @Route("/{id}/edit", name="admin_metadatos_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->config['editType'] = new \Sistema\UserBundle\Form\MetadatosCapaEditType();
        $config = $this->getConfig();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find '.$config['entityName'].' entity.');
        }
        
        $library = simplexml_load_string(file_get_contents("files/metadatos/".$entity->getFileName()));
        $arrayMetadatos["titulo"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->citation->children('gmd', true)->CI_Citation->children('gmd', true)->title->children('gco', true)->CharacterString;
        $arrayMetadatos["fecha"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->citation->children('gmd', true)->CI_Citation->children('gmd', true)->date->children('gmd', true)->CI_Date->children('gmd', true)->date->children('gco', true)->DateTime;
        $arrayMetadatos["tipofecha"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->citation->children('gmd', true)->CI_Citation->children('gmd', true)->date->children('gmd', true)->CI_Date->children('gmd', true)->dateType->children('gmd', true)->CI_DateTypeCode->attributes();
        $arrayMetadatos["Resumencd"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->abstract->children('gco', true)->CharacterString;
        $arrayMetadatos["pcnombre"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->pointOfContact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->individualName->children('gco', true)->CharacterString;
        $arrayMetadatos["pcorganizacion"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->pointOfContact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->organisationName->children('gco', true)->CharacterString;
        $arrayMetadatos["pccargo"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->pointOfContact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->positionName->children('gco', true)->CharacterString;
        $arrayMetadatos["pcrol"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->pointOfContact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->role->children('gmd', true)->CI_RoleCode->attributes();
        $arrayMetadatos["pctelefono"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->pointOfContact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->phone->children('gmd', true)->CI_Telephone->children('gmd', true)->voice->children('gco', true)->CharacterString;
        $arrayMetadatos["pcfax"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->pointOfContact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->phone->children('gmd', true)->CI_Telephone->children('gmd', true)->facsimile->children('gco', true)->CharacterString;
        $arrayMetadatos["pcdireccion"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->pointOfContact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->address->children('gmd', true)->CI_Address->children('gmd', true)->deliveryPoint->children('gco', true)->CharacterString;
        $arrayMetadatos["pcciudad"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->pointOfContact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->address->children('gmd', true)->CI_Address->children('gmd', true)->city->children('gco', true)->CharacterString;
        $arrayMetadatos["pcprovincia"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->pointOfContact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->address->children('gmd', true)->CI_Address->children('gmd', true)->administrativeArea->children('gco', true)->CharacterString;
        $arrayMetadatos["pccodpostal"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->pointOfContact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->address->children('gmd', true)->CI_Address->children('gmd', true)->postalCode->children('gco', true)->CharacterString;
        $arrayMetadatos["pcpais"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->pointOfContact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->address->children('gmd', true)->CI_Address->children('gmd', true)->country->children('gco', true)->CharacterString;
        $arrayMetadatos["pcemail"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->pointOfContact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->address->children('gmd', true)->CI_Address->children('gmd', true)->electronicMailAddress->children('gco', true)->CharacterString;
        $arrayMetadatos["palabrasclave"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->descriptiveKeywords->children('gmd', true)->MD_Keywords->children('gmd', true)->keyword->children('gco', true)->CharacterString;
        $arrayMetadatos["tipopclave"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->descriptiveKeywords->children('gmd', true)->MD_Keywords->children('gmd', true)->type->children('gmd', true)->MD_KeywordTypeCode->attributes();
        $arrayMetadatos["tiporepesp"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->spatialRepresentationType->children('gmd', true)->MD_SpatialRepresentationTypeCode->attributes();
        $arrayMetadatos["denominador"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->spatialResolution->children('gmd', true)->MD_Resolution->children('gmd', true)->equivalentScale->children('gmd', true)->MD_RepresentativeFraction->children('gmd', true)->denominator->children('gco', true)->Integer;
        $arrayMetadatos["idiomacd"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->language->children('gmd', true)->LanguageCode->attributes()['codeListValue'];                
        $arrayMetadatos["caracterescd"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->characterSet->children('gmd', true)->MD_CharacterSetCode->attributes();
        $arrayMetadatos["temacd"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->topicCategory->children('gmd', true)->MD_TopicCategoryCode;
        $arrayMetadatos["desde"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->extent[0]->children('gmd', true)->EX_Extent->children('gmd', true)->temporalElement->children('gmd', true)->EX_TemporalExtent->children('gmd', true)->extent->children('gml', true)->TimePeriod->children('gml', true)->beginPosition;
        $arrayMetadatos["hasta"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->extent[0]->children('gmd', true)->EX_Extent->children('gmd', true)->temporalElement->children('gmd', true)->EX_TemporalExtent->children('gmd', true)->extent->children('gml', true)->TimePeriod->children('gml', true)->endPosition;
        $arrayMetadatos["loeste"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->extent[1]->children('gmd', true)->EX_Extent->children('gmd', true)->geographicElement->children('gmd', true)->EX_GeographicBoundingBox->children('gmd', true)->westBoundLongitude->children('gco', true)->Decimal;
        $arrayMetadatos["leste"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->extent[1]->children('gmd', true)->EX_Extent->children('gmd', true)->geographicElement->children('gmd', true)->EX_GeographicBoundingBox->children('gmd', true)->eastBoundLongitude->children('gco', true)->Decimal;
        $arrayMetadatos["lsur"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->extent[1]->children('gmd', true)->EX_Extent->children('gmd', true)->geographicElement->children('gmd', true)->EX_GeographicBoundingBox->children('gmd', true)->southBoundLatitude->children('gco', true)->Decimal;
        $arrayMetadatos["lnorte"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->extent[1]->children('gmd', true)->EX_Extent->children('gmd', true)->geographicElement->children('gmd', true)->EX_GeographicBoundingBox->children('gmd', true)->northBoundLatitude->children('gco', true)->Decimal;
        $arrayMetadatos["nomgeog"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->extent[1]->children('gmd', true)->EX_Extent->children('gmd', true)->description->children('gco', true)->CharacterString;
        $arrayMetadatos["infocomp"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->supplementalInformation->children('gco', true)->CharacterString;
        $arrayMetadatos["njerarquico"] = $library->children('gmd', true)->dataQualityInfo->children('gmd', true)->DQ_DataQuality->children('gmd', true)->scope->children('gmd', true)->DQ_Scope->children('gmd', true)->level->children('gmd', true)->MD_ScopeCode->attributes();
        $arrayMetadatos["linaje"] = $library->children('gmd', true)->dataQualityInfo->children('gmd', true)->DQ_DataQuality->children('gmd', true)->lineage->children('gmd', true)->LI_Lineage->children('gmd', true)->statement->children('gco', true)->CharacterString;
        $arrayMetadatos["codigoref"] = $library->children('gmd', true)->referenceSystemInfo->children('gmd', true)->MD_ReferenceSystem->children('gmd', true)->referenceSystemIdentifier->children('gmd', true)->RS_Identifier->children('gmd', true)->code->children('gco', true)->CharacterString;
        $arrayMetadatos["distnom"] = $library->children('gmd', true)->distributionInfo->children('gmd', true)->MD_Distribution->children('gmd', true)->distributionFormat->children('gmd', true)->MD_Format->children('gmd', true)->name->children('gco', true)->CharacterString;
        $arrayMetadatos["distversion"] =$library->children('gmd', true)->distributionInfo->children('gmd', true)->MD_Distribution->children('gmd', true)->distributionFormat->children('gmd', true)->MD_Format->children('gmd', true)->version->children('gco', true)->CharacterString;
        $arrayMetadatos["url"] = $library->children('gmd', true)->distributionInfo->children('gmd', true)->MD_Distribution->children('gmd', true)->transferOptions->children('gmd', true)->MD_DigitalTransferOptions->children('gmd', true)->onLine->children('gmd', true)->CI_OnlineResource->children('gmd', true)->linkage->children('gmd', true)->URL;
        $arrayMetadatos["descripcion"] = $library->children('gmd', true)->distributionInfo->children('gmd', true)->MD_Distribution->children('gmd', true)->transferOptions->children('gmd', true)->MD_DigitalTransferOptions->children('gmd', true)->onLine->children('gmd', true)->CI_OnlineResource->children('gmd', true)->description->children('gco', true)->CharacterString;
        $arrayMetadatos["identificador"] = $library->children('gmd', true)->fileIdentifier->children('gco', true)->CharacterString;
        $arrayMetadatos["idiomamd"] = $library->children('gmd', true)->language->children('gmd', true)->LanguageCode->attributes()['codeListValue'];
        $arrayMetadatos["normamd"] = $library->children('gmd', true)->metadataStandardName->children('gco', true)->CharacterString;
        $arrayMetadatos["versionmd"] = $library->children('gmd', true)->metadataStandardVersion->children('gco', true)->CharacterString;
        $arrayMetadatos["caracteresmd"] = $library->children('gmd', true)->characterSet->children('gmd', true)->MD_CharacterSetCode->attributes();
        $arrayMetadatos["fechamd"] = $library->children('gmd', true)->dateStamp->children('gco', true)->DateTime;
        $arrayMetadatos["nombremd"] =$library->children('gmd', true)->contact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->individualName->children('gco', true)->CharacterString;
        $arrayMetadatos["organizacionmd"] = $library->children('gmd', true)->contact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->organisationName->children('gco', true)->CharacterString;
        $arrayMetadatos["cargomd"] = $library->children('gmd', true)->contact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->positionName->children('gco', true)->CharacterString;
        $arrayMetadatos["rolmd"] = $library->children('gmd', true)->contact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->role->children('gmd', true)->CI_RoleCode->attributes();
        $arrayMetadatos["telefonomd"] = $library->children('gmd', true)->contact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->phone->children('gmd', true)->CI_Telephone->children('gmd', true)->voice->children('gco', true)->CharacterString;
        $arrayMetadatos["faxmd"] = $library->children('gmd', true)->contact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->phone->children('gmd', true)->CI_Telephone->children('gmd', true)->facsimile->children('gco', true)->CharacterString;
        $arrayMetadatos["direccionmd"] = $library->children('gmd', true)->contact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->address->children('gmd', true)->CI_Address->children('gmd', true)->deliveryPoint->children('gco', true)->CharacterString;
        $arrayMetadatos["ciudadmd"] = $library->children('gmd', true)->contact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->address->children('gmd', true)->CI_Address->children('gmd', true)->city->children('gco', true)->CharacterString;
        $arrayMetadatos["provinciamd"] = $library->children('gmd', true)->contact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->address->children('gmd', true)->CI_Address->children('gmd', true)->administrativeArea->children('gco', true)->CharacterString;
        $arrayMetadatos["codpostalmd"] = $library->children('gmd', true)->contact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->address->children('gmd', true)->CI_Address->children('gmd', true)->postalCode->children('gco', true)->CharacterString;
        $arrayMetadatos["paismd"] = $library->children('gmd', true)->contact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->address->children('gmd', true)->CI_Address->children('gmd', true)->country->children('gco', true)->CharacterString;
        $arrayMetadatos["emailmd"] = $library->children('gmd', true)->contact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->address->children('gmd', true)->CI_Address->children('gmd', true)->electronicMailAddress->children('gco', true)->CharacterString;
        
        $this->useACL($entity, 'edit');
        $editForm = $this->createEditForm($config, $entity);
        $deleteForm = $this->createDeleteForm($config, $id);
        $desde = explode("T", $arrayMetadatos["desde"])[0];
        $hasta = explode("T", $arrayMetadatos["hasta"])[0];
        // remove the form to return to the view
        unset($config['editType']);
        $config = $em->getRepository('SistemaAdminBundle:Config')->findAll();
        return array(
            'config'      => $config,
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'metadatos'   => $arrayMetadatos,
            'config'      => $config[0],
            'desde'      => $desde,
            'hasta'      => $hasta
        );
    }

    /**
     * Edits an existing Metadatos entity.
     *
     * @Route("/{id}", name="admin_metadatos_update")
     * @Method("PUT")
     * @Template("SistemaUserBundle:Metadatos:edit.html.twig")
     */
    public function updateAction($id)
    {
        $this->config['editType'] = new \Sistema\UserBundle\Form\MetadatosCapaEditType();
        $config = $this->getConfig();
    	$request = $this->getRequest();
        $metadatos = $request->get("sistema_userbundle_metadatos");
        $xml = $this->guardarXml($metadatos);
        
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find '.$config['entityName'].' entity.');
        }
        
        if ($entity->getFileName()){
                $sc = $this->container->get('security.context');
                $user = $sc->getToken()->getUser();
                $archivo = fopen('files/metadatos/'. $entity->getFileName(), "w");
                fwrite($archivo, $xml);
                fclose($archivo);
                $entity->setFileName($entity->getFileName());
                $entity->setUpdatedAt(new \DateTime("now"));
            }
        $entity->getUsuarioCapa()->setEstado("En revision");
        $entity->getUsuarioCapa()->setInforme("En revision por modificacion");
        $this->useACL($entity, 'update');
        $deleteForm = $this->createDeleteForm($config, $id);
        $editForm = $this->createEditForm($config, $entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {            
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $this->generateUrl('admin_usuariocapa');
            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config'      => $config,
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Metadatos entity.
     *
     * @Route("/{id}", name="admin_metadatos_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $response = parent::deleteAction($id);

        return $response;
    }
    
    public function guardarXml($request) {        
        $xml = new \DOMDocument('1.0');

        $root = $xml->createElement('gmd:MD_Metadata');

        $root->setAttribute('xmlns:gmd', 'http://www.isotc211.org/2005/gmd');
        $root->setAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
        $root->setAttribute('xmlns:gml', 'http://www.opengis.net/gml');
        $root->setAttribute('xmlns:gts', 'http://www.isotc211.org/2005/gts');
        $root->setAttribute('xmlns:gco', 'http://www.isotc211.org/2005/gco');
        $root->setAttribute('xmlns:geonet', 'http://www.fao.org/geonetwork');

        $root = $xml->appendChild($root);        

        $fileIdentifier = $xml->createElement('gmd:fileIdentifier');
        $fileIdentifier->setAttribute('xmlns:gmx', 'http://www.isotc211.org/2005/gmx');
        $fileIdentifier->setAttribute('xmlns:srv', 'http://www.isotc211.org/2005/srv');
        $fileIdentifier = $root->appendChild($fileIdentifier);

        $CharacterString = $xml->createElement('gco:CharacterString', $request['fileIdentifier']);
        $CharacterString = $fileIdentifier->appendChild($CharacterString);

        $language = $xml->createElement('gmd:language');
        $language = $root->appendChild($language);

        $languagecode = $xml->createElement('gmd:LanguageCode');
        $languagecode->setAttribute('codeList', 'http://www.isotc211.org/2005/resources/codeList.xml#LanguageCode');
        $languagecode->setAttribute('codeListValue', $request['LanguageCode_MD']);
        $languagecode = $language->appendChild($languagecode);

        $characterSet = $xml->createElement('gmd:characterSet');
        $characterSet = $root->appendChild($characterSet);

        $MD_CharacterSetCode = $xml->createElement('gmd:MD_CharacterSetCode');
        $MD_CharacterSetCode->setAttribute('codeListValue', $request['CharacterSetCode_MD']);
        $MD_CharacterSetCode->setAttribute('codeList', './resources/codeList.xml#MD_CharacterSetCode');
        $MD_CharacterSetCode = $characterSet->appendChild($MD_CharacterSetCode);

        $contact = $xml->createElement('gmd:contact');
        $contact = $root->appendChild($contact);

        $CI_ResponsibleParty = $xml->createElement('gmd:CI_ResponsibleParty');
        $CI_ResponsibleParty = $contact->appendChild($CI_ResponsibleParty);

        $individualName = $xml->createElement('gmd:individualName');
        $individualName = $CI_ResponsibleParty->appendChild($individualName);

        $CharacterString = $xml->createElement('gco:CharacterString', $request['individualName_MD']);
        $CharacterString = $individualName->appendChild($CharacterString);

        $organisationName = $xml->createElement('gmd:organisationName');
        $organisationName = $CI_ResponsibleParty->appendChild($organisationName);

        $CharacterString = $xml->createElement('gco:CharacterString', $request['organisationName_MD']);
        $CharacterString = $organisationName->appendChild($CharacterString);

        $positionName = $xml->createElement('gmd:positionName');
        $positionName = $CI_ResponsibleParty->appendChild($positionName);

        $CharacterString = $xml->createElement('gco:CharacterString', $request['positionName_MD']);
        $CharacterString = $positionName->appendChild($CharacterString);

        $contactInfo = $xml->createElement('gmd:contactInfo');
        $contactInfo = $CI_ResponsibleParty->appendChild($contactInfo);

        $CI_Contact = $xml->createElement('gmd:CI_Contact');
        $CI_Contact = $contactInfo->appendChild($CI_Contact);

        $phone = $xml->createElement('gmd:phone');
        $phone = $CI_Contact->appendChild($phone);

        $CI_Telephone = $xml->createElement('gmd:CI_Telephone');
        $CI_Telephone = $phone->appendChild($CI_Telephone);

        $voice = $xml->createElement('gmd:voice');
        $voice = $CI_Telephone->appendChild($voice);

        $CharacterString = $xml->createElement('gco:CharacterString', $request['voice_MD']);
        $CharacterString = $voice->appendChild($CharacterString);

        $facsimile = $xml->createElement('gmd:facsimile');
        $facsimile = $CI_Telephone->appendChild($facsimile);

        $CharacterString = $xml->createElement('gco:CharacterString', $request['facsimile_MD']);
        $CharacterString = $facsimile->appendChild($CharacterString);

        $address = $xml->createElement('gmd:address');
        $address = $CI_Contact->appendChild($address);

        $CI_Address = $xml->createElement('gmd:CI_Address');
        $CI_Address = $address->appendChild($CI_Address);

        $deliveryPoint = $xml->createElement('gmd:deliveryPoint');
        $deliveryPoint = $CI_Address->appendChild($deliveryPoint);

        $CharacterString = $xml->createElement('gco:CharacterString', $request['deliveryPoint_MD']);
        $CharacterString = $deliveryPoint->appendChild($CharacterString);

        $city = $xml->createElement('gmd:city');
        $city = $CI_Address->appendChild($city);

        $CharacterString = $xml->createElement('gco:CharacterString', $request['city_MD']);
        $CharacterString = $city->appendChild($CharacterString);

        $administrativeArea = $xml->createElement('gmd:administrativeArea');
        $administrativeArea = $CI_Address->appendChild($administrativeArea);

        $CharacterString = $xml->createElement('gco:CharacterString', $request['administrativeArea_MD']);
        $CharacterString = $administrativeArea->appendChild($CharacterString);

        $postalCode = $xml->createElement('gmd:postalCode');
        $postalCode = $CI_Address->appendChild($postalCode);

        $CharacterString = $xml->createElement('gco:CharacterString', $request['postalCode_MD']);
        $CharacterString = $postalCode->appendChild($CharacterString);

        $country = $xml->createElement('gmd:country');
        $country = $CI_Address->appendChild($country);

        $CharacterString = $xml->createElement('gco:CharacterString', $request['country_MD']);
        $CharacterString = $country->appendChild($CharacterString);

        $electronicMailAddress = $xml->createElement('gmd:electronicMailAddress');
        $electronicMailAddress = $CI_Address->appendChild($electronicMailAddress);

        $CharacterString = $xml->createElement('gco:CharacterString', $request['electronicMailAddress_MD']);
        $CharacterString = $electronicMailAddress->appendChild($CharacterString);

        $role = $xml->createElement('gmd:role');
        $role = $CI_ResponsibleParty->appendChild($role);

        $CI_RoleCode = $xml->createElement('gmd:CI_RoleCode');
        $CI_RoleCode->setAttribute('codeListValue', $request['CI_RoleCode_MD']);
        $CI_RoleCode->setAttribute('codeList', 'http://www.isotc211.org/2005/resources/codeList.xml#CI_RoleCode');
        $CI_RoleCode = $role->appendChild($CI_RoleCode);

        $dateStamp = $xml->createElement('gmd:dateStamp');
        $dateStamp = $root->appendChild($dateStamp);

        $DateTime = $xml->createElement('gco:DateTime', $request['dateStamp']);
        $DateTime->setAttribute('xmlns:srv', 'http://www.isotc211.org/2005/srv');
        $DateTime->setAttribute('xmlns:gmx', 'http://www.isotc211.org/2005/gmx');
        $DateTime = $dateStamp->appendChild($DateTime);

        $metadataStandardName = $xml->createElement('gmd:metadataStandardName');
        $metadataStandardName = $root->appendChild($metadataStandardName);

        $CharacterString = $xml->createElement('gco:CharacterString', $request['metadataStandardName']);
        $CharacterString = $metadataStandardName->appendChild($CharacterString);

        $metadataStandardVersion = $xml->createElement('gmd:metadataStandardVersion');
        $metadataStandardVersion = $root->appendChild($metadataStandardVersion);

        $CharacterString = $xml->createElement('gco:CharacterString', $request['metadataStandardVersion']);
        $CharacterString = $metadataStandardVersion->appendChild($CharacterString);

        $referenceSystemInfo = $xml->createElement('gmd:referenceSystemInfo');
        $referenceSystemInfo = $root->appendChild($referenceSystemInfo);

        $MD_ReferenceSystem = $xml->createElement('gmd:MD_ReferenceSystem');
        $MD_ReferenceSystem = $referenceSystemInfo->appendChild($MD_ReferenceSystem);

        $referenceSystemIdentifier = $xml->createElement('gmd:referenceSystemIdentifier');
        $referenceSystemIdentifier = $MD_ReferenceSystem->appendChild($referenceSystemIdentifier);

        $RS_Identifier = $xml->createElement('gmd:RS_Identifier');
        $RS_Identifier = $referenceSystemIdentifier->appendChild($RS_Identifier);

        $code = $xml->createElement('gmd:code');
        $code = $RS_Identifier->appendChild($code);

        $CharacterString = $xml->createElement('gco:CharacterString', $request['code']);
        $CharacterString = $code->appendChild($CharacterString);

        $identificationInfo = $xml->createElement('gmd:identificationInfo');
        $identificationInfo = $root->appendChild($identificationInfo);

        $MD_DataIdentification = $xml->createElement('gmd:MD_DataIdentification');
        $MD_DataIdentification = $identificationInfo->appendChild($MD_DataIdentification);
        
        $citation = $xml->createElement('gmd:citation');
        $citation = $MD_DataIdentification->appendChild($citation);

        $CI_Citation = $xml->createElement('gmd:CI_Citation');
        $CI_Citation = $citation->appendChild($CI_Citation);

        $title = $xml->createElement('gmd:title');
        $title = $CI_Citation->appendChild($title);

        $CharacterString = $xml->createElement('gco:CharacterString', $request['title']);
        $CharacterString = $title->appendChild($CharacterString);

        $date = $xml->createElement('gmd:date');
        $date = $CI_Citation->appendChild($date);

        $CI_Date = $xml->createElement('gmd:CI_Date');
        $CI_Date = $date->appendChild($CI_Date);

        $date = $xml->createElement('gmd:date');
        $date = $CI_Date->appendChild($date);

        $CharacterString = $xml->createElement('gco:DateTime', $request['date']);
        $CharacterString = $date->appendChild($CharacterString);

        $dateType = $xml->createElement('gmd:dateType');
        $dateType = $CI_Date->appendChild($dateType);

        $CI_DateTypeCode = $xml->createElement('gmd:CI_DateTypeCode');
        $CI_DateTypeCode->setAttribute('codeListValue', $request['CI_DateTypeCode']);
        $CI_DateTypeCode->setAttribute('codeList', './resources/codeList.xml#MD_CharacterSetCode');
        $CI_DateTypeCode = $dateType->appendChild($CI_DateTypeCode);

        $abstract = $xml->createElement('gmd:abstract');
        $abstract = $MD_DataIdentification->appendChild($abstract);

        $CharacterString = $xml->createElement('gco:CharacterString', $request['abstract']);
        $CharacterString = $abstract->appendChild($CharacterString);

        $pointOfContact = $xml->createElement('gmd:pointOfContact');
        $pointOfContact = $MD_DataIdentification->appendChild($pointOfContact);

        $CI_ResponsibleParty = $xml->createElement('gmd:CI_ResponsibleParty');
        $CI_ResponsibleParty = $pointOfContact->appendChild($CI_ResponsibleParty);

        $individualName = $xml->createElement('gmd:individualName');
        $individualName = $CI_ResponsibleParty->appendChild($individualName);

        $CharacterString = $xml->createElement('gco:CharacterString', $request['individualName']);
        $CharacterString = $individualName->appendChild($CharacterString);

        $organisationName = $xml->createElement('gmd:organisationName');
        $organisationName = $CI_ResponsibleParty->appendChild($organisationName);

        $CharacterString = $xml->createElement('gco:CharacterString', $request['organisationName']);
        $CharacterString = $organisationName->appendChild($CharacterString);

        $positionName = $xml->createElement('gmd:positionName');
        $positionName = $CI_ResponsibleParty->appendChild($positionName);

        $CharacterString = $xml->createElement('gco:CharacterString', $request['positionName']);
        $CharacterString = $positionName->appendChild($CharacterString);

        $contactInfo = $xml->createElement('gmd:contactInfo');
        $contactInfo = $CI_ResponsibleParty->appendChild($contactInfo);

        $CI_Contact = $xml->createElement('gmd:CI_Contact');
        $CI_Contact = $contactInfo->appendChild($CI_Contact);

        $phone = $xml->createElement('gmd:phone');
        $phone = $CI_Contact->appendChild($phone);

        $CI_Telephone = $xml->createElement('gmd:CI_Telephone');
        $CI_Telephone = $phone->appendChild($CI_Telephone);

        $voice = $xml->createElement('gmd:voice');
        $voice = $CI_Telephone->appendChild($voice);

        $CharacterString = $xml->createElement('gco:CharacterString', $request['voice']);
        $CharacterString = $voice->appendChild($CharacterString);

        $facsimile = $xml->createElement('gmd:facsimile');
        $facsimile = $CI_Telephone->appendChild($facsimile);

        $CharacterString = $xml->createElement('gco:CharacterString', $request['facsimile']);
        $CharacterString = $facsimile->appendChild($CharacterString);

        $address = $xml->createElement('gmd:address');
        $address = $CI_Contact->appendChild($address);

        $CI_Address = $xml->createElement('gmd:CI_Address');
        $CI_Address = $address->appendChild($CI_Address);

        $deliveryPoint = $xml->createElement('gmd:deliveryPoint');
        $deliveryPoint = $CI_Address->appendChild($deliveryPoint);

        $CharacterString = $xml->createElement('gco:CharacterString', $request['deliveryPoint']);
        $CharacterString = $deliveryPoint->appendChild($CharacterString);

        $city = $xml->createElement('gmd:city');
        $city = $CI_Address->appendChild($city);

        $CharacterString = $xml->createElement('gco:CharacterString', $request['city']);
        $CharacterString = $city->appendChild($CharacterString);

        $administrativeArea = $xml->createElement('gmd:administrativeArea');
        $administrativeArea = $CI_Address->appendChild($administrativeArea);

        $CharacterString = $xml->createElement('gco:CharacterString', $request['administrativeArea']);
        $CharacterString = $administrativeArea->appendChild($CharacterString);

        $postalCode = $xml->createElement('gmd:postalCode');
        $postalCode = $CI_Address->appendChild($postalCode);

        $CharacterString = $xml->createElement('gco:CharacterString', $request['postalCode']);
        $CharacterString = $postalCode->appendChild($CharacterString);

        $country = $xml->createElement('gmd:country');
        $country = $CI_Address->appendChild($country);

        $CharacterString = $xml->createElement('gco:CharacterString', $request['country']);
        $CharacterString = $country->appendChild($CharacterString);

        $electronicMailAddress = $xml->createElement('gmd:electronicMailAddress');
        $electronicMailAddress = $CI_Address->appendChild($electronicMailAddress);

        $CharacterString = $xml->createElement('gco:CharacterString', $request['electronicMailAddress']);
        $CharacterString = $electronicMailAddress->appendChild($CharacterString);

        $role = $xml->createElement('gmd:role');
        $role = $CI_ResponsibleParty->appendChild($role);

        $CI_RoleCode = $xml->createElement('gmd:CI_RoleCode');
        $CI_RoleCode->setAttribute('codeListValue', $request['CI_RoleCode']);
        $CI_RoleCode->setAttribute('codeList', 'http://www.isotc211.org/2005/resources/codeList.xml#CI_RoleCode');
        $CI_RoleCode = $role->appendChild($CI_RoleCode);

        $descriptiveKeywords = $xml->createElement('gmd:descriptiveKeywords');
        $descriptiveKeywords = $MD_DataIdentification->appendChild($descriptiveKeywords);

        $MD_Keywords = $xml->createElement('gmd:MD_Keywords');
        $MD_Keywords = $descriptiveKeywords->appendChild($MD_Keywords);

        $keyword = $xml->createElement('gmd:keyword');
        $keyword = $MD_Keywords->appendChild($keyword);

        $CharacterString = $xml->createElement('gco:CharacterString', $request['keyword']);
        $CharacterString = $keyword->appendChild($CharacterString);

        $type = $xml->createElement('gmd:type');
        $type = $MD_Keywords->appendChild($type);

        $MD_KeywordTypeCode = $xml->createElement('gmd:MD_KeywordTypeCode');
        $MD_KeywordTypeCode->setAttribute('codeListValue', $request['KeywordTypeCode']);
        $MD_KeywordTypeCode->setAttribute('codeList', 'http://www.isotc211.org/2005/resources/codeList.xml#MD_KeywordTypeCode');
        $MD_KeywordTypeCode = $type->appendChild($MD_KeywordTypeCode);

        $spatialRepresentationType = $xml->createElement('gmd:spatialRepresentationType');
        $spatialRepresentationType = $MD_DataIdentification->appendChild($spatialRepresentationType);

        $MD_SpatialRepresentationTypeCode = $xml->createElement('gmd:MD_SpatialRepresentationTypeCode');
        $MD_SpatialRepresentationTypeCode->setAttribute('codeListValue', $request['SpatialRepresentationTypeCode']);
        $MD_SpatialRepresentationTypeCode->setAttribute('codeList', 'http://www.isotc211.org/2005/resources/codeList.xml#MD_SpatialRepresentationTypeCode');
        $MD_SpatialRepresentationTypeCode = $spatialRepresentationType->appendChild($MD_SpatialRepresentationTypeCode);

        $spatialResolution = $xml->createElement('gmd:spatialResolution');
        $spatialResolution = $MD_DataIdentification->appendChild($spatialResolution);

        $MD_Resolution = $xml->createElement('gmd:MD_Resolution');
        $MD_Resolution = $spatialResolution->appendChild($MD_Resolution);

        $equivalentScale = $xml->createElement('gmd:equivalentScale');
        $equivalentScale = $MD_Resolution->appendChild($equivalentScale);

        $MD_RepresentativeFraction = $xml->createElement('gmd:MD_RepresentativeFraction');
        $MD_RepresentativeFraction = $equivalentScale->appendChild($MD_RepresentativeFraction);

        $denominator = $xml->createElement('gmd:denominator');
        $denominator = $MD_RepresentativeFraction->appendChild($denominator);

        $Integer = $xml->createElement('gco:Integer', $request['denominator']);
        $Integer = $denominator->appendChild($Integer);

        $language = $xml->createElement('gmd:language');
        $language = $MD_DataIdentification->appendChild($language);

        $languagecode = $xml->createElement('gmd:LanguageCode');
        $languagecode->setAttribute('codeList', 'http://www.isotc211.org/2005/resources/codeList.xml#LanguageCode');
        $languagecode->setAttribute('codeListValue', $request['LanguageCode']);
        $languagecode = $language->appendChild($languagecode);

        $characterSet = $xml->createElement('gmd:characterSet');
        $characterSet = $MD_DataIdentification->appendChild($characterSet);

        $MD_CharacterSetCode = $xml->createElement('gmd:MD_CharacterSetCode');
        $MD_CharacterSetCode->setAttribute('codeListValue', $request['CharacterSetCode']);
        $MD_CharacterSetCode->setAttribute('codeList', './resources/codeList.xml#MD_CharacterSetCode');
        $MD_CharacterSetCode = $characterSet->appendChild($MD_CharacterSetCode);

        $topicCategory = $xml->createElement('gmd:topicCategory');
        $topicCategory = $MD_DataIdentification->appendChild($topicCategory);

        $MD_TopicCategoryCode = $xml->createElement('gmd:MD_TopicCategoryCode', $request['TopicCategoryCode']);
        $MD_TopicCategoryCode = $topicCategory->appendChild($MD_TopicCategoryCode);

        $extent = $xml->createElement('gmd:extent');
        $extent = $MD_DataIdentification->appendChild($extent);

        $EX_Extent = $xml->createElement('gmd:EX_Extent');
        $EX_Extent = $extent->appendChild($EX_Extent);

        $temporalElement = $xml->createElement('gmd:temporalElement');
        $temporalElement = $EX_Extent->appendChild($temporalElement);

        $EX_TemporalExtent = $xml->createElement('gmd:EX_TemporalExtent');
        $EX_TemporalExtent = $temporalElement->appendChild($EX_TemporalExtent);

        $extent = $xml->createElement('gmd:extent');
        $extent = $EX_TemporalExtent->appendChild($extent);

        $fecha_extent_begin = explode(" ", $request['extent_begin']);
        $fecha_extent_end = explode(" ", $request['extent_end']);

        $TimePeriod = $xml->createElement('gml:TimePeriod');
        $TimePeriod->setAttribute('gml:id', 'd1e468a1049886');
        $TimePeriod = $extent->appendChild($TimePeriod);

        $CharacterString = $xml->createElement('gml:beginPosition', $fecha_extent_begin[0] . 'T' . $fecha_extent_begin[1]);
        $CharacterString = $TimePeriod->appendChild($CharacterString);

        $CharacterString = $xml->createElement('gml:endPosition', $fecha_extent_begin[0] . 'T' . $fecha_extent_begin[1]);
        $CharacterString = $TimePeriod->appendChild($CharacterString);

        $extent = $xml->createElement('gmd:extent');
        $extent = $MD_DataIdentification->appendChild($extent);

        $EX_Extent = $xml->createElement('gmd:EX_Extent');
        $EX_Extent = $extent->appendChild($EX_Extent);

        $description = $xml->createElement('gmd:description');
        $description = $EX_Extent->appendChild($description);

        $CharacterString = $xml->createElement('gco:CharacterString', $request['extent']);
        $CharacterString = $description->appendChild($CharacterString);

        $geographicElement = $xml->createElement('gmd:geographicElement');
        $geographicElement = $EX_Extent->appendChild($geographicElement);

        $EX_GeographicBoundingBox = $xml->createElement('gmd:EX_GeographicBoundingBox');
        $EX_GeographicBoundingBox = $geographicElement->appendChild($EX_GeographicBoundingBox);

        $westBoundLongitude = $xml->createElement('gmd:westBoundLongitude');
        $westBoundLongitude = $EX_GeographicBoundingBox->appendChild($westBoundLongitude);

        $Decimal = $xml->createElement('gco:Decimal', $request['westBoundLongitude']);
        $Decimal = $westBoundLongitude->appendChild($Decimal);

        $eastBoundLongitude = $xml->createElement('gmd:eastBoundLongitude');
        $eastBoundLongitude = $EX_GeographicBoundingBox->appendChild($eastBoundLongitude);

        $Decimal = $xml->createElement('gco:Decimal', $request['eastBoundLongitude']);
        $Decimal = $eastBoundLongitude->appendChild($Decimal);

        $southBoundLatitude = $xml->createElement('gmd:southBoundLatitude');
        $southBoundLatitude = $EX_GeographicBoundingBox->appendChild($southBoundLatitude);

        $Decimal = $xml->createElement('gco:Decimal', $request['southBoundLatitude']);
        $Decimal = $southBoundLatitude->appendChild($Decimal);

        $northBoundLatitude = $xml->createElement('gmd:northBoundLatitude');
        $northBoundLatitude = $EX_GeographicBoundingBox->appendChild($northBoundLatitude);

        $Decimal = $xml->createElement('gco:Decimal', $request['northBoundLatitude']);
        $Decimal = $northBoundLatitude->appendChild($Decimal);

        $supplementalInformation = $xml->createElement('gmd:supplementalInformation');
        $supplementalInformation = $MD_DataIdentification->appendChild($supplementalInformation);

        $CharacterString = $xml->createElement('gco:CharacterString', $request['supplementalInformation']);
        $CharacterString = $supplementalInformation->appendChild($CharacterString);

        $distributionInfo = $xml->createElement('gmd:distributionInfo');
        $distributionInfo = $root->appendChild($distributionInfo);

        $MD_Distribution = $xml->createElement('gmd:MD_Distribution');
        $MD_Distribution = $distributionInfo->appendChild($MD_Distribution);

        $distributionFormat = $xml->createElement('gmd:distributionFormat');
        $distributionFormat = $MD_Distribution->appendChild($distributionFormat);

        $MD_Format = $xml->createElement('gmd:MD_Format');
        $MD_Format = $distributionFormat->appendChild($MD_Format);

        $name = $xml->createElement('gmd:name');
        $name = $MD_Format->appendChild($name);

        $CharacterString = $xml->createElement('gco:CharacterString', $request['name']);
        $CharacterString = $name->appendChild($CharacterString);

        $version = $xml->createElement('gmd:version');
        $version = $MD_Format->appendChild($version);

        $CharacterString = $xml->createElement('gco:CharacterString', $request['version']);
        $CharacterString = $version->appendChild($CharacterString);

        $transferOptions = $xml->createElement('gmd:transferOptions');
        $transferOptions = $MD_Distribution->appendChild($transferOptions);

        $MD_DigitalTransferOptions = $xml->createElement('gmd:MD_DigitalTransferOptions');
        $MD_DigitalTransferOptions = $transferOptions->appendChild($MD_DigitalTransferOptions);

        $onLine = $xml->createElement('gmd:onLine');
        $onLine = $MD_DigitalTransferOptions->appendChild($onLine);

        $CI_OnlineResource = $xml->createElement('gmd:CI_OnlineResource');
        $CI_OnlineResource = $onLine->appendChild($CI_OnlineResource);

        $linkage = $xml->createElement('gmd:linkage');
        $linkage = $CI_OnlineResource->appendChild($linkage);

        $URL = $xml->createElement('gmd:URL', $request['URL']);
        $URL = $linkage->appendChild($URL);

        $description = $xml->createElement('gmd:description');
        $description = $CI_OnlineResource->appendChild($description);

        $CharacterString = $xml->createElement('gco:CharacterString', $request['description']);
        $CharacterString = $description->appendChild($CharacterString);

        $dataQualityInfo = $xml->createElement('gmd:dataQualityInfo');
        $dataQualityInfo = $root->appendChild($dataQualityInfo);

        $DQ_DataQuality = $xml->createElement('gmd:DQ_DataQuality');
        $DQ_DataQuality = $dataQualityInfo->appendChild($DQ_DataQuality);

        $scope = $xml->createElement('gmd:scope');
        $scope = $DQ_DataQuality->appendChild($scope);

        $DQ_Scope = $xml->createElement('gmd:DQ_Scope');
        $DQ_Scope = $scope->appendChild($DQ_Scope);

        $level = $xml->createElement('gmd:level');
        $level = $DQ_Scope->appendChild($level);

        $MD_ScopeCode = $xml->createElement('gmd:MD_ScopeCode');
        $MD_ScopeCode->setAttribute('codeListValue', $request['ScopeCode']);
        $MD_ScopeCode->setAttribute('codeList', 'http://www.isotc211.org/2005/resources/codeList.xml#MD_ScopeCode');
        $MD_ScopeCode = $level->appendChild($MD_ScopeCode);

        $lineage = $xml->createElement('gmd:lineage');
        $lineage = $DQ_DataQuality->appendChild($lineage);

        $LI_Lineage = $xml->createElement('gmd:LI_Lineage');
        $LI_Lineage = $lineage->appendChild($LI_Lineage);

        $statement = $xml->createElement('gmd:statement');
        $statement = $LI_Lineage->appendChild($statement);

        $CharacterString = $xml->createElement('gco:CharacterString', $request['Statement']);
        $CharacterString = $statement->appendChild($CharacterString);
        
        $estee = $xml->saveXML();
        $este = $this->acentos(utf8_encode($estee));
        return $este;
    }
    
    function acentos($cadena) {
        $search = explode(",", "á,é,í,ó,ú,ñ,Á,É,Í,Ó,Ú,Ñ,Ã¡,Ã©,Ã­,Ã³,Ãº,Ã±,ÃÃ¡,ÃÃ©,ÃÃ­,ÃÃ³,ÃÃº,ÃÃ±,Ã“,Ã ,Ã‰,Ã ,Ãš,â€œ,â€ ,Â¿,ü,&#xE1;,&#xF1;,&#xF3;,&#xE9;,&#xED;,&#xFA;,&#xFC;,&#xC1;,&#xC9;,&#xCD;,&#xD3;,&#xDA;,&#xDC;,&#xD1;");
        $replace = explode(",", "á,é,í,ó,ú,ñ,Á,É,Í,Ó,Ú,Ñ,á,é,í,ó,ú,ñ,Á,É,Í,Ó,Ú,Ñ,Ó,Á,É,Í,Ú,\",\",¿,&uuml,á,ñ,ó,é,í,ú,ü,Á,É,Í,Ó,Ú,Ü,Ñ");
        $cadena = str_replace($search, $replace, $cadena);

        return $cadena;
    }
    
    /**
    * Creates a form to create a entity.
    * @param array $config
    * @param $entity The entity
    * @return \Symfony\Component\Form\Form The form
    */
    public function createFsForm($config, $entity)
    {
        $form = $this->createForm($config['newType'], $entity, array(
            'action' => $this->generateUrl($config['create']),
            'method' => 'POST',
        ));

        $form
            ->add(
                'save', 'submit', array(
                'translation_domain' => 'MWSimpleAdminCrudBundle',
                'label'              => 'views.new.save',
                'attr'               => array(
                    'class' => 'form-control btn-success',
                    'col'   => 'col-lg-2',
                )
                )
            )            
        ;

        return $form;
    }    
}
