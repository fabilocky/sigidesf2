<?php

namespace Sistema\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\UserBundle\Entity\User;
use Sistema\UserBundle\Form\UserType;
use Sistema\UserBundle\Form\RegistroType;
use Sistema\UserBundle\Form\ForgotPasswordType;
use Sistema\UserBundle\Form\UserEditType;
use Sistema\UserBundle\Form\UserFilterType;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * User controller.
 * @author Fabian Serafini <name@gmail.com>
 *
 * @Route("/registro")
 */
class RegistroController extends Controller
{
    /**
     * Displays a form to create a new User entity.
     *
     * @Route("/", name="user_register")
     * @Method("GET")
     * @Template()
     */
    public function registerAction()
    {
        //$this->get('security_role')->controlRolesUser();
        $entity = new User();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }
    
    /**
    * Creates a form to create a User entity.
    *
    * @param User $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(User $entity)
    {
        $form = $this->createForm(new RegistroType(), $entity, array(
            'action' => $this->generateUrl('register_create'),
            'method' => 'POST',
        ));

        $form
            ->add(
                'save', 'submit', array(
                'translation_domain' => 'MWSimpleAdminCrudBundle',
                'label'              => 'Registrarme',
                'attr'               => array('class' => 'col-lg-2 btn btn-success')
                )
            )            
        ;

        return $form;
    }
    
    /**
     * Creates a new User entity.
     *
     * @Route("/", name="register_create")
     * @Method("POST")
     * @Template("SistemaUserBundle:Registro:register.html.twig")
     */
    public function createAction(Request $request)
    {   
        
        $entity = new User();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $password = $form->getData()->getPassword();
        if ($form->isValid()) {

            $this->setSecurePassword($entity);            
            $em = $this->getDoctrine()->getManager();
            $rol_user = $em->getRepository("SistemaUserBundle:Role")->findOneByName("ROLE_USER");
            $config = $em->getRepository("SistemaAdminBundle:Config")->findAll();
            $entity->addRole($rol_user);
            $em->persist($entity);
            $em->flush();
             $message = \Swift_Message::newInstance()
                ->setSubject('Bienvenido a '. $config[0]->getTitulo())
                ->setFrom(array($config[0]->getMail() => $config[0]->getTitulo()))
                ->setTo($entity->getEmail())
                ->setBody(
                    $this->renderView(                
                        'SistemaUserBundle:Registro:mail.html.twig', 
                        array(
                            'name' => $entity->getNombre(),
                            'titulo' => $config[0]->getTitulo(),
                            'usuario' => $form->getData()->getUsername(),
                            'password' => $password,
                            )
                    ),                    
                    'text/html'
                )       
            ;
            $this->get('mailer')->send($message);
            $this->get('session')->getFlashBag()->add('success', 'Su Registro ha sido exitoso, inicie sesion con su nuevo usuario y contraseña');

            $nextAction = $this->generateUrl('login');                    

            return $this->redirect($nextAction);

        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }
    
    private function setSecurePassword($entity)
    {
        $factory = $this->get('security.encoder_factory');
        $entity->setSalt(md5(time()));
        $encoder = $factory->getEncoder($entity);
        $password = $encoder->encodePassword($entity->getPassword(), $entity->getSalt());
        $entity->setPassword($password);
    }
    
    /**
     * Displays a form to create a new User entity.
     *
     * @Route("/forgotpass", name="user_forgot_password")
     * @Method("GET")
     * @Template()
     */
    public function forgotPasswordAction()
    {
        //$this->get('security_role')->controlRolesUser();
//        $entity = new User();
        $form = $this->createForm(new ForgotPasswordType(), null, array(
            'action' => $this->generateUrl('forgot_create'),
            'method' => 'POST',
        ));

        $form
            ->add(
                'save', 'submit', array(
                'translation_domain' => 'MWSimpleAdminCrudBundle',
                'label'              => 'Recuperar',
                'attr'               => array('class' => 'col-lg-2 btn btn-success')
                )
            )            
        ;

        return array(            
            'form'   => $form->createView(),
        );
    }
    
    /**
     * Creates a new User entity.
     *
     * @Route("/forgotpass/", name="forgot_create")
     * @Method("POST")
     * @Template("SistemaUserBundle:Registro:register.html.twig")
     */
    public function forgotCreateAction(Request $request)
    {
        $email = $request->get('sistema_userbundle_forgot_password')['email'];
        //ladybug_dump($email);die();
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository("SistemaUserBundle:User")->findOneByEmail($email);
        if (!$user){
            $this->get('session')->getFlashBag()->add('danger', 'No Existe un usuario registrado con ese E-mail');
            $nextAction = $this->generateUrl('user_forgot_password');
            return $this->redirect($nextAction);
        }
        $user->setPassword($user->getUsername());
        $this->setSecurePassword($user);            
        $em = $this->getDoctrine()->getManager();            
        $config = $em->getRepository("SistemaAdminBundle:Config")->findAll();            
        $em->persist($user);
        $em->flush();
         $message = \Swift_Message::newInstance()
            ->setSubject('Recuperación de contraseña de '. $config[0]->getTitulo())
            ->setFrom(array($config[0]->getMail() => $config[0]->getTitulo()))
            ->setTo($user->getEmail())
            ->setBody(
                $this->renderView(                
                    'SistemaUserBundle:Registro:forgot_mail.html.twig', 
                    array(
                        'name' => $user->getNombre(),
                        'titulo' => $config[0]->getTitulo(),
                        'usuario' => $user->getUsername(),
                        'password' => $user->getUsername(),
                        )
                ),                    
                'text/html'
            )       
        ;
        $this->get('mailer')->send($message);
        $this->get('session')->getFlashBag()->add('success', 'Su Contraseña se recuperó exitosamente, inicie sesion con su nueva contraseña');

        $nextAction = $this->generateUrl('login');                    

        return $this->redirect($nextAction);
        
        
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }
}