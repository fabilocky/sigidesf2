<?php

namespace Sistema\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AtributoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', 'text', array(
                'constraints' => array(new \Symfony\Component\Validator\Constraints\NotBlank()),
                "attr" => array(
                    "data-bv-notempty" => "true",
                    "required" => "required",
                ),
            ))
            ->add('tipo', 'choice', array(                
                'choices'  => array(
                    'text' => 'texto',
                    'date'     => 'fecha',
                    'number'     => 'numero',
                )                   
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'cascade_validation' => true,
        ));
    }

    public function getName()
    {
        return 'atributo';
    }
}