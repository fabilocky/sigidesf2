<?php

namespace Sistema\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * UserType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class ForgotPasswordType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder    
            ->add('email', null, array(
                'label' => 'E-mail',
                'label_attr' => array(
                    'class' => 'form-label col-lg-3',
                ),
                'attr'  => array(
                    'autofocus' => 'autofocus',
                    'class' => 'form-control',
                    "data-bv-notempty" => "true",
                    "data-bv-emailaddress" => "true"
                )                
            ));    
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            //'data_class' => 'Sistema\UserBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_userbundle_forgot_password';
    }
}
