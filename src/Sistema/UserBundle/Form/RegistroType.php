<?php

namespace Sistema\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Gregwar\CaptchaBundle\Type\CaptchaType;

/**
 * UserType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class RegistroType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', null, array(
                'label' => 'Nombre',
                'label_attr' => array(
                    'class' => 'form-label col-lg-3',
                ),
                'attr'  => array(
                    'autofocus' => 'autofocus',
                    'class' => 'form-control'
                )
            ))
            ->add('apellido', null, array(
                'label' => 'Apellido',
                'label_attr' => array(
                    'class' => 'form-label col-lg-3',
                ),
                'attr'  => array(
                    'autofocus' => 'autofocus',
                    'class' => 'form-control'
                )
            ))
            ->add('email', null, array(
                'label' => 'E-mail',
                'label_attr' => array(
                    'class' => 'form-label col-lg-3',
                ),
                'attr'  => array(
                    'autofocus' => 'autofocus',
                    'class' => 'form-control'
                )
            ))
            ->add('username', null, array(
                'label' => 'Usuario',
                'label_attr' => array(
                    'class' => 'form-label col-lg-3',
                ),
                'attr'  => array(
                    'autofocus' => 'autofocus',
                    'class' => 'form-control'
                )
            ))
            ->add('password', 'repeated', array(
                'type'            => 'password',
                'required'        => true,
                'invalid_message' => 'Las Contraseñas deben Coincidir.',
                'options'         => array(
                    'attr' => array(
                        'class' => 'password-field'
                    )
                ),
                'first_options'  => array(
                    'label' => 'Contraseña',
                    'label_attr' => array(
                        'class' => 'form-label col-lg-3',
                    ),
                    'attr'  => array(
                        'class' => 'form-control'
                    )
                ),
                'second_options' => array(
                    'label' => 'Repetir',
                    'label_attr' => array(
                        'class' => 'form-label col-lg-3',
                    ),
                    'attr'  => array(
                        'class' => 'form-control'
                    )
                ),
            ))
            ->add('dni', null, array(                
                'label' => 'DNI',
                'label_attr' => array(
                    'class' => 'form-label col-lg-3',
                ),
                'attr'  => array(                    
                    'autofocus' => 'autofocus',
                    'class' => 'form-control'
                )
            ))            
            ->add('fechanac', 'bootstrapdatetime', array(
                'required'   => true,                
                'label'      => 'Fecha Nacimiento',                
                'label_attr' => array(
                    'class' => 'form-label col-lg-3',
                ),
                'attr'  => array(                    
                    'class' => 'form-control'
                )
            ))
            ->add('telefono', null, array(
                'label' => 'Telefono',
                'label_attr' => array(
                    'class' => 'form-label col-lg-3',
                ),
                'attr'  => array(
                    'autofocus' => 'autofocus',
                    'class' => 'form-control'
                )
            ))
        ;
        $builder->add('captcha', 'captcha', array(
                'label' => 'Captcha',
                'label_attr' => array(
                    'class' => 'form-label col-lg-3',
                ),
                'attr'  => array(
                    'autofocus' => 'autofocus',
                    'class' => 'form-control'
                )
            ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\UserBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_userbundle_user';
    }
}
