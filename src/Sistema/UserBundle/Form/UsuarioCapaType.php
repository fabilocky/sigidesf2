<?php

namespace Sistema\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * UsuarioCapaType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class UsuarioCapaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')            
            ->add('capaFile1', 'vich_file', array(
            'label' => 'shp',
            'required'      => true,
            'allow_delete'  => true, // not mandatory, default is true
            'download_link' => true, // not mandatory, default is true
            ))
            ->add('capaFile2', 'vich_file', array(
            'label' => 'shx',
            'required'      => true,
            'allow_delete'  => true, // not mandatory, default is true
            'download_link' => true, // not mandatory, default is true
            ))
            ->add('capaFile3', 'vich_file', array(
            'label' => 'dbf',
            'required'      => true,
            'allow_delete'  => true, // not mandatory, default is true
            'download_link' => true, // not mandatory, default is true
            ))
            ->add('usuario',null,array(
                'attr'=>array('style'=>'display:none;'),
                'label_attr' => array(
                    'style'=>'display:none;'
                ),
                ) )
            ->add('categoria', 'select2', array(
                'class' => 'Sistema\AdminBundle\Entity\Categoria',
                'url'   => 'Capa_autocomplete_categoria',
                'configs' => array(
                    'multiple' => true,//required true or false
                    'width'    => 'off',
                ),
                'attr' => array(
                    'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                )
            ))
//            ->add('updatedAt', 'bootstrapdatetime', array(
//                'required'   => true,
//                'label'      => 'Updatedat',
//                'label_attr' => array(
//                    'class' => 'col-lg-2 col-md-2 col-sm-2',
//                ),
//                'widget_type' => 'both',
//            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\UserBundle\Entity\UsuarioCapa'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_userbundle_usuariocapa';
    }
}
