<?php

namespace Sistema\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * UserType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class UserEditPerfilType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', null, array(
                'label' => 'Nombre',
                'label_attr' => array(
                    'class' => 'form-label col-lg-1',
                ),
                'attr'  => array(
                    'autofocus' => 'autofocus',
                    'class' => 'form-control'
                )
            ))
            ->add('apellido', null, array(
                'label' => 'Apellido',
                'label_attr' => array(
                    'class' => 'form-label col-lg-1',
                ),
                'attr'  => array(
                    'autofocus' => 'autofocus',
                    'class' => 'form-control'
                )
            ))
            ->add('email', null, array(
                'label' => 'E-mail',
                'label_attr' => array(
                    'class' => 'form-label col-lg-1',
                ),
                'attr'  => array(
                    'autofocus' => 'autofocus',
                    'class' => 'form-control'
                )
            ))
            ->add('username', null, array(
                'label' => 'Usuario',
                'label_attr' => array(
                    'class' => 'form-label col-lg-1',
                ),
                'attr'  => array(
                    'autofocus' => 'autofocus',
                    'class' => 'form-control'
                )
            ))
            ->add('password', 'repeated', array(
                'type'            => 'password',
                'required'        => false,
                'invalid_message' => 'Las Contraseñas deben Coincidir.',
                'options'         => array(
                    'attr' => array(
                        'class' => 'password-field'
                    )
                ),
                'first_options'  => array(
                    'label' => 'Contraseña',
                    'label_attr' => array(
                        'class' => 'form-label col-lg-1',
                    ),
                    'attr'  => array(
                        'class' => 'form-control'
                    )
                ),
                'second_options' => array(
                    'label' => 'Repetir',
                    'label_attr' => array(
                        'class' => 'form-label col-lg-1',
                    ),
                    'attr'  => array(
                        'class' => 'form-control'
                    )
                ),
            ))
            ->add('dni', null, array(
                'label' => 'DNI',
                'label_attr' => array(
                    'class' => 'form-label col-lg-1',
                ),
                'attr'  => array(
                    'autofocus' => 'autofocus',
                    'class' => 'form-control'
                )
            ))
            ->add('fechanac', 'bootstrapdatetime', array(
                'required'   => true,                
                'label'      => 'Fecha Nacimiento',                
                'label_attr' => array(
                    'class' => 'form-label col-lg-1',
                ),
                'attr'  => array(                    
                    'class' => 'form-control'
                )
            ))
            ->add('telefono', null, array(
                'label' => 'Telefono',
                'label_attr' => array(
                    'class' => 'form-label col-lg-1',
                ),
                'attr'  => array(
                    'autofocus' => 'autofocus',
                    'class' => 'form-control'
                )
            ))
//            ->add('user_roles', 'select2', array(
//                'label' => 'Permisos',
//                'class' => 'Sistema\UserBundle\Entity\Role',
//                'url'   => 'ReglaPermiso_autocomplete_permiso',
//                'configs' => array(
//                    'multiple' => true,//required true or false
//                    'width'    => 'off',
//                ),
//                'attr' => array(
//                    'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
//                ),                
//            ))
//            ->add('isActive', null, array(
//                'label' => 'Activo',
//                'label_attr' => array(
//                    'class' => 'form-label col-lg-1',
//                ),
//                "disabled" => true
//            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\UserBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_userbundle_user';
    }
}
