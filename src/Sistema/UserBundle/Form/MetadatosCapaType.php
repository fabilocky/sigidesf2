<?php

namespace Sistema\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * MetadatosType form.
 * @author Fabian Serafini <fdserafini@gmail.com>
 */
class MetadatosCapaType extends AbstractType
{
    protected $user;

    public function __construct ($user = null, $capa = null)
    {
            $this->user = $user ;
            $this->capa = $capa ;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('usuariocapa',null, array(
                'attr'=>array('style'=>'display:none;'),
                'label_attr' => array(
                    'style'=>'display:none;'
                ),
                    ))
            ->add('fileIdentifier', null, array(
                "mapped" => false,
                "label" => "Identificador del Archivo de Metadatos",
                "data" => sha1(uniqid(mt_rand(), true)),
                "attr" => array(
                    "data-bv-notempty" => "true",                  
                ),
                "read_only" => true
            ))
            ->add('LanguageCode', "choice", array(
                "mapped" => false,
                "label" => "Idioma del Conjunto de Datos",
                'choices' => array('spa' => 'español', 'eng' => 'inglés', 'por' => 'portugués'),
            ))
            ->add('CharacterSetCode', 'choice', array(
                "mapped" => false,
                "label" => "Juego de Caracteres del Conjunto de Datos",
                'choices' => array('UTF8' => 'UTF8', 'LATIN1' => 'LATIN1'),
            ))
            ->add('individualName', null, array(
                "mapped" => false,
                "label" => "Nombre Completo Personal",
                "data" => $this->user->getNombre(). " " . $this->user->getApellido(),
                "attr" => array(
                    "data-bv-notempty" => "true",                  
                )
            ))
            ->add('organisationName', null, array(
                "mapped" => false,
                "label" => "Nombre de la Organización",
                "attr" => array(
                    "data-bv-notempty" => "true",                  
                )
            ))
            ->add('positionName', null, array(
                "mapped" => false,
                "label" => "Nombre del Cargo",
                "attr" => array(
                    "data-bv-notempty" => "true",                  
                )
            ))
            ->add('voice', null, array(
                "mapped" => false,
                "label" => "Número de Teléfono de Voz",
                "data" => $this->user->getTelefono(),
                "attr" => array(
                    "data-bv-notempty" => "true",                  
                )
            ))
            ->add('facsimile', null, array(
                "mapped" => false,
                "label" => "Número de Fax",
                "data" => $this->user->getTelefono(),
                "attr" => array(
                    "data-bv-notempty" => "true",                  
                )
            ))
            ->add('deliveryPoint', null, array(
                "mapped" => false,
                "label" => "Dirección",
                "attr" => array(
                    "data-bv-notempty" => "true",                  
                )
            ))
            ->add('city', null, array(
                "mapped" => false,
                "label" => "Ciudad",
                "attr" => array(
                    "data-bv-notempty" => "true",                  
                )
            ))
            ->add('administrativeArea', null, array(
                "mapped" => false,
                "label" => "Provincia o Area Administrativa",
                "attr" => array(
                    "data-bv-notempty" => "true",                  
                )
            ))
            ->add('postalCode', null, array(
                "mapped" => false,
                "label" => "Código Postal",
                "attr" => array(
                    "data-bv-notempty" => "true",                  
                )
            ))
            ->add('country', null, array(
                "mapped" => false,
                "label" => "País",
                "attr" => array(
                    "data-bv-notempty" => "true",                  
                )
            ))
            ->add('electronicMailAddress', null, array(
                "mapped" => false,
                "label" => "Dirección Electrónica",
                "data" => $this->user->getEmail(),
                "attr" => array(
                    "data-bv-notempty" => "true",
                    "data-bv-emailaddress" => "true"
                )
            ))
            ->add('CI_RoleCode', 'choice', array(
                "mapped" => false,
                "label" => "Rol del punto de Contacto",
                'choices' => array(
                                 'resourceProvider' => 'Proveedor del recurso', 'custodian' => 'Custodio',
                                            'owner' => 'Propietario', 'user' => 'Usuario',
                                      'distributor' => 'Distribuidor',
                                       'originator' => 'Creador', 'pointOfContact' => 'Punto de contacto',
                               'principalInvestigator' => 'Investigador principal', 'processor' => 'Procesador',
                                         'publisher' => 'Editor', 'author' => 'Autor',
                          ),
            ))
            ->add('dateStamp', null, array(
                "mapped" => false,
                "label" => "Fecha de creación de los metadatos",
                'data' => date("Y-m-d") . 'T' . date("H:i:s"),
                "attr" => array(
                    "data-bv-notempty" => "true",                  
                )
            ))
            ->add('metadataStandardName', null, array(
                "mapped" => false,
                "label" => "Nombre de la Norma de Metadatos",
                "data" => "ISO 19115:2003/19139:2007",
                "attr" => array(
                    "data-bv-notempty" => "true",                  
                )
            ))
            ->add('metadataStandardVersion', null, array(
                "mapped" => false,
                "label" => "Versión de la Norma de Metadatos",
                "data" => "1.1",
                "attr" => array(
                    "data-bv-notempty" => "true",                  
                )
            ))
            ->add('code', null, array(
                "mapped" => false,
                "label" => "Código del Sistema de Referencia",
                "data" => "WGS 1984",
                "attr" => array(
                    "data-bv-notempty" => "true",                  
                )
            ))
            ->add('title', null, array(
                "mapped" => false,
                'label'      => 'Título del conjunto de datos',
                "data" => $this->capa->getNombre(),
                "attr" => array(
                    "data-bv-notempty" => "true",                  
                )
            ))
            ->add('date', null, array(
                "mapped" => false,
                'label'      => 'Fecha',
                'data' => date("Y-m-d") . 'T' . date("H:i:s"),
                "attr" => array(
                    "data-bv-notempty" => "true",                  
                )
            ))
            ->add('CI_DateTypeCode', 'choice', array(
                "mapped" => false,
                'label'      => 'Tipo de Fecha',
                'choices'  => array(                    
                    'publication' => 'Publicación',
                    'creation' => 'Creación', 
                    'revision' => 'Revisión'
                )   
            ))
            ->add('abstract', 'textarea', array(
                "mapped" => false,
                'label'      => 'Resumen del Conjunto de Datos',
                "attr" => array(
                    "data-bv-notempty" => "true",                    
                    "placeholder" => "Escriba un breve resumen sobre los datos"
                ) 
            ))
            ->add('URL', null, array(
                "mapped" => false,
                'label'      => 'URL',
                "attr" => array(
                    "data-bv-notempty" => "true",
                    'data-bv-uri' => "true",
                    "type" => "url",
                    "placeholder" => "http://"
                ) 
            ))           
            ->add('name', null, array(
                "mapped" => false,
                "label" => "Nombre",
                "data" => $this->capa->getNombre(),
                "attr" => array(
                    "data-bv-notempty" => "true",                  
                )
            ))
            ->add('description', null, array(
                "mapped" => false,
                'label'      => 'Descripción',
                "attr" => array(
                    "data-bv-notempty" => "true",                  
                )
            ))    
            ->add('keyword', null, array(
                "mapped" => false,
                "label" => "Palabras clave",
                "attr" => array(
                    "data-bv-notempty" => "true",                  
                ) 
            ))
            ->add('KeywordTypeCode', 'choice', array(
                "mapped" => false,
                "label" => "Tipo de Palabra clave",
                'choices'  => array(                    
                    'theme' => 'Tema', 'discipline' => 'Disciplina', 'place' => 'Lugar', 'stratum' => 'Estrato',
                               'temporal' => 'Temporal' 
                )
            ))
            ->add('SpatialRepresentationTypeCode', 'choice', array(
                "mapped" => false,
                "label" => "Tipo de Representación Espacial",
                'choices'  => array(                    
                    'vector' => 'Vector', 'grid' => 'Cuadrícula', 'textTable' => 'Tabla de texto',
                                     'tin' => 'TIN', 'stereoModel' => 'Modelo estéreo', 'video' => 'Video',
                )
            ))
            ->add('denominator', null, array(
                "mapped" => false,
                "label" => "Denominador",
                "attr" => array(
                    "data-bv-notempty" => "true",
                    "data-bv-integer" => "true",
                )
            ))
            ->add('TopicCategoryCode', 'choice', array(
                "mapped" => false,
                "label" => "Tema del Conjunto de Datos",
                'choices' => array(
                                                        'farming' => 'Agrícola', 'biota' => 'Biota',
                                                     'boundaries' => 'Límites',
                               'climatologyMeteorologyAtmosphere' => 'Climatología, meteorología, atmósfera',
                                                        'economy' => 'Economía', 'elevation' => 'Elevación', 
                                                    'environment' => 'Medio ambiente',
                                                 'Medio ambiente' => 'Información geocientífica', 'health' => 'Salud',
                                      'imageryBaseMapsEarthCover' => 'Imágenes, mapas base, coberturas de la tierra', 
                                           'intelligenceMilitary' => 'Inteligencia militar',
                                                   'inlandWaters' => 'Características del agua',
                                                       'location' => 'Localización', 
                                                         'oceans' => 'Océanos', 
                                               'planningCadastre' => 'Planificación de catastro', 
                                                        'society' => 'Sociedad', 'structure' => 'Construcción',
                                                 'transportation' => 'Transporte', 
                                         'utilitiesCommunication' => 'Servicios públicos de comunicación',
                           ),
            ))
            ->add('extent', null, array(
                "mapped" => false,
                "label" => "Nombre Geográfico",
                "attr" => array(
                    "data-bv-notempty" => "true",                  
                )
            ))
            ->add('extent_begin', 'bootstrapdatetime', array(
                "required" => true,
                "mapped" => false,
                "label" => "Desde",
                'widget_type' => 'date',
                'format'        => 'yyyy-MM-dd',
                'data' => new \DateTime("today")
//                "attr" => array(
//                    "data-bv-notempty" => "true",                  
//                )
                //'widget' => 'single_text',
            ))
            ->add('extent_end', 'bootstrapdatetime', array(
                "required" => true,
                "mapped" => false,
                "label" => "Hasta",
                'widget_type' => 'date',
                'format'        => 'yyyy-MM-dd',
                'data' => new \DateTime('+1 years')
//                "attr" => array(
//                    "data-bv-notempty" => "true",                  
//                )
                //'widget' => 'single_text',
            ))
            ->add('westBoundLongitude', null, array(
                "mapped" => false,
                "label" => "Longitud Oeste",
                "attr" => array(
                    "data-bv-notempty" => "true",                  
                )
            ))
            ->add('eastBoundLongitude', null, array(
                "mapped" => false,
                "label" => "Longitud Este",
                "attr" => array(
                    "data-bv-notempty" => "true",                  
                )
            ))
            ->add('southBoundLatitude', null, array(
                "mapped" => false,
                "label" => "Latitud Sur",
                "attr" => array(
                    "data-bv-notempty" => "true",                  
                )
            ))
            ->add('northBoundLatitude', null, array(
                "mapped" => false,
                "label" => "Latitud Norte",
                "attr" => array(
                    "data-bv-notempty" => "true",                  
                )
            ))
            ->add('supplementalInformation', 'textarea', array(
                "mapped" => false,
                "label" => "Información Complementaria",
                "attr" => array(
                    "data-bv-notempty" => "true",
                    "placeholder" => "Escriba informacion adicional sobre los datos"
                )
            ))
            ->add('ScopeCode', 'choice', array(
                "mapped" => false,
                "label" => "Nivel Jerárquico",
                'choices' => array(
                                          'attribute' => 'Atributo', 'attributeType' => 'Tipo de atributo', 
                                 'collectionHardware' => 'Hardware de la recolección', 
                                  'collectionSession' => 'Sesión de la colección', 
                                            'dataset' => 'Conjunto de datos', 'series' => 'Series', 
                               'nonGeographicDataset' => 'Conjunto de datos no geográficos', 
                                     'dimensionGroup' => 'Grupo de la dimensión', 'feature' => 'Objetos', 
                                        'featureType' => 'Tipos de objetos', 'propertyType' => 'Tipos de propiedad', 
                                       'fieldSession' => 'Sesión del campo', 'software' => 'Software', 
                                            'service' => 'Servicio', 
                                              'model' => 'Modelo', 'tile' => 'Subconjunto', 'dataset' => 'Video',
                               ),
            ))
            ->add('Statement', 'textarea', array(
                "mapped" => false,
                "label" => "Declaración del Linaje",
                "attr" => array(
                    "data-bv-notempty" => "true",                  
                )
            ))
            ->add('version', null, array(
                "mapped" => false,
                "label" => "Versión",
                "attr" => array(
                    "data-bv-notempty" => "true",                  
                )
            ))            
            ->add('LanguageCode_MD', 'choice', array(
                "mapped" => false,
                "label" => "Idioma de los Metadatos",
                'choices' => array('spa' => 'español', 'eng' => 'inglés', 'por' => 'portugués'),
            ))
            ->add('CharacterSetCode_MD', 'choice', array(
                "mapped" => false,
                "label" => "Conjunto de Caracteres de los Metadatos",
                'choices' => array('UTF8' => 'UTF8', 'LATIN1' => 'LATIN1'),
            ))            
            ->add('individualName_MD', null, array(
                "mapped" => false,
                "label" => "Nombre Completo Personal",
                "data" => $this->user->getNombre(). " " . $this->user->getApellido(),
                "attr" => array(
                    "data-bv-notempty" => "true",                  
                )
            ))
            ->add('organisationName_MD', null, array(
                "mapped" => false,
                "label" => "Nombre de la Organización",
                "attr" => array(
                    "data-bv-notempty" => "true",                  
                )
            ))
            ->add('positionName_MD', null, array(
                "mapped" => false,
                "label" => "Nombre del Cargo",
                "attr" => array(
                    "data-bv-notempty" => "true",                  
                )
            ))
            ->add('voice_MD', null, array(
                "mapped" => false,
                "label" => "Número de Teléfono de Voz",
                "data" => $this->user->getTelefono(),
                "attr" => array(
                    "data-bv-notempty" => "true",                  
                )
            ))
            ->add('facsimile_MD', null, array(
                "mapped" => false,
                "label" => "Número de Fax",
                "data" => $this->user->getTelefono(),
                "attr" => array(
                    "data-bv-notempty" => "true",                  
                )
            ))
            ->add('deliveryPoint_MD', null, array(
                "mapped" => false,
                "label" => "Dirección",
                "attr" => array(
                    "data-bv-notempty" => "true",                  
                )
            ))
            ->add('city_MD', null, array(
                "mapped" => false,
                "label" => "Ciudad",
                "attr" => array(
                    "data-bv-notempty" => "true",                  
                )
            ))
            ->add('administrativeArea_MD', null, array(
                "mapped" => false,
                "label" => "Provincia o Area Administrativa",
                "attr" => array(
                    "data-bv-notempty" => "true",                  
                )
            ))
            ->add('postalCode_MD', null, array(
                "mapped" => false,
                "label" => "Código Postal",
                "attr" => array(
                    "data-bv-notempty" => "true",                  
                )
            ))
            ->add('country_MD', null, array(
                "mapped" => false,
                "label" => "País",
                "attr" => array(
                    "data-bv-notempty" => "true",                  
                )
            ))
            ->add('electronicMailAddress_MD', null, array(
                "mapped" => false,
                "label" => "Dirección Electrónica",
                "data" => $this->user->getEmail(),
                "attr" => array(
                    "data-bv-notempty" => "true",
                    "data-bv-emailaddress" => "true"
                )
            ))
            ->add('CI_RoleCode_MD', 'choice', array(
                "mapped" => false,
                "label" => "Rol del punto de Contacto",
                'choices' => array(
                                 'resourceProvider' => 'Proveedor del recurso', 'custodian' => 'Custodio',
                                            'owner' => 'Propietario', 'user' => 'Usuario',
                                      'distributor' => 'Distribuidor',
                                       'originator' => 'Creador', 'pointOfContact' => 'Punto de contacto',
                               'principalInvestigator' => 'Investigador principal', 'processor' => 'Procesador',
                                         'publisher' => 'Editor', 'author' => 'Autor',
                          ),
            ))       
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(            
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_userbundle_metadatos';
    }
}
