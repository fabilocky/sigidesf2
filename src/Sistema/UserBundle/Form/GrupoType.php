<?php

namespace Sistema\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * GrupoType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class GrupoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
//            ->add('owner', 'select2', array(
//                'class' => 'Sistema\UserBundle\Entity\User',
//                'url'   => 'Grupo_autocomplete_owner',
//                'configs' => array(
//                    'multiple' => false,//required true or false
//                    'width'    => 'off',
//                ),
//                'attr' => array(
//                    'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
//                )
//            ))
            ->add('owner',null,array(
                'attr'=>array('style'=>'display:none;'),
                'label_attr' => array(
                    'style'=>'display:none;'
                ),
                ) )
            ->add('usuarios', 'select2', array(
                'class' => 'Sistema\UserBundle\Entity\User',
                'url'   => 'Grupo_autocomplete_usuarios',
                'configs' => array(
                    'multiple' => true,//required true or false
                    'width'    => 'off',
                ),
                'attr' => array(
                    'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                )
            ))
            ->add('mapas', 'select2', array(
                'class' => 'Sistema\UserBundle\Entity\Mapa',
                'url'   => 'Grupo_autocomplete_mapas',
                'configs' => array(
                    'multiple' => true,//required true or false
                    'width'    => 'off',
                ),
                'attr' => array(
                    'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                )
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\UserBundle\Entity\Grupo'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_userbundle_grupo';
    }
}
