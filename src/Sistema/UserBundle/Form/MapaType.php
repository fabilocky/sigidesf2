<?php

namespace Sistema\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * MapaType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class MapaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('geometria', 'choice', array(                
                'choices'  => array(
                    'point' => 'puntos',
                    'path'     => 'lineas',
                    'polygon'     => 'poligonos',
                )                   
))
//            ->add('atributos', 'collection', array(
//        //'type'      => 'email',
//        'prototype' => true,
//        'allow_add' => true,
//        'options'   => array(
//            'required'  => false,
//            //'attr'      => array('class' => 'email-box'),
//        )
//    ))
//            ->add(
//                $builder->create('atributos', 'form', array('by_reference' => false))
//                    ->add('name', 'text')
//                    ->add('tipo', 'text')
//            )
            ->add('atributos', 'collection', array(
                'type' => new AtributoType(),
                'constraints' => array(new \Symfony\Component\Validator\Constraints\Valid()),
                'cascade_validation' => true,
                'prototype' => true,
                'allow_add' => true,
                'allow_delete' => true,
                'options'   => array(
                    'required'  => true,                    
                )
                )
                    )
            ->add('usuario',null,array(
                'attr'=>array('style'=>'display:none;'),
                'label_attr' => array(
                    'style'=>'display:none;'
                ),
                ) )
            ->add('mapaFile', 'vich_file', array(
            'label'      => 'Crear proyecto desde archivo .gpx o .kml',
            'required'      => false,
            'allow_delete'  => true, // not mandatory, default is true
            'download_link' => true, // not mandatory, default is true
            ))            
            ->add('activo')
            //->add('fileName')
//            ->add('updatedAt', 'bootstrapdatetime', array(
//                'required'   => true,
//                'label'      => 'Updatedat',
//                'label_attr' => array(
//                    'class' => 'col-lg-2 col-md-2 col-sm-2',
//                ),
//                'widget_type' => 'both',
//            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\UserBundle\Entity\Mapa',
            'cascade_validation' => true,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_userbundle_mapa';
    }
}
