<?php

namespace Sistema\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Doctrine\ORM\Mapping\OneToOne;

/**
 * Metadatos
 *
 * @ORM\Table()
 * @ORM\Entity
 * @Vich\Uploadable
 */
class Metadatos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\OneToOne(targetEntity="UsuarioCapa", inversedBy="metadato")
     * @ORM\JoinColumn(name="usuariocapa_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $usuariocapa;
    
    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="metadato_file", fileNameProperty="fileName")
     * 
     * @var File
     */
    private $metadatoFile;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $fileName;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $updatedAt;
    
    public function __toString() {
        return $this->getFileName();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Mapa
     */
    public function setMetadatoFile(File $file = null)
    {
        $this->metadatoFile = $file;
        
        if ($file) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return File
     */
    public function getMetadatoFile()
    {
        return $this->metadatoFile;
    }

    /**
     * @param string $fileName
     *
     * @return Mapa
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Mapa
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    
    /**
     * Set usuario
     *
     * @param \Sistema\UserBundle\Entity\User $usuario
     * @return Mapa
     */
    public function setUsuariocapa(\Sistema\UserBundle\Entity\UsuarioCapa $usuariocapa = null)
    {
        $this->usuariocapa = $usuariocapa;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \Sistema\UserBundle\Entity\Usuario 
     */
    public function getUsuariocapa()
    {
        return $this->usuariocapa;
    }
}
