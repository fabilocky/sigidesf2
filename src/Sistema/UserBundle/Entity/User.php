<?php
namespace Sistema\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sistema\AdminBundle\Entity\MWSgedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Table(name="admin_user")
 * @ORM\Entity
 * @UniqueEntity(
 *     fields={"username", "email"},
 *     errorPath="email",
 *     message="Este email se encuentra registrado por otro usuario.",
 *     errorPath="username",
 *     message="Este usuario ya existe en el sistema."     
 * )
 */

class User extends MWSgedmo implements UserInterface, AdvancedUserInterface, \Serializable
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
    * @ORM\Column(type="string", length=255)
    */
    protected $nombre;
    
    /**
    * @ORM\Column(type="string", length=255)
    */
    protected $apellido;
    
    /**
    * @ORM\Column(type="string", length=255)
    */
    protected $email;
    
    /**
    * @ORM\Column(type="string", length=255)
    */
    protected $username;

    /**
     * @ORM\Column(name="password", type="string", length=255)
     */
    protected $password;

    /**
     * @ORM\Column(name="salt", type="string", length=255)
     */
    protected $salt;
    
    /**
    * @ORM\Column(type="integer")
    */
    protected $dni;
    
    /**
    * @ORM\Column(type="date")
    */
    protected $fechanac;
    
    /**
    * @ORM\Column(type="string", length=255, nullable=true)
    */
    protected $telefono;

    /**
     * se utilizó user_roles para no hacer conflicto al aplicar ->toArray en getRoles()
     * @ORM\ManyToMany(targetEntity="Role",cascade={"persist"})
     * @ORM\JoinTable(name="user_role",
     *     joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")}
     * )
     */
    protected $user_roles;

     /**
     * @var boolean $isActive
     *
     * @ORM\Column(type="boolean", nullable=true)
     * @Assert\Type(type="boolean", message="El valor {{ value }} no es un {{ type }} valido.")
     */
    private $isActive;    
    
    /**
     * @var user
     *
     * @ORM\OneToMany(targetEntity="Sistema\UserBundle\Entity\Mapa", mappedBy="usuario")
     */
    private $mapas;
    
    /**
     * @var user
     *
     * @ORM\OneToMany(targetEntity="Sistema\UserBundle\Entity\UsuarioCapa", mappedBy="usuario")
     */
    private $capas;
    
    /**
     * @var user
     *
     * @ORM\OneToMany(targetEntity="Sistema\UserBundle\Entity\Grupo", mappedBy="owner")
     */
    private $grupo;
    
    /**
     * @ORM\ManyToMany(targetEntity="Sistema\UserBundle\Entity\Grupo", mappedBy="usuarios")
     **/
    private $grupos;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->setIsActive(true);
        $this->user_roles = new \Doctrine\Common\Collections\ArrayCollection();        
    }
    
    public function __toString() {
        return $this->username." - ". $this->nombre . " " . $this->apellido;
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set salt
     *
     * @param string $salt
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    /**
     * Get salt
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Add user_roles
     *
     * @param Ssitema\UserBundle\Entity\Role $userRoles
     */
    public function addRole(\Sistema\UserBundle\Entity\Role $userRoles)
    {
        $this->user_roles[] = $userRoles;
    }

    public function setUserRoles($roles)
    {
        $this->user_roles = $roles;
    }

    /**
     * Get user_roles
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getUserRoles()
    {
        return $this->user_roles;

    }

    /**
     * Get roles
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getRoles()
    {        
            return $this->user_roles->toArray(); //IMPORTANTE: el mecanismo de seguridad de Sf2 requiere ésto como un array
          
    }

    /**
     * Compares this user to another to determine if they are the same.
     *
     * @param  UserInterface $user The user
     * @return boolean       True if equal, false othwerwise.
     */
    public function equals(UserInterface $user)
    {
        return md5($this->getUsername()) == md5($user->getUsername());

    }

    /**
     * Erases the user credentials.
     */
    public function eraseCredentials()
    {
    }

     /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        /*
         * ! Don't serialize $roles field !
         */

        return \serialize(array(
            $this->id,
            $this->username,
            $this->salt,
            $this->isActive,
            $this->password
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->salt,
            $this->isActive,
            $this->password
        ) = \unserialize($serialized);
    }

    /**
     * Set isActive
     *
     * @param  boolean $isActive
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Add user_roles
     *
     * @param  \Sistema\UserBundle\Entity\Role $userRoles
     * @return User
     */
    public function addUserRole(\Sistema\UserBundle\Entity\Role $userRoles)
    {
        $this->user_roles[] = $userRoles;

        return $this;
    }

    /**
     * Remove user_roles
     *
     * @param \Sistema\UserBundle\Entity\Role $userRoles
     */
    public function removeUserRole(\Sistema\UserBundle\Entity\Role $userRoles)
    {
        $this->user_roles->removeElement($userRoles);
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return $this->getIsActive();
    }    

    /**
     * Add mapas
     *
     * @param \Sistema\UserBundle\Entity\Mapa $mapas
     * @return User
     */
    public function addMapa(\Sistema\UserBundle\Entity\Mapa $mapas)
    {
        $this->mapas[] = $mapas;

        return $this;
    }

    /**
     * Remove mapas
     *
     * @param \Sistema\UserBundle\Entity\Mapa $mapas
     */
    public function removeMapa(\Sistema\UserBundle\Entity\Mapa $mapas)
    {
        $this->mapas->removeElement($mapas);
    }

    /**
     * Get mapas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMapas()
    {
        return $this->mapas;
    }

    /**
     * Add grupo
     *
     * @param \Sistema\UserBundle\Entity\Grupo $grupo
     * @return User
     */
    public function addGrupo(\Sistema\UserBundle\Entity\Grupo $grupo)
    {
        $this->grupo[] = $grupo;

        return $this;
    }

    /**
     * Remove grupo
     *
     * @param \Sistema\UserBundle\Entity\Grupo $grupo
     */
    public function removeGrupo(\Sistema\UserBundle\Entity\Grupo $grupo)
    {
        $this->grupo->removeElement($grupo);
    }

    /**
     * Get grupo
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGrupo()
    {
        return $this->grupo;
    }

    /**
     * Get grupos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGrupos()
    {
        return $this->grupos;
    }
    
     /**
     * Set nombre
     *
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    
     /**
     * Set apellido
     *
     * @param string $apellido
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;
    }

    /**
     * Get apellido
     *
     * @return string
     */
    public function getApellido()
    {
        return $this->apellido;
    }
    
     /**
     * Set email
     *
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
    
     /**
     * Set dni
     *
     * @param integer $dni
     */
    public function setDni($dni)
    {
        $this->dni = $dni;
    }

    /**
     * Get dni
     *
     * @return integer
     */
    public function getDni()
    {
        return $this->dni;
    }
    
     /**
     * Set fechanac
     *
     * @param date $fechanac
     */
    public function setFechanac($fechanac)
    {
        $this->fechanac = $fechanac;
    }

    /**
     * Get fechanac
     *
     * @return date
     */
    public function getFechanac()
    {
        return $this->fechanac;
    }
    
     /**
     * Set telefono
     *
     * @param string $telefono
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }
}
