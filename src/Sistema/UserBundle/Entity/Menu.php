<?php

namespace Sistema\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Menu
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\UserBundle\Entity\MenuRepository")
 */
class Menu
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $url;
    
    /**
     * @var string
     *
     * @ORM\Column(name="ruta", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $ruta;

    /**
     * @var string
     * @ORM\ManyToMany(targetEntity="Sistema\UserBundle\Entity\Role", cascade={"persist"})
     * @ORM\JoinTable(name="menu_role",
     *     joinColumns={@ORM\JoinColumn(name="menu_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")}
     * )
     */
    private $menu_roles;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activo", type="boolean")
     */
    private $activo;

    /**
     * @var string
     *
     * @ORM\Column(name="orden", type="integer", length=20)
     */
    private $orden;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->menu_roles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->setActivo(true);
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Menu
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    
        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Menu
     */
    public function setUrl($url)
    {
        $this->url = $url;
    
        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }
    
    /**
     * Set ruta
     *
     * @param string $ruta
     * @return Menu
     */
    public function setRuta($ruta)
    {
        $this->ruta = $ruta;
    
        return $this;
    }

    /**
     * Get ruta
     *
     * @return string 
     */
    public function getRuta()
    {
        return $this->ruta;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     * @return Menu
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;
    
        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set orden
     *
     * @param integer $orden
     * @return Menu
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;
    
        return $this;
    }

    /**
     * Get orden
     *
     * @return integer 
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Add menu_roles
     *
     * @param \Sistema\UserBundle\Entity\Role $menuRoles
     * @return Menu
     */
    public function addMenuRole(\Sistema\UserBundle\Entity\Role $menuRoles)
    {
        $this->menu_roles[] = $menuRoles;
    
        return $this;
    }

    /**
     * Remove menu_roles
     *
     * @param \Sistema\UserBundle\Entity\Role $menuRoles
     */
    public function removeMenuRole(\Sistema\UserBundle\Entity\Role $menuRoles)
    {
        $this->menu_roles->removeElement($menuRoles);
    }

    /**
     * Get menu_roles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMenuRoles()
    {
        return $this->menu_roles;
    }
}