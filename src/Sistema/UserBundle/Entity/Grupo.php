<?php

namespace Sistema\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Grupo
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Grupo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;
    
    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToOne(targetEntity="Sistema\UserBundle\Entity\User", inversedBy="grupo")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $owner;    
    
    /**
     * @var \Sistema\UserBundle\Entity\User
     *
     * @ORM\ManyToMany(targetEntity="Sistema\UserBundle\Entity\User", inversedBy="grupos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     * })
     */
    private $usuarios;
    
    /**
     * @var \Sistema\AdminBundle\Entity\Proyecto
     *
     * @ORM\ManyToMany(targetEntity="Sistema\UserBundle\Entity\Mapa", inversedBy="grupos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mapa_id", referencedColumnName="id")
     * })
     */
    private $mapas;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Grupo
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->usuarios = new \Doctrine\Common\Collections\ArrayCollection();
        $this->mapas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set owner
     *
     * @param \Sistema\UserBundle\Entity\User $owner
     * @return Grupo
     */
    public function setOwner(\Sistema\UserBundle\Entity\User $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \Sistema\UserBundle\Entity\User 
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Add usuarios
     *
     * @param \Sistema\UserBundle\Entity\User $usuarios
     * @return Grupo
     */
    public function addUsuario(\Sistema\UserBundle\Entity\User $usuarios)
    {
        $this->usuarios[] = $usuarios;

        return $this;
    }

    /**
     * Remove usuarios
     *
     * @param \Sistema\UserBundle\Entity\User $usuarios
     */
    public function removeUsuario(\Sistema\UserBundle\Entity\User $usuarios)
    {
        $this->usuarios->removeElement($usuarios);
    }

    /**
     * Get usuarios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsuarios()
    {
        return $this->usuarios;
    }

    /**
     * Add mapas
     *
     * @param \Sistema\UserBundle\Entity\Mapa $mapas
     * @return Grupo
     */
    public function addMapa(\Sistema\UserBundle\Entity\Mapa $mapas)
    {
        $this->mapas[] = $mapas;

        return $this;
    }

    /**
     * Remove mapas
     *
     * @param \Sistema\UserBundle\Entity\Mapa $mapas
     */
    public function removeMapa(\Sistema\UserBundle\Entity\Mapa $mapas)
    {
        $this->mapas->removeElement($mapas);
    }

    /**
     * Get mapas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMapas()
    {
        return $this->mapas;
    }
}
