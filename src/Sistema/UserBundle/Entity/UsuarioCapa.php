<?php

namespace Sistema\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Sistema\UserBundle\Validator\Constraints as SistemaAssert;

/**
 * UsuarioCapa
 *
 * @ORM\Table()
 * @ORM\Entity
 * @Vich\Uploadable
 */
class UsuarioCapa
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=50)
     */
    private $nombre;    
    
    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="capa_file", fileNameProperty="shp")
     * 
     * @var File
     * @Assert\File(     
     *     mimeTypes = {"application/octet-stream", "application/x-esri-shape"},
     *     mimeTypesMessage = "Por favor seleccione un archivo valido en shp"
     * )
     * @SistemaAssert\isShp
     */
    private $capaFile1;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string     
     */
    private $shp;
    
    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="capa_file", fileNameProperty="shx")
     * 
     * @var File
     * @Assert\File(     
     *     mimeTypes = {"application/octet-stream", "application/x-esri-shape"},
     *     mimeTypesMessage = "Por favor seleccione un archivo valido en shx"
     * )
     * @SistemaAssert\isShx
     */
    private $capaFile2;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string     
     */
    private $shx;
    
    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="capa_file", fileNameProperty="dbf")
     * 
     * @var File
     * @Assert\File(     
     *     mimeTypes = {"application/octet-stream", "application/x-dbf"},
     *     mimeTypesMessage = "Por favor seleccione un archivo valido en dbf"
     * )
     * @SistemaAssert\isDbf
     */
    private $capaFile3;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string     
     */
    private $dbf;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @ORM\OneToOne(targetEntity="Metadatos", mappedBy="usuariocapa")
     */
    private $metadato;
    
    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=50, nullable=true)
     */
    private $estado;
    
    /**
     * @var string
     *
     * @ORM\Column(name="informe", type="text", nullable=true)
     */
    private $informe;
    
    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToOne(targetEntity="Sistema\UserBundle\Entity\User", inversedBy="capas")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $usuario;
    
    /**
     * @var \Sistema\AdminBundle\Entity\Categoria
     *
     * @ORM\ManyToMany(targetEntity="Sistema\AdminBundle\Entity\Categoria", inversedBy="capasusuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categoria_id", referencedColumnName="id")
     * })
     */
    private $categoria;
    
    /**
     * @ORM\OneToOne(targetEntity="Sistema\AdminBundle\Entity\Capa", inversedBy="usuariocapa")
     * @ORM\JoinColumn(name="capa_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $capa;
    
    public function __toString() {
        return $this->nombre;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return UsuarioCapa
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }    
    
    public function setCapaFile1(File $image = null)
    {
        $this->capaFile1 = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return File
     */
    public function getCapaFile1()
    {
        return $this->capaFile1;
    }

    /**
     * @param string $imageName
     *
     * @return Product
     */
    public function setShp($imageName)
    {
        $this->shp = $imageName;

        return $this;
    }

    /**
     * @return string
     */
    public function getShp()
    {
        return $this->shp;
    }
    
    public function setCapaFile2(File $image = null)
    {
        $this->capaFile2 = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return File
     */
    public function getCapaFile2()
    {
        return $this->capaFile2;
    }

    /**
     * @param string $imageName
     *
     * @return Product
     */
    public function setShx($imageName)
    {
        $this->shx = $imageName;

        return $this;
    }

    /**
     * @return string
     */
    public function getShx()
    {
        return $this->shx;
    }
    
    public function setCapaFile3(File $image = null)
    {
        $this->capaFile3 = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return File
     */
    public function getCapaFile3()
    {
        return $this->capaFile3;
    }

    /**
     * @param string $imageName
     *
     * @return Product
     */
    public function setDbf($imageName)
    {
        $this->dbf = $imageName;

        return $this;
    }

    /**
     * @return string
     */
    public function getDbf()
    {
        return $this->dbf;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return UsuarioCapa
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set metadato
     *
     * @param \Sistema\UserBundle\Entity\Metadatos $metadato
     * @return UsuarioCapa
     */
    public function setMetadato(\Sistema\UserBundle\Entity\Metadatos $metadato = null)
    {
        $this->metadato = $metadato;

        return $this;
    }

    /**
     * Get metadato
     *
     * @return \Sistema\UserBundle\Entity\Metadatos 
     */
    public function getMetadato()
    {
        return $this->metadato;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return UsuarioCapa
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set usuario
     *
     * @param \Sistema\UserBundle\Entity\User $usuario
     * @return UsuarioCapa
     */
    public function setUsuario(\Sistema\UserBundle\Entity\User $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \Sistema\UserBundle\Entity\User 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->categoria = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add categoria
     *
     * @param \Sistema\AdminBundle\Entity\Categoria $categoria
     * @return UsuarioCapa
     */
    public function addCategorium(\Sistema\AdminBundle\Entity\Categoria $categoria)
    {
        $this->categoria[] = $categoria;

        return $this;
    }

    /**
     * Remove categoria
     *
     * @param \Sistema\AdminBundle\Entity\Categoria $categoria
     */
    public function removeCategorium(\Sistema\AdminBundle\Entity\Categoria $categoria)
    {
        $this->categoria->removeElement($categoria);
    }

    /**
     * Get categoria
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set capa
     *
     * @param \Sistema\AdminBundle\Entity\Capa $capa
     * @return UsuarioCapa
     */
    public function setCapa(\Sistema\AdminBundle\Entity\Capa $capa = null)
    {
        $this->capa = $capa;

        return $this;
    }

    /**
     * Get capa
     *
     * @return \Sistema\AdminBundle\Entity\Capa 
     */
    public function getCapa()
    {
        return $this->capa;
    }

    /**
     * Set informe
     *
     * @param string $informe
     * @return UsuarioCapa
     */
    public function setInforme($informe)
    {
        $this->informe = $informe;

        return $this;
    }

    /**
     * Get informe
     *
     * @return string 
     */
    public function getInforme()
    {
        return $this->informe;
    }
}
