<?php
namespace Sistema\UserBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class isShp extends Constraint
{
    public $message = 'Extensión inválida en Shp';
}