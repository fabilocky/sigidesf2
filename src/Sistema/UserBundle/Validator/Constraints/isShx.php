<?php
namespace Sistema\UserBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class isShx extends Constraint
{
    public $message = 'Extensión inválida en Shx';
}