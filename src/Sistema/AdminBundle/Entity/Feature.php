<?php

namespace Sistema\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Feature
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Feature
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="fid", type="string", length=50)
     */
    private $fid;
    
    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Sistema\AdminBundle\Entity\Vinculacion", inversedBy="features")
     * @ORM\JoinColumn(name="vinculacion_id", referencedColumnName="id")
     */
    private $vinculacion;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fid
     *
     * @param string $fid
     * @return Feature
     */
    public function setFid($fid)
    {
        $this->fid = $fid;

        return $this;
    }

    /**
     * Get fid
     *
     * @return string 
     */
    public function getFid()
    {
        return $this->fid;
    }
    
    /**
     * Set vinculacion
     *
     * @param  \Sistema\AdminBundle\Entity\Vinculacion $vinculacion
     * @return Archivo
     */
    public function setVinculacion(\Sistema\AdminBundle\Entity\Vinculacion $vinculacion = null)
    {
        $this->vinculacion = $vinculacion;

        return $this;
    }

    /**
     * Get vinculacion
     *
     * @return \Sistema\AdminBundle\Entity\Vinculacion
     */
    public function getVinculacion()
    {
        return $this->vinculacion;
    }
}
