<?php

namespace Sistema\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Config
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\AdminBundle\Entity\ConfigRepository")
 * @Vich\Uploadable
 */
class Config
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="zoom", type="string", length=50)
     */
    private $zoom;    
    
    /**
     * @var string
     *
     * @ORM\Column(name="centergoogle", type="string", length=50)
     */
    private $centergoogle;

   /**
     * @var array
     *
     * @ORM\Column(name="arbol", type="json_array", nullable=true)
     */
    private $arbol;
    
    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=50)
     */
    private $titulo;
    
    /**
     * @var string
     *
     * @ORM\Column(name="urlgeoserver", type="string", length=150)
     */
    private $urlgeoserver;
    
    /**
     * @var string
     *
     * @ORM\Column(name="urlwfs", type="string", length=150, nullable=true)
     */
    private $urlwfs;   
    
    /**
     * @var string
     *
     * @ORM\Column(name="host", type="string", length=50, nullable=true)
     */
    private $host;
    
    /**
     * @var string
     *
     * @ORM\Column(name="dbname", type="string", length=50, nullable=true)
     */
    private $dbname;
    
    /**
     * @var string
     *
     * @ORM\Column(name="user", type="string", length=50, nullable=true)
     */
    private $user;
    
    /**
     * @var string
     *
     * @ORM\Column(name="pass", type="string", length=50, nullable=true)
     */
    private $pass;
    
    /**
     * @var string
     *
     * @ORM\Column(name="tabla", type="string", length=50, nullable=true)
     */
    private $tabla;
    
    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="config_image", fileNameProperty="imageName")
     * 
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $imageName;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;
    
    /**
     * @var string
     *
     * @ORM\Column(name="workspacegs", type="string", length=50, nullable=true)
     */
    private $workspacegs;
    
    /**
     * @var string
     *
     * @ORM\Column(name="datastoregs", type="string", length=50, nullable=true)
     */
    private $datastoregs;
    
    /**
     * @var string
     *
     * @ORM\Column(name="proyecciongs", type="string", length=50, nullable=true)
     */
    private $proyecciongs;
    
    /**
     * @var string
     *
     * @ORM\Column(name="usergs", type="string", length=50, nullable=true)
     */
    private $usergs;
    
    /**
     * @var string
     *
     * @ORM\Column(name="passgs", type="string", length=50, nullable=true)
     */
    private $passgs;
    
    /**
     * @var string
     *
     * @ORM\Column(name="userpgsql", type="string", length=50, nullable=true)
     */
    private $userpgsql;
    
    /**
     * @var string
     *
     * @ORM\Column(name="passpgsql", type="string", length=50, nullable=true)
     */
    private $passpgsql;
    
    /**
     * @var string
     *
     * @ORM\Column(name="hostpgsql", type="string", length=50, nullable=true)
     */
    private $hostpgsql;
    
    /**
     * @var string
     *
     * @ORM\Column(name="bdpgsql", type="string", length=50, nullable=true)
     */
    private $bdpgsql;
    
    /**
     * @var string
     *
     * @ORM\Column(name="bdmetadatospgsql", type="string", length=50, nullable=true)
     */
    private $bdmetadatospgsql;
    
    /**
     * @var string
     *
     * @ORM\Column(name="urlgeonetwork", type="string", length=50, nullable=true)
     */
    private $urlgeonetwork;
    
    /**
     * @var string
     *
     * @ORM\Column(name="mail", type="string", length=50, nullable=true)
     */
    private $mail;
    
    /**
     * @var string
     *
     * @ORM\Column(name="colorbannerp", type="string", length=50, nullable=true)
     */
    private $colorbannerp;
    
    /**
     * @var string
     *
     * @ORM\Column(name="colorbanners", type="string", length=50, nullable=true)
     */
    private $colorbanners;
    
    /**
     * @var array
     *
     * @ORM\Column(name="vinculaciones", type="array", nullable=true)
     */
    private $vinculaciones;

    public function __toString() {
        return $this->titulo;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set zoom
     *
     * @param string $zoom
     * @return Config
     */
    public function setZoom($zoom)
    {
        $this->zoom = $zoom;

        return $this;
    }

    /**
     * Get zoom
     *
     * @return string 
     */
    public function getZoom()
    {
        return $this->zoom;
    }  
    
    /**
     * Set center
     *
     * @param string $center
     * @return Config
     */
    public function setCenter($center)
    {
        $this->center = $center;

        return $this;
    }

    /**
     * Get center
     *
     * @return string 
     */
    public function getCenter()
    {
        return $this->center;
    }
    
    /**
     * Set centergoogle
     *
     * @param string $centergoogle
     * @return Config
     */
    public function setCentergoogle($centergoogle)
    {
        $this->centergoogle = $centergoogle;

        return $this;
    }

    /**
     * Get centergoogle
     *
     * @return string 
     */
    public function getCentergoogle()
    {
        return $this->centergoogle;
    }
    
    /**
     * Set arbol
     *
     * @param array $arbol
     * @return Config
     */
    public function setArbol($arbol)
    {
        $this->arbol = $arbol;

        return $this;
    }

    /**
     * Get arbol
     *
     * @return array 
     */
    public function getArbol()
    {
        return $this->arbol;
    }
    
    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Config
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }  

    /**
     * Set urlgeoserver
     *
     * @param string $urlgeoserver
     * @return Config
     */
    public function setUrlgeoserver($urlgeoserver)
    {
        $this->urlgeoserver = $urlgeoserver;

        return $this;
    }

    /**
     * Get urlgeoserver
     *
     * @return string 
     */
    public function getUrlgeoserver()
    {
        return $this->urlgeoserver;
    }
    
    /**
     * Set urlwfs
     *
     * @param string $urlwfs
     * @return Config
     */
    public function setUrlwfs($urlwfs)
    {
        $this->urlwfs = $urlwfs;

        return $this;
    }

    /**
     * Get urlwfs
     *
     * @return string 
     */
    public function getUrlwfs()
    {
        return $this->urlwfs;
    }

    /**
     * Set capa
     *
     * @param \Sistema\AdminBundle\Entity\Capa $capa
     * @return Config
     */
    public function setCapa(\Sistema\AdminBundle\Entity\Capa $capa = null)
    {
        $this->capa = $capa;

        return $this;
    }

    /**
     * Get capa
     *
     * @return \Sistema\AdminBundle\Entity\Capa 
     */
    public function getCapa()
    {
        return $this->capa;
    }

    /**
     * Set atributo
     *
     * @param array $atributo
     * @return Config
     */
    public function setAtributo($atributo)
    {
        $this->atributo = $atributo;

        return $this;
    }

    /**
     * Get atributo
     *
     * @return array
     */
    public function getAtributo()
    {
        return $this->atributo;
    }

    /**
     * Add capas
     *
     * @param \Sistema\AdminBundle\Entity\Capa $capas
     * @return Config
     */
    public function addCapa(\Sistema\AdminBundle\Entity\Capa $capas)
    {
        $this->capas[] = $capas;

        return $this;
    }

    /**
     * Remove capas
     *
     * @param \Sistema\AdminBundle\Entity\Capa $capas
     */
    public function removeCapa(\Sistema\AdminBundle\Entity\Capa $capas)
    {
        $this->capas->removeElement($capas);
    }

    /**
     * Get capas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCapas()
    {
        return $this->capas;
    }

    /**
     * Set host
     *
     * @param string $host
     * @return Config
     */
    public function setHost($host)
    {
        $this->host = $host;

        return $this;
    }

    /**
     * Get host
     *
     * @return string 
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * Set dbname
     *
     * @param string $dbname
     * @return Config
     */
    public function setDbname($dbname)
    {
        $this->dbname = $dbname;

        return $this;
    }

    /**
     * Get dbname
     *
     * @return string 
     */
    public function getDbname()
    {
        return $this->dbname;
    }

    /**
     * Set user
     *
     * @param string $user
     * @return Config
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return string 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set pass
     *
     * @param string $pass
     * @return Config
     */
    public function setPass($pass)
    {
        $this->pass = $pass;

        return $this;
    }

    /**
     * Get pass
     *
     * @return string 
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * Set table
     *
     * @param string $tabla
     * @return Config
     */
    public function setTabla($tabla)
    {
        $this->tabla = $tabla;

        return $this;
    }

    /**
     * Get tabla
     *
     * @return string 
     */
    public function getTabla()
    {
        return $this->tabla;
    }
    
    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Product
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param string $imageName
     *
     * @return Product
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Config
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set workspacegs
     *
     * @param string $workspacegs
     * @return Config
     */
    public function setWorkspacegs($workspacegs)
    {
        $this->workspacegs = $workspacegs;

        return $this;
    }

    /**
     * Get workspacegs
     *
     * @return string 
     */
    public function getWorkspacegs()
    {
        return $this->workspacegs;
    }

    /**
     * Set datastoregs
     *
     * @param string $datastoregs
     * @return Config
     */
    public function setDatastoregs($datastoregs)
    {
        $this->datastoregs = $datastoregs;

        return $this;
    }

    /**
     * Get datastoregs
     *
     * @return string 
     */
    public function getDatastoregs()
    {
        return $this->datastoregs;
    }

    /**
     * Set proyecciongs
     *
     * @param string $proyecciongs
     * @return Config
     */
    public function setProyecciongs($proyecciongs)
    {
        $this->proyecciongs = $proyecciongs;

        return $this;
    }

    /**
     * Get proyecciongs
     *
     * @return string 
     */
    public function getProyecciongs()
    {
        return $this->proyecciongs;
    }

    /**
     * Set usergs
     *
     * @param string $usergs
     * @return Config
     */
    public function setUsergs($usergs)
    {
        $this->usergs = $usergs;

        return $this;
    }

    /**
     * Get usergs
     *
     * @return string 
     */
    public function getUsergs()
    {
        return $this->usergs;
    }

    /**
     * Set passgs
     *
     * @param string $passgs
     * @return Config
     */
    public function setPassgs($passgs)
    {
        $this->passgs = $passgs;

        return $this;
    }

    /**
     * Get passgs
     *
     * @return string 
     */
    public function getPassgs()
    {
        return $this->passgs;
    }

    /**
     * Set userpgsql
     *
     * @param string $userpgsql
     * @return Config
     */
    public function setUserpgsql($userpgsql)
    {
        $this->userpgsql = $userpgsql;

        return $this;
    }

    /**
     * Get userpgsql
     *
     * @return string 
     */
    public function getUserpgsql()
    {
        return $this->userpgsql;
    }

    /**
     * Set passpgsql
     *
     * @param string $passpgsql
     * @return Config
     */
    public function setPasspgsql($passpgsql)
    {
        $this->passpgsql = $passpgsql;

        return $this;
    }

    /**
     * Get passpgsql
     *
     * @return string 
     */
    public function getPasspgsql()
    {
        return $this->passpgsql;
    }

    /**
     * Set hostpgsql
     *
     * @param string $hostpgsql
     * @return Config
     */
    public function setHostpgsql($hostpgsql)
    {
        $this->hostpgsql = $hostpgsql;

        return $this;
    }

    /**
     * Get hostpgsql
     *
     * @return string 
     */
    public function getHostpgsql()
    {
        return $this->hostpgsql;
    }

    /**
     * Set bdpgsql
     *
     * @param string $bdpgsql
     * @return Config
     */
    public function setBdpgsql($bdpgsql)
    {
        $this->bdpgsql = $bdpgsql;

        return $this;
    }

    /**
     * Get bdpgsql
     *
     * @return string 
     */
    public function getBdpgsql()
    {
        return $this->bdpgsql;
    }

    /**
     * Set bdmetadatospgsql
     *
     * @param string $bdmetadatospgsql
     * @return Config
     */
    public function setBdmetadatospgsql($bdmetadatospgsql)
    {
        $this->bdmetadatospgsql = $bdmetadatospgsql;

        return $this;
    }

    /**
     * Get bdmetadatospgsql
     *
     * @return string 
     */
    public function getBdmetadatospgsql()
    {
        return $this->bdmetadatospgsql;
    }

    /**
     * Set urlgeonetwork
     *
     * @param string $urlgeonetwork
     * @return Config
     */
    public function setUrlgeonetwork($urlgeonetwork)
    {
        $this->urlgeonetwork = $urlgeonetwork;

        return $this;
    }

    /**
     * Get urlgeonetwork
     *
     * @return string 
     */
    public function getUrlgeonetwork()
    {
        return $this->urlgeonetwork;
    }

    /**
     * Set mail
     *
     * @param string $mail
     * @return Config
     */
    public function setMail($mail)
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string 
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set colorbannerp
     *
     * @param string $colorbannerp
     * @return Config
     */
    public function setColorbannerp($colorbannerp)
    {
        $this->colorbannerp = $colorbannerp;

        return $this;
    }

    /**
     * Get colorbannerp
     *
     * @return string 
     */
    public function getColorbannerp()
    {
        return $this->colorbannerp;
    }

    /**
     * Set colorbanners
     *
     * @param string $colorbanners
     * @return Config
     */
    public function setColorbanners($colorbanners)
    {
        $this->colorbanners = $colorbanners;

        return $this;
    }

    /**
     * Get colorbanners
     *
     * @return string 
     */
    public function getColorbanners()
    {
        return $this->colorbanners;
    }
    
    /**
     * Set vinculaciones
     *
     * @param array $vinculaciones
     * @return Config
     */
    public function setVinculaciones($vinculaciones)
    {
        $this->vinculaciones = $vinculaciones;

        return $this;
    }

    /**
     * Get vinculaciones
     *
     * @return array 
     */
    public function getVinculaciones()
    {
        return $this->vinculaciones;
    }
}
