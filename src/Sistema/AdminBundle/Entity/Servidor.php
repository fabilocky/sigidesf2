<?php

namespace Sistema\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Servidor
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\AdminBundle\Entity\ServidorRepository")
 */
class Servidor
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=50)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;
    
    /**
     * @var capa
     *
     * @ORM\OneToMany(targetEntity="Sistema\AdminBundle\Entity\Capa", mappedBy="servidor")
     */
    private $capa;

    public function __toString() {
        return $this->url;
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Servidor
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Servidor
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->capa = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add capa
     *
     * @param \Sistema\AdminBundle\Entity\Capa $capa
     * @return Servidor
     */
    public function addCapon(\Sistema\AdminBundle\Entity\Capa $capa)
    {
        $this->capa[] = $capa;

        return $this;
    }

    /**
     * Remove capa
     *
     * @param \Sistema\AdminBundle\Entity\Capa $capa
     */
    public function removeCapon(\Sistema\AdminBundle\Entity\Capa $capa)
    {
        $this->capa->removeElement($capa);
    }

    /**
     * Get capa
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCapa()
    {
        return $this->capa;
    }
}
