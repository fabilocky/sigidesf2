<?php

namespace Sistema\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping\ManyToMany;

/**
 * Vinculacion
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Vinculacion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\OneToMany(targetEntity="Sistema\AdminBundle\Entity\Feature", mappedBy="vinculacion", cascade={"all"}, orphanRemoval=true)
     */
    private $features;
    
    /**
     * @var integer
     *
     * @ORM\OneToMany(targetEntity="Sistema\AdminBundle\Entity\Archivo", mappedBy="vinculacion", cascade={"all"}, orphanRemoval=true)
     * @Assert\Valid()
     */
    private $archivos;

    public function __construct()
    {       
        $this->archivos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->features = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    public function __toString() {
        return $this->getId();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }   
    
    /**
     * Add archivos
     *
     * @param  \Sistema\AdminBundle\Entity\Archivo $archivo
     * @return Vinculacion
     */
    public function addArchivo(\Sistema\AdminBundle\Entity\Archivo $archivo)
    {
        $archivo->setVinculacion($this);
        $this->archivos[] = $archivo;

        return $this;
    }

    /**
     * Remove documentos
     *
     * @param \Sistema\AdminBundle\Entity\Archivo $imagen
     */
    public function removeArchivo(\Sistema\AdminBundle\Entity\Archivo $imagen)
    {
        $this->archivos->removeElement($imagen);
    }

    /**
     * Get documentos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArchivos()
    {
        return $this->archivos;
    }
    
    /**
     * Add features
     *
     * @param  \Sistema\AdminBundle\Entity\Feature $feature
     * @return Vinculacion
     */
    public function addFeature(\Sistema\AdminBundle\Entity\Feature $feature)
    {
        $feature->setVinculacion($this);
        $this->features[] = $feature;        
        return $this;
    }

    /**
     * Remove documentos
     *
     * @param \Sistema\AdminBundle\Entity\Feature $imagen
     */
    public function removeFeature(\Sistema\AdminBundle\Entity\Feature $imagen)
    {
        $this->features->removeElement($imagen);
    }

    /**
     * Get documentos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFeatures()
    {
        return $this->features;
    }
}