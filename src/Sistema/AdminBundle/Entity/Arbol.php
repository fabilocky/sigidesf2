<?php

namespace Sistema\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Arbol
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Arbol
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var array
     *
     * @ORM\Column(name="arbol", type="text")
     */
    private $arbol;    
    
    /**
     * @ORM\OneToOne(targetEntity="Sistema\AdminBundle\Entity\Pestania", cascade={"persist"})
     * @ORM\JoinColumn(name="pestania_id", referencedColumnName="id")
     */
    private $pestania;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set arbol
     *
     * @param array $arbol
     * @return Arbol
     */
    public function setArbol($arbol)
    {
        $this->arbol = $arbol;

        return $this;
    }

    /**
     * Get arbol
     *
     * @return array 
     */
    public function getArbol()
    {
        return $this->arbol;
    }    

    /**
     * Set pestania
     *
     * @param \Sistema\AdminBundle\Entity\Pestania $pestania
     * @return Arbol
     */
    public function setPestania(\Sistema\AdminBundle\Entity\Pestania $pestania = null)
    {
        $this->pestania = $pestania;

        return $this;
    }

    /**
     * Get pestania
     *
     * @return \Sistema\AdminBundle\Entity\Pestania 
     */
    public function getPestania()
    {
        return $this->pestania;
    }
}
