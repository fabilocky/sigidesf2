<?php

namespace Sistema\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Capa
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\AdminBundle\Entity\CapaRepository")
 */
class Capa
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=50)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="capa", type="string", length=80)
     */
    private $capa;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activo", type="boolean")
     */
    private $activo;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="base", type="boolean")
     */
    private $base;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="inicio", type="boolean", nullable=true)
     */
    private $inicio;
    
    /**
     * @var \Sistema\AdminBundle\Entity\Categoria
     *
     * @ORM\ManyToMany(targetEntity="Sistema\AdminBundle\Entity\Categoria", inversedBy="capas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categoria_id", referencedColumnName="id")
     * })
     */
    private $categoria;    
    
    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToOne(targetEntity="Sistema\AdminBundle\Entity\Servidor", inversedBy="capa")
     * @ORM\JoinColumn(name="servidor_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $servidor;
    
    /**
     * @ORM\OneToOne(targetEntity="Sistema\AdminBundle\Entity\Config")
     */
    protected $config;

    /**
     * @ORM\OneToOne(targetEntity="Sistema\UserBundle\Entity\UsuarioCapa", mappedBy="capa")
     */
    private $usuariocapa;
    
    /**
     * @var string
     *
     * @ORM\Column(name="metadatos", type="string", nullable=true)
     */
    private $metadatos;

    public function __toString() {
        return $this->titulo;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Capa
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set capa
     *
     * @param string $capa
     * @return Capa
     */
    public function setCapa($capa)
    {
        $this->capa = $capa;

        return $this;
    }

    /**
     * Get capa
     *
     * @return string 
     */
    public function getCapa()
    {
        return $this->capa;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     * @return Capa
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }
    
    /**
     * Set base
     *
     * @param boolean $base
     * @return Capa
     */
    public function setBase($base)
    {
        $this->base = $base;

        return $this;
    }

    /**
     * Get base
     *
     * @return boolean 
     */
    public function getBase()
    {
        return $this->base;
    }
    
    /**
     * Set categoria
     *
     * @param \Sistema\AdminBundle\Entity\Categoria $categoria
     * @return Capas
     */
    public function setCategoria(\Doctrine\ORM\PersistentCollection $categoria = null)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return \Sistema\AdminBundle\Entity\Categorias 
     */
    public function getCategoria()
    {
        return $this->categoria;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->categoria = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add categoria
     *
     * @param \Sistema\AdminBundle\Entity\Categoria $categoria
     * @return Capa
     */
    public function addCategorium(\Sistema\AdminBundle\Entity\Categoria $categoria)
    {
        $this->categoria[] = $categoria;

        return $this;
    }

    /**
     * Remove categoria
     *
     * @param \Sistema\AdminBundle\Entity\Categoria $categoria
     */
    public function removeCategorium(\Sistema\AdminBundle\Entity\Categoria $categoria)
    {
        $this->categoria->removeElement($categoria);
    }

    /**
     * Set servidor
     *
     * @param \Sistema\AdminBundle\Entity\Servidor $servidor
     * @return Capa
     */
    public function setServidor(\Sistema\AdminBundle\Entity\Servidor $servidor = null)
    {
        $this->servidor = $servidor;

        return $this;
    }

    /**
     * Get servidor
     *
     * @return \Sistema\AdminBundle\Entity\Servidor 
     */
    public function getServidor()
    {
        return $this->servidor;
    }    

    /**
     * Set config
     *
     * @param \Sistema\AdminBundle\Entity\Config $config
     * @return Capa
     */
    public function setConfig(\Sistema\AdminBundle\Entity\Config $config = null)
    {
        $this->config = $config;

        return $this;
    }

    /**
     * Get config
     *
     * @return \Sistema\AdminBundle\Entity\Config 
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Set configvinculacion
     *
     * @param \Sistema\AdminBundle\Entity\Config $configvinculacion
     * @return Capa
     */
    public function setConfigvinculacion(\Sistema\AdminBundle\Entity\Config $configvinculacion = null)
    {
        $this->configvinculacion = $configvinculacion;

        return $this;
    }

    /**
     * Get configvinculacion
     *
     * @return \Sistema\AdminBundle\Entity\Config 
     */
    public function getConfigvinculacion()
    {
        return $this->configvinculacion;
    }

    /**
     * Set usuariocapa
     *
     * @param \Sistema\UserBundle\Entity\UsuarioCapa $usuariocapa
     * @return Capa
     */
    public function setUsuariocapa(\Sistema\UserBundle\Entity\UsuarioCapa $usuariocapa = null)
    {
        $this->usuariocapa = $usuariocapa;

        return $this;
    }

    /**
     * Get usuariocapa
     *
     * @return \Sistema\UserBundle\Entity\UsuarioCapa 
     */
    public function getUsuariocapa()
    {
        return $this->usuariocapa;
    }

    /**
     * Set inicio
     *
     * @param boolean $inicio
     * @return Capa
     */
    public function setInicio($inicio)
    {
        $this->inicio = $inicio;

        return $this;
    }

    /**
     * Get inicio
     *
     * @return boolean 
     */
    public function getInicio()
    {
        return $this->inicio;
    }

    /**
     * Set metadatos
     *
     * @param string $metadatos
     * @return Capa
     */
    public function setMetadatos($metadatos)
    {
        $this->metadatos = $metadatos;

        return $this;
    }

    /**
     * Get metadatos
     *
     * @return string 
     */
    public function getMetadatos()
    {
        return $this->metadatos;
    }
}
