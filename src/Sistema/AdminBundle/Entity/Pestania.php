<?php

namespace Sistema\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pestania
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Pestania
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=50)
     */
    private $nombre;
    
    /**
     * @ORM\ManyToMany(targetEntity="Sistema\AdminBundle\Entity\Categoria", mappedBy="pestania")
     **/
    private $categorias;
    
    /**
     * @ORM\OneToOne(targetEntity="Sistema\AdminBundle\Entity\Arbol", mappedBy="pestania", cascade={"remove", "persist"})
     */
    private $arbol;

    public function __toString() {
        return $this->nombre;
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Pestania
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->categorias = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add categorias
     *
     * @param \Sistema\AdminBundle\Entity\Categoria $categorias
     * @return Pestania
     */
    public function addCategoria(\Sistema\AdminBundle\Entity\Categoria $categorias)
    {
        $categorias->addPestanium($this);
        $this->categorias[] = $categorias;

        return $this;
    }

    /**
     * Remove categorias
     *
     * @param \Sistema\AdminBundle\Entity\Categoria $categorias
     */
    public function removeCategoria(\Sistema\AdminBundle\Entity\Categoria $categorias)
    {
        $this->categorias->removeElement($categorias);
    }

    /**
     * Get categorias
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategorias()
    {
        return $this->categorias;
    }

    /**
     * Set arbol
     *
     * @param \Sistema\AdminBundle\Entity\Arbol $arbol
     * @return Pestania
     */
    public function setArbol(\Sistema\AdminBundle\Entity\Arbol $arbol = null)
    {
        $this->arbol = $arbol;

        return $this;
    }

    /**
     * Get arbol
     *
     * @return \Sistema\AdminBundle\Entity\Arbol 
     */
    public function getArbol()
    {
        return $this->arbol;
    }
}
