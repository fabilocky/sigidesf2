<?php

namespace Sistema\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Categoria
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Sistema\AdminBundle\Entity\CategoriaRepository")
 */
class Categoria
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=50)
     */
    private $nombre;
    
    /**
     * @var category
     *
     * @ORM\OneToMany(targetEntity="Sistema\AdminBundle\Entity\Categoria", mappedBy="parent", orphanRemoval=true, cascade={"persist"})
     */
    private $children;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToOne(targetEntity="Sistema\AdminBundle\Entity\Categoria", inversedBy="children", cascade={"persist"})
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;
    
    
    /**
     * @ORM\ManyToMany(targetEntity="Sistema\AdminBundle\Entity\Capa", mappedBy="categoria")
     **/
    private $capas;
    
    /**
     * @ORM\ManyToMany(targetEntity="Sistema\UserBundle\Entity\UsuarioCapa", mappedBy="categoria")
     **/
    private $capasusuario;

    /**
     * @var integer
     *
     * @ORM\Column(name="orden", type="integer", nullable=true)
     */
    private $orden;    
    
    /**
     * @var \Sistema\AdminBundle\Entity\Pestania
     *
     * @ORM\ManyToMany(targetEntity="Sistema\AdminBundle\Entity\Pestania", inversedBy="categorias")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pestania_id", referencedColumnName="id")
     * })
     */
    private $pestania;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="expandida", type="boolean")
     */
    private $expandida;
    
    /**
     * Constructor
     */
    public function __construct()
    {        
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();        
    }
    
    public function __toString() {
        return $this->nombre;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Categoria
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    
    /**
     * Add children
     *
     * @param \Sistema\AdminBundle\Entity\Categoria $children
     * @return Categoria
     */
    public function addChild(\Sistema\AdminBundle\Entity\Categoria $children)
    {
        $this->children[] = $children;

        return $this;
    }

    /**
     * Remove children
     *
     * @param \Sistema\AdminBundle\Entity\Categoria $children
     */
    public function removeChild(\Sistema\AdminBundle\Entity\Categoria $children)
    {
        $this->children->removeElement($children);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set parent
     *
     * @param \Sistema\AdminBundle\Entity\Categoria $parent
     * @return Categoria
     */
    public function setParent(\Sistema\AdminBundle\Entity\Categoria $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \Sistema\AdminBundle\Entity\Categoria 
     */
    public function getParent()
    {
        return $this->parent;
    }
    
    /**
     * Add capas
     *
     * @param \Sistema\AdminBundle\Entity\Capa $capas
     * @return Categoria
     */
    public function addCapa(\Sistema\AdminBundle\Entity\Capa $capas)
    {
        $capas->addCategorium($this);
        $this->capas[] = $capas;

        return $this;
    }

    /**
     * Remove capas
     *
     * @param \Sistema\AdminBundle\Entity\Capa $capas
     */
    public function removeCapa(\Sistema\AdminBundle\Entity\Capa $capas)
    {
        $this->capas->removeElement($capas);
    }

    /**
     * Get capas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCapas()
    {
        return $this->capas;
    }
    
    /**
     * Set orden
     *
     * @param string $orden
     * @return Categoria
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return string 
     */
    public function getOrden()
    {
        return $this->orden;
    }    
    
    /**
     * Set expandida
     *
     * @param boolean $expandida
     * @return Categoria
     */
    public function setExpandida($expandida)
    {
        $this->expandida = $expandida;

        return $this;
    }

    /**
     * Get expandida
     *
     * @return boolean 
     */
    public function getExpandida()
    {
        return $this->expandida;
    }

    /**
     * Add pestania
     *
     * @param \Sistema\AdminBundle\Entity\Pestania $pestania
     * @return Categoria
     */
    public function addPestanium(\Sistema\AdminBundle\Entity\Pestania $pestania)
    {
        $this->pestania[] = $pestania;

        return $this;
    }

    /**
     * Remove pestania
     *
     * @param \Sistema\AdminBundle\Entity\Pestania $pestania
     */
    public function removePestanium(\Sistema\AdminBundle\Entity\Pestania $pestania)
    {
        $this->pestania->removeElement($pestania);
    }

    /**
     * Get pestania
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPestania()
    {
        return $this->pestania;
    }

    /**
     * Add capasusuario
     *
     * @param \Sistema\UserBundle\Entity\CapaUsuario $capasusuario
     * @return Categoria
     */
    public function addCapasusuario(\Sistema\UserBundle\Entity\UsuarioCapa $capasusuario)
    {
        $this->capasusuario[] = $capasusuario;

        return $this;
    }

    /**
     * Remove capasusuario
     *
     * @param \Sistema\UserBundle\Entity\CapaUsuario $capasusuario
     */
    public function removeCapasusuario(\Sistema\UserBundle\Entity\UsuarioCapa $capasusuario)
    {
        $this->capasusuario->removeElement($capasusuario);
    }

    /**
     * Get capasusuario
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCapasusuario()
    {
        return $this->capasusuario;
    }
}
