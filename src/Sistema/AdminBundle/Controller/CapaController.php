<?php

namespace Sistema\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\AdminBundle\Entity\Capa;
use Sistema\AdminBundle\Form\CapaType;
use Sistema\AdminBundle\Form\CapaFilterType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

set_time_limit(10);

libxml_disable_entity_loader(false);

/**
 * Capa controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/capa")
 */
class CapaController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/AdminBundle/Resources/config/Capa.yml',
    );

    /**
     * Lists all Capa entities.
     *
     * @Route("/", name="admin_capa")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->config['filterType'] = new CapaFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Capa entity.
     *
     * @Route("/", name="admin_capa_create")
     * @Method("POST")
     * @Template("SistemaAdminBundle:Capa:new.html.twig")
     */
    public function createAction() {
        $this->config['newType'] = new CapaType();
        $config = $this->getConfig();
        $request = $this->getRequest();
        $entity = new $config['entity']();
        $capa = $request->get("sistema_adminbundle_capa");
        $entity->setCapa($capa["capa"]);
        $form = $this->createCreateForm($config, $entity);
        $form->handleRequest($request);
//        var_dump($request->get("sistema_adminbundle_capa"));die();
        //var_dump($form);die();

        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
        $this->useACL($entity, 'create');

        $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

        $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $entity->getId()));
        return $this->redirect($nextAction);


        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Capa entity.
     *
     * @Route("/new", name="admin_capa_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = new CapaType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Capa entity.
     *
     * @Route("/{id}", name="admin_capa_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Capa entity.
     *
     * @Route("/{id}/edit", name="admin_capa_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->config['editType'] = new CapaType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Capa entity.
     *
     * @Route("/{id}", name="admin_capa_update")
     * @Method("PUT")
     * @Template("SistemaAdminBundle:Capa:edit.html.twig")
     */
    public function updateAction($id) {
        $this->config['editType'] = new CapaType();
        $config = $this->getConfig();
    	$request = $this->getRequest();
        $capa = $request->get("sistema_adminbundle_capa");        
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository($config['repository'])->find($id);
        $entity->setCapa($capa["capa"]);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find '.$config['entityName'].' entity.');
        }
        $this->useACL($entity, 'update');
        $deleteForm = $this->createDeleteForm($config, $id);
        $editForm = $this->createEditForm($config, $entity);
        $editForm->handleRequest($request);
        
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $editForm->get('saveAndAdd')->isClicked()
                        ? $this->generateUrl($config['new'])
                        : $this->generateUrl($config['show'], array('id' => $id));
            return $this->redirect($nextAction);       

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config'      => $config,
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Capa entity.
     *
     * @Route("/{id}", name="admin_capa_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter Capa.
     *
     * @Route("/exporter/{extension}", name="admin_capa_export")
     */
    public function getExporter() {
        $response = parent::exportCsvAction();

        return $response;
    }

    /**
     * Autocomplete a Capa entity.
     *
     * @Route("/autocomplete-forms/get-categoria", name="Capa_autocomplete_categoria")
     */
    public function getAutocompleteCategoria() {
        $options = array(
            'repository' => "SistemaAdminBundle:Categoria",
            'field' => "nombre",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Capa entity.
     *
     * @Route("/autocomplete-forms/get-servidor", name="Capa_autocomplete_servidor")
     */
    public function getAutocompleteServidor() {
        $options = array(
            'repository' => "SistemaAdminBundle:Servidor",
            'field' => "url",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Capa entity.
     *
     * @Route("/autocomplete-forms/get-capas", name="Capa_autocomplete_capas")
     */
    public function retornaCapas() {
        $isAjax = $this->getRequest()->isXMLHttpRequest();
        if ($isAjax) {
            $capas = null;
            $id = $this->getRequest()->get('id');
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SistemaAdminBundle:Servidor')->findOneById($id);
            //$funcionando = $this->getXml($entity->getUrl()."REQUEST=GetCapabilities&SERVICE=WMS&VERSION=1.1.1",5);
            ini_set('default_socket_timeout', 5);
            if ($xml = @simplexml_load_file($entity->getUrl() . "REQUEST=GetCapabilities&SERVICE=WMS&VERSION=1.1.1")) {
                $json = json_encode($xml);
                $array = json_decode($json, TRUE);
                //var_dump($array["Capability"]["Layer"]["Layer"]);die();
                if (isset($array["Capability"]["Layer"]["Layer"])){
                    $capas = json_encode($array["Capability"]["Layer"]["Layer"]);
                }                
            }
            if ($capas) {
                return new Response($capas);
            }
            else
                return new Response("Servidor No Encontrado o No Disponible");;
        }
        return new Response('Error. This is not ajax!', 400);
    }

    function getXml($url, $timeout = 0) {
        $ch = curl_init($url);

        curl_setopt_array($ch, array(
            CURLOPT_TIMEOUT => (int) $timeout
        ));

        if ($xml = curl_exec($ch)) {
            return @simplexml_load_string($xml);
        } else {
            return false;
        }
    }
    
    /**
     * Autocomplete a ReglaPermiso entity.
     *
     * @Route("/autocomplete-forms/get-permiso", name="ReglaPermiso_autocomplete_permiso")
     */
    public function getAutocompleteRole()
    {
        $options = array(
            'repository' => "SistemaUserBundle:Role",
            'field'      => "role_name",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

}