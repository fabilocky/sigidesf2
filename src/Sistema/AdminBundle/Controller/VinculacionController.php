<?php

namespace Sistema\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\AdminBundle\Entity\Vinculacion;
use Sistema\AdminBundle\Form\VinculacionType;
use Sistema\AdminBundle\Form\VinculacionFilterType;

/**
 * Vinculacion controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/vinculacion")
 */
class VinculacionController extends Controller
{
    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/AdminBundle/Resources/config/Vinculacion.yml',
    );

    /**
     * Lists all Vinculacion entities.
     *
     * @Route("/", name="admin_vinculacion")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->config['filterType'] = new VinculacionFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Vinculacion entity.
     *
     * @Route("/", name="admin_vinculacion_create")
     * @Method("POST")
     * @Template("SistemaAdminBundle:Vinculacion:new.html.twig")
     */
    public function createAction()
    {        
        $features = explode(",", $_POST["vectores"]);        
        $this->config['newType'] = new VinculacionType();
        $config = $this->getConfig();
    	$request = $this->getRequest();
        $entity = new $config['entity']();
        $form   = $this->createCreateForm($config, $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            foreach ($features as $feature){
                $feat = new \Sistema\AdminBundle\Entity\Feature();
                $feat->setFid($feature);
                $feat->setVinculacion($entity);
                $entity->addFeature($feat);
            }
            //ladybug_dump($entity->getArchivos()[0]);die();
            $em->persist($entity);
            $em->flush();
            $this->useACL($entity, 'create');

            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            $nextAction = $form->get('saveAndAdd')->isClicked()
                ? $this->generateUrl($config['new'])
                : $this->generateUrl('postvinculacion');
            return $this->redirect($nextAction);

        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config'     => $config,
            'entity'     => $entity,
            'form'       => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Vinculacion entity.
     *
     * @Route("/new", name="admin_vinculacion_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->config['newType'] = new VinculacionType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Vinculacion entity.
     *
     * @Route("/{id}", name="admin_vinculacion_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Vinculacion entity.
     *
     * @Route("/{id}/edit", name="admin_vinculacion_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->config['editType'] = new VinculacionType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Vinculacion entity.
     *
     * @Route("/{id}", name="admin_vinculacion_update")
     * @Method("PUT")
     * @Template("SistemaAdminBundle:Vinculacion:edit.html.twig")
     */
    public function updateAction($id)
    {
        $this->config['editType'] = new VinculacionType();
        $config = $this->getConfig();
    	$request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find '.$config['entityName'].' entity.');
        }
        $this->useACL($entity, 'update');
        $deleteForm = $this->createDeleteForm($config, $id);
        $editForm = $this->createEditForm($config, $entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $editForm->get('saveAndAdd')->isClicked()
                        ? $this->generateUrl($config['new'])
                        : $this->generateUrl('mapavinculacion');
            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config'      => $config,
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Vinculacion entity.
     *
     * @Route("/{id}", name="admin_vinculacion_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Autocomplete a Vinculacion entity.
     *
     * @Route("/autocomplete-forms/get-archivos", name="Vinculacion_autocomplete_archivos")
     */
    public function getAutocompleteArchivo()
    {
        $options = array(
            'repository' => "SistemaAdminBundle:Archivo",
            'field'      => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }
}