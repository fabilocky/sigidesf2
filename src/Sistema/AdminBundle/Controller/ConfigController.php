<?php

namespace Sistema\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\AdminBundle\Entity\Config;
use Sistema\AdminBundle\Form\ConfigType;
use Sistema\AdminBundle\Form\ConfigFilterType;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Doctrine\ORM\Query;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Config controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/config")
 */
class ConfigController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/AdminBundle/Resources/config/Config.yml',
    );

    /**
     * Lists all Config entities.
     *
     * @Route("/", name="admin_config")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->config['filterType'] = new ConfigFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Finds and displays a Config entity.
     *
     * @Route("/{id}", name="admin_config_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Config entity.
     *
     * @Route("/{id}/edit", name="admin_config_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->config['editType'] = new ConfigType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Config entity.
     *
     * @Route("/{id}", name="admin_config_update")
     * @Method("PUT")
     * @Template("SistemaAdminBundle:Config:edit.html.twig")
     */
    public function updateAction($id) {
        $this->config['editType'] = new ConfigType();
        $config = $this->getConfig();
    	$request = $this->getRequest();        
        $css = ".bannerprincipal {
            background-color: ". $request->get('sistema_adminbundle_config')['colorbannerp']." !important;
        }

        #banner-secundario {
            background-color: ". $request->get('sistema_adminbundle_config')['colorbanners'].";
        }";        
        
        $archivo = fopen('css/banner.css', "w");
        fwrite($archivo, $css);
        fclose($archivo);
        $capa = $request->get("sistema_adminbundle_config");        
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository($config['repository'])->find($id);
        //$entity->setAtributo($capa["atributo"]);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find '.$config['entityName'].' entity.');
        }
        $this->useACL($entity, 'update');
        $deleteForm = $this->createDeleteForm($config, $id);
        $editForm = $this->createEditForm($config, $entity);
        $editForm->handleRequest($request);
        
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $editForm->get('saveAndAdd')->isClicked()
                        ? $this->generateUrl($config['new'])
                        : $this->generateUrl($config['show'], array('id' => $id));
            return $this->redirect($nextAction);       

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config'      => $config,
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Config entity.
     *
     * @Route("/{id}", name="admin_config_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter Config.
     *
     * @Route("/exporter/{extension}", name="admin_config_export")
     */
    public function getExporter() {
        $response = parent::exportCsvAction();

        return $response;
    }

    /**
     * Displays a form to create a new Capa entity.
     *
     * @Route("/arbol/configurar", name="admin_arbol")
     * @Method("GET")
     * @Template()
     */
    public function arbolAction() {
        $form = $this->createFormBuilder()
        ->add('proyecto', 'select2', array(
                'class' => 'Sistema\AdminBundle\Entity\Proyecto',
                'url'   => 'Usuario_autocomplete_proyecto',
                'configs' => array(
                    'multiple' => false,//required true or false
                    'width'    => 'off',
                ),
                'attr' => array(
                    'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                )
            ))     
        ->getForm();
        $em = $this->getDoctrine()->getManager();
        $proyectos = $em->getRepository('SistemaAdminBundle:Proyecto')->findAll();
        return array(            
            'form' => $form->createView(),
            'proyectos' => $proyectos,
        );
    }
    
    /**
     * Displays a form to create a new Capa entity.
     *
     * @Route("/arbolpestania/configurar", name="admin_arbol_pestania")
     * @Method("GET")
     * @Template()
     */
    public function arbolpestaniaAction() {
        $form = $this->createFormBuilder()
        ->add('pestania', 'select2', array(
                'class' => 'Sistema\AdminBundle\Entity\Pestania',
                'url'   => 'Pestania_autocomplete_pestania',
                'configs' => array(
                    'multiple' => false,//required true or false
                    'width'    => 'off',
                ),
                'attr' => array(
                    'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                )
            ))     
        ->getForm();
        $em = $this->getDoctrine()->getManager();
        $pestanias = $em->getRepository('SistemaAdminBundle:Pestania')->findAll();
        return array(            
            'form' => $form->createView(),
            'pestanias' => $pestanias,
        );
    }

    /**
     * Displays a form to create a new Capa entity.
     *
     * @Route("/save/tree", name="admin_save_tree")
     * @Method("POST")     
     */
    public function savetreeAction() {
        $request = $this->getRequest();
        $config = $this->getConfig();        
        $em = $this->getDoctrine()->getManager();
        if ($_POST["tipo"] == "pestania"){
        $configurar = $em->getRepository('SistemaAdminBundle:Pestania')->find($_POST["id"]);    
        } else {
        $configurar = $em->getRepository('SistemaAdminBundle:Proyecto')->find($_POST["id"]);
        }
        //$configurar = $configurar[0];
//        ladybug_dump($_POST["tree"]);die();
        if ($configurar->getArbol()){
            $configurar->getArbol()->setArbol($_POST["tree"]);
            if ($_POST["tipo"] == "pestania"){
                $configurar->getArbol()->setPestania($configurar); 
            } else {
                $configurar->getArbol()->setProyecto($configurar);
            }            
        } else {
            $arbol = new \Sistema\AdminBundle\Entity\Arbol();
            $arbol->setArbol($_POST["tree"]);
            if ($_POST["tipo"] == "pestania"){
                $arbol->setPestania($configurar);
            } else {
                $arbol->setProyecto($configurar);
            }           
            $configurar->setArbol($arbol);            
        }        
        $jsonArray = json_decode($_POST["tree"]);
        $arbol = $this->read_tree_recursively($jsonArray);        
        $i = 1;
        foreach ($arbol as $cat) {
            $categoria = $em->getRepository('SistemaAdminBundle:Categoria')->find($cat["id"]);
            $parent = $em->getRepository('SistemaAdminBundle:Categoria')->find($cat["parent_id"]);
            $categoria->setParent($parent);
            $categoria->setOrden($i);
            $i++;
        }
        $em->flush();        
        $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

        $nextAction = $this->generateUrl('admin_arbol');
        return $this->redirect($nextAction);
    }

    /**
     * Displays a form to create a new Capa entity.
     *
     */
    function inArrayR($needle, $haystack, $strict = false) {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->inArrayR($needle, $item, $strict))) {
                return true;
            }
        }

        return false;
    }    

    function read_tree_recursively($items, $parent_id = 0, $result = array(), $level = 0) {
        foreach ($items as $child) {            
            $result[$child->id] = array(
                'id' => $child->id,
                'parent_id' => $parent_id,
                'level' => $level,
                'parent' => $parent_id,
                'name' => $child->name,
            );

            if (!empty($child->children)) {
                $result = $this->read_tree_recursively($child->children, $child->id, $result, $level + 1);
            }
        }
        return $result;
    }

    function recorro($matriz, $categorias) {

        foreach ($matriz as $key => $value) {
            $count = 0;
            if (is_array($value)) {
                if (isset($value["id"])) {                    
                    foreach ($categorias as $cat) {
                        if ($cat->getId() == $value["id"]) {
                            $count++;
                        }
                    }
                }
                //si es un array sigo recorriendo
//			echo 'key:'. $key;
//			echo '<br>';
                $this->recorro($value, $categorias);
            } else {
                //si es un elemento lo muestro                    
//			echo $key.': '.$value ;
//			echo '<br>';
            }
        }

        die();
    }
    
    
    function BuildTree($datas, $parent = 0, $limit = 0) {
        if ($limit > 1000)
            return ''; // Make sure not to have an endless recursion
//        $arbol = '[';
        $tree = '';
//        for ($i = 0, $ni = count($datas); $i < $ni; $i++) {
//            if ($datas[$i]['parent'] == $parent) {
//                $tree .= '{"name": "' . $datas[$i]['name'] . '", "is_open":true, text: "' . $datas[$i]['name'] . '",id: "' . $datas[$i]['id'] . '", children: [';               
//                //$tree .= $datas[$i]['name'];
//                $tree .= $this->BuildTree($datas, $datas[$i]['id'], $limit++);
//                $tree .= ']},';
//            }
//        }
        foreach ($datas as $data) {                     
            if (is_array($data) && $data['parent'] == $parent) {
                $tree .= '{"name": "' . $data['name'] . '", "is_open":true, "text": "' . $data['name'] . '", "id": "' . $data['id'] . '", "children": [';               
                //$tree .= $datas[$i]['name'];
                $tree .= $this->BuildTree($data, $data['id'], $limit++);
                $tree .= ']},';
            }
        }
        $tree = trim($tree, ',');
//        $arbol .= $tree.']';
        return $tree;
    }
    
    /**
     * Autocomplete a Capa entity.
     *
     * @Route("/autocomplete-forms/get-atributos", name="Config_autocomplete_atributos")
     */
    public function retornaAtributos() {        
            $capas = null;
            $id = $this->getRequest()->get('id');
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SistemaAdminBundle:Capa')->findOneById($id);
            $config = $em->getRepository('SistemaAdminBundle:Config')->findAll();
            $config = $config[0];
            //$funcionando = $this->getXml($entity->getUrl()."REQUEST=GetCapabilities&SERVICE=WMS&VERSION=1.1.1",5);
            $atributosjson = null;
            if ($entity){
                $json = file_get_contents($entity->getServidor()->getUrl() . "service=WFS&version=1.0.0&request=GetFeature&typeName=" . $entity->getCapa() . "&maxFeatures=1&outputFormat=json");
            $array = json_decode($json, TRUE);
            $atributos = $array["features"][0]["properties"];
            $atributosjson = json_encode($atributos);
            }            
            if ($atributosjson) {
                return new JsonResponse($atributosjson);
            }
            else {
                return new Response("Servidor No Encontrado o No Disponible");
            }                
    }

    function getXml($url, $timeout = 0) {
        $ch = curl_init($url);

        curl_setopt_array($ch, array(
            CURLOPT_TIMEOUT => (int) $timeout
        ));

        if ($xml = curl_exec($ch)) {
            return @simplexml_load_string($xml);
        } else {
            return false;
        }
    }
    
    /**
     * Autocomplete a Capa entity.
     *
     * @Route("/autocomplete-forms/get-arbol", name="Config_autocomplete_arbol")
     */
    public function retornaArbol() {
        $capas = null;
        $id = $this->getRequest()->get('id');
        $tipo = $this->getRequest()->get('tipo');
        $em = $this->getDoctrine()->getManager();
        if ($tipo == "pestania"){
            $proyecto = $em->getRepository("SistemaAdminBundle:Pestania")->find($id);
        } else {
            $proyecto = $em->getRepository("SistemaAdminBundle:Proyecto")->find($id);
        }               
        if ($proyecto->getArbol()) {
            $arbol = $proyecto->getArbol()->getArbol();            
            $categorias = $this->read_tree_recursively(json_decode($arbol));
            $categoriasp = $proyecto->getCategorias();
            //controla que esten todas las categorias, si no esta elimina el viejo nodo            
            foreach ($categorias as $cat) {
                $band = false;
                foreach ($categoriasp as $catp) {
                    if ($catp->getId() == $cat["id"]) {
                        $band = true;
                        break;
                    }
                }                
                if ($band == false){ //tengo que eliminar el elemento del array de categorias
                    unset($categorias[$cat["id"]]);
                                        
                    $arbol = $this->BuildTree($categorias); // armo de nuevo el arbol con las categorias
                    $arr = json_decode($arbol, true);                   
                    $arbol = "[".$arbol."]";                   
                    
//                    $arrne['name'] = $catp->getNombre();
//                    $arrne['is_open'] = true;
//                    $arrne['text'] = $catp->getNombre();
//                    $arrne['id'] = $catp->getId();
//                    array_push( $arr, $arrne );
//                    $arbol = json_encode($arr);                    
                }
            }                        
            //controla que esten todas las categorias, si no esta agrega el nuevo nodo
            foreach ($categoriasp as $catp) {
                $band = false;
                foreach ($categorias as $cat) {
                    if ($catp->getId() == $cat["id"]) {
                        $band = true;
                        break;
                    }
                }                
                if ($band == false){                    
                    $arr = json_decode($arbol, true);                    
                    $arrne['name'] = $catp->getNombre();
                    $arrne['is_open'] = true;
                    $arrne['text'] = $catp->getNombre();
                    $arrne['id'] = $catp->getId();                    
                    array_push( $arr, $arrne );
                    $arbol = json_encode($arr);                    
                }
            }
            
        } else {
            $i = 0;
        foreach ($proyecto->getCategorias() as $categoria) {
            $datas[$i]["id"] = $categoria->getId();            
            $datas[$i]["name"] = $categoria->getNombre();
            $datas[$i]["capas"] = $categoria->getCapas();
            $datas[$i]["parent"] = 0;
            $i++;
        }
        }
        if (isset($datas)) {
            $array = $this->BuildTree($datas);            
        } else {
            $array = null;
        }
        if (isset($arbol)) {
            return new JsonResponse($arbol);
        } elseif ($array) {
            return new JsonResponse("[".$array."]");
        } else {
            return new JsonResponse("[]");
        }
    }

}