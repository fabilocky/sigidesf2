<?php

namespace Sistema\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\AdminBundle\Entity\Servidor;
use Sistema\AdminBundle\Form\ServidorType;
use Sistema\AdminBundle\Form\ServidorFilterType;

/**
 * Servidor controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/servidor")
 */
class ServidorController extends Controller
{
    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/AdminBundle/Resources/config/Servidor.yml',
    );

    /**
     * Lists all Servidor entities.
     *
     * @Route("/", name="admin_servidor")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->config['filterType'] = new ServidorFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Servidor entity.
     *
     * @Route("/", name="admin_servidor_create")
     * @Method("POST")
     * @Template("SistemaAdminBundle:Servidor:new.html.twig")
     */
    public function createAction()
    {
        $this->config['newType'] = new ServidorType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new Servidor entity.
     *
     * @Route("/new", name="admin_servidor_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->config['newType'] = new ServidorType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Servidor entity.
     *
     * @Route("/{id}", name="admin_servidor_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Servidor entity.
     *
     * @Route("/{id}/edit", name="admin_servidor_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->config['editType'] = new ServidorType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Servidor entity.
     *
     * @Route("/{id}", name="admin_servidor_update")
     * @Method("PUT")
     * @Template("SistemaAdminBundle:Servidor:edit.html.twig")
     */
    public function updateAction($id)
    {
        $this->config['editType'] = new ServidorType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a Servidor entity.
     *
     * @Route("/{id}", name="admin_servidor_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter Servidor.
     *
     * @Route("/exporter/{extension}", name="admin_servidor_export")
     */
    public function getExporter()
    {
        $response = parent::exportCsvAction();

        return $response;
    }

    /**
     * Autocomplete a Servidor entity.
     *
     * @Route("/autocomplete-forms/get-capa", name="Servidor_autocomplete_capa")
     */
    public function getAutocompleteCapa()
    {
        $options = array(
            'repository' => "SistemaAdminBundle:Capa",
            'field'      => "titulo",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }
}