<?php

namespace Sistema\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\AdminBundle\Entity\Proveedor;
use Sistema\AdminBundle\Form\ProveedorType;
use Sistema\AdminBundle\Form\ProveedorFilterType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Proveedor controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/proveedor")
 */
class ProveedorController extends Controller
{
    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/AdminBundle/Resources/config/Proveedor.yml',
    );

    /**
     * Lists all Proveedor entities.
     *
     * @Route("/", name="admin_proveedor")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->config['filterType'] = new ProveedorFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Proveedor entity.
     *
     * @Route("/", name="admin_proveedor_create")
     * @Method("POST")
     * @Template("SistemaAdminBundle:Proveedor:new.html.twig")
     */
    public function createAction()
    {
        $this->config['newType'] = new ProveedorType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new Proveedor entity.
     *
     * @Route("/new", name="admin_proveedor_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->config['newType'] = new ProveedorType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Proveedor entity.
     *
     * @Route("/{id}", name="admin_proveedor_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Proveedor entity.
     *
     * @Route("/{id}/edit", name="admin_proveedor_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->config['editType'] = new ProveedorType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Proveedor entity.
     *
     * @Route("/{id}", name="admin_proveedor_update")
     * @Method("PUT")
     * @Template("SistemaAdminBundle:Proveedor:edit.html.twig")
     */
    public function updateAction($id)
    {
        $this->config['editType'] = new ProveedorType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a Proveedor entity.
     *
     * @Route("/{id}", name="admin_proveedor_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $response = parent::deleteAction($id);

        return $response;
    }
    
    /**
     * Finds and displays a precio Tipo Producto entity.
     *
     * @Route("/set/default", name="set_default")
     */
    public function setDefault() {        
      
            $id = $this->getRequest()->get('id');
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SistemaAdminBundle:Proveedor')->findall();
            foreach ($entity as $ent){
                if ($ent->getId() == $id){
                    $ent->setDefaultmap(1);
                } else {
                    $ent->setDefaultmap(0);
                }
                $em->flush();
            }
        
        return new Response();
    }
}