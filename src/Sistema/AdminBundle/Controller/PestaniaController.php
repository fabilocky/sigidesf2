<?php

namespace Sistema\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\AdminBundle\Entity\Pestania;
use Sistema\AdminBundle\Form\PestaniaType;
use Sistema\AdminBundle\Form\PestaniaFilterType;

/**
 * Pestania controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/pestania")
 */
class PestaniaController extends Controller
{
    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/AdminBundle/Resources/config/Pestania.yml',
    );

    /**
     * Lists all Pestania entities.
     *
     * @Route("/", name="admin_pestania")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->config['filterType'] = new PestaniaFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Pestania entity.
     *
     * @Route("/", name="admin_pestania_create")
     * @Method("POST")
     * @Template("SistemaAdminBundle:Pestania:new.html.twig")
     */
    public function createAction()
    {
        $this->config['newType'] = new PestaniaType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new Pestania entity.
     *
     * @Route("/new", name="admin_pestania_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->config['newType'] = new PestaniaType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Pestania entity.
     *
     * @Route("/{id}", name="admin_pestania_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Pestania entity.
     *
     * @Route("/{id}/edit", name="admin_pestania_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->config['editType'] = new PestaniaType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Pestania entity.
     *
     * @Route("/{id}", name="admin_pestania_update")
     * @Method("PUT")
     * @Template("SistemaAdminBundle:Pestania:edit.html.twig")
     */
    public function updateAction($id)
    {
        $this->config['editType'] = new PestaniaType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a Pestania entity.
     *
     * @Route("/{id}", name="admin_pestania_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Autocomplete a Pestania entity.
     *
     * @Route("/autocomplete-forms/get-categorias", name="Pestania_autocomplete_categorias")
     */
    public function getAutocompleteCategoria()
    {
        $options = array(
            'repository' => "SistemaAdminBundle:Categoria",
            'field'      => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Pestania entity.
     *
     * @Route("/autocomplete-forms/get-arbol", name="Pestania_autocomplete_arbol")
     */
    public function getAutocompleteArbol()
    {
        $options = array(
            'repository' => "SistemaAdminBundle:Arbol",
            'field'      => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }
    
    /**
     * Autocomplete a Pestania entity.
     *
     * @Route("/autocomplete-forms/get-pestania", name="Pestania_autocomplete_pestania")
     */
    public function getAutocompletePestania()
    {
        $options = array(
            'repository' => "SistemaAdminBundle:Pestania",
            'field'      => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }
    
    /**
     * Autocomplete a Pestania entity.
     *
     * @Route("/autocomplete-forms/get-capa", name="Pestania_autocomplete_capa")
     */
    public function getAutocompleteCapa()
    {
        $options = array(
            'repository' => "SistemaAdminBundle:Capa",
            'field'      => "titulo",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }
}