<?php

namespace Sistema\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\AdminBundle\Entity\Proyecto;
use Sistema\AdminBundle\Form\ProyectoType;
use Sistema\AdminBundle\Form\ProyectoFilterType;

/**
 * Proyecto controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/proyecto")
 */
class ProyectoController extends Controller
{
    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'Sistema/AdminBundle/Resources/config/Proyecto.yml',
    );

    /**
     * Lists all Proyecto entities.
     *
     * @Route("/", name="admin_proyecto")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->config['filterType'] = new ProyectoFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Proyecto entity.
     *
     * @Route("/", name="admin_proyecto_create")
     * @Method("POST")
     * @Template("SistemaAdminBundle:Proyecto:new.html.twig")
     */
    public function createAction()
    {
        $this->config['newType'] = new ProyectoType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new Proyecto entity.
     *
     * @Route("/new", name="admin_proyecto_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->config['newType'] = new ProyectoType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Proyecto entity.
     *
     * @Route("/{id}", name="admin_proyecto_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Proyecto entity.
     *
     * @Route("/{id}/edit", name="admin_proyecto_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->config['editType'] = new ProyectoType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Proyecto entity.
     *
     * @Route("/{id}", name="admin_proyecto_update")
     * @Method("PUT")
     * @Template("SistemaAdminBundle:Proyecto:edit.html.twig")
     */
    public function updateAction($id)
    {
        $this->config['editType'] = new ProyectoType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a Proyecto entity.
     *
     * @Route("/{id}", name="admin_proyecto_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Autocomplete a Proyecto entity.
     *
     * @Route("/autocomplete-forms/get-usuarios", name="Proyecto_autocomplete_usuarios")
     */
    public function getAutocompleteUser()
    {
        $options = array(
            'repository' => "SistemaUserBundle:User",
            'field'      => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }
    
    /**
     * Autocomplete a Localidad entity.
     *
     * @Route("/autocomplete-forms/get-proyecto", name="Usuario_autocomplete_proyecto")
     */
    public function getAutocompleteProyecto()
    {
        $options = array(
            'repository' => "SistemaAdminBundle:Proyecto",
            'field'      => "nombre",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }
}