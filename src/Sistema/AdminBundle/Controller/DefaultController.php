<?php

namespace Sistema\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/hello/{name}")
     * @Template()
     */
    public function indexAction($name)
    {
        return array('name' => $name);
    }
    
    /**
     * @Route("/actualizar")
     * @Template()
     */
    public function actualizarAction()
    {
        return array();
    }
    
    /**
     * Definimos las rutas para el login:
     * @Route("/updateusers", name="updateusers")
     */
    public function actualizarusuarios() {        
        $em = $this->getDoctrine()->getManager();
        $config = $em->getRepository('SistemaAdminBundle:Config')->findAll();
        $connectionFactory = $this->container->get('doctrine.dbal.connection_factory');
        $connection = $connectionFactory->createConnection(
                array('pdo' => new \PDO("pgsql:host=10.10.20.207;dbname=zzidechaco_bck-capacitaciones", "admin_postgres", "admpostgres"))
        );
        $query = $connection->executeQuery("SELECT * FROM persona INNER JOIN sf_guard_user ON sf_guard_user.id = persona.user_id");
        $results = $query->fetchAll();
        $role = array($em->getRepository('SistemaUserBundle:Role')->findOneByName('ROLE_USER'));
        foreach ($results as $user) {
//            ladybug_dump($user);die();
            $entity = $em->getRepository('SistemaUserBundle:User')->findOneByUsername($user["username"]);            
            if (!$entity) {
                $usuario = new \Sistema\UserBundle\Entity\User();
                $usuario->setUsername($user["username"]);
                $usuario->setNombre($user["first_name"]);
                $usuario->setApellido($user["last_name"]);
                $usuario->setEmail($user["email_address"]);
                $usuario->setDni($user["numero_documento"]);
                $usuario->setFechanac(new \DateTime($user["fecha_nacimiento"]));
                $usuario->setTelefono($user["telefono"]);
                $usuario->setIsActive(true);
                $usuario->setSalt(md5(time()));
                $usuario->setPassword($user["username"]);
                $usuario->setUserRoles($role);
                $encoder = $this->container->get('security.encoder_factory')->getEncoder($usuario);
                $passwordCodificado = $encoder->encodePassword(
                        $usuario->getPassword(), $usuario->getSalt()
                );               
                $usuario->setPassword($passwordCodificado);
                // Guardar el nuevo usuario en la base de datos
                $em->persist($usuario);
                $em->flush();
            } else {
                $entity->setUsername($user["username"]);
                $entity->setNombre($user["first_name"]);
                $entity->setApellido($user["last_name"]);
                $entity->setEmail($user["email_address"]);
                $entity->setDni($user["numero_documento"]);
                $entity->setFechanac(new \DateTime($user["fecha_nacimiento"]));
                $entity->setTelefono($user["telefono"]);
                $entity->setIsActive(true);
                $entity->setSalt(md5(time()));
                $entity->setPassword($user["username"]);
                if (count($entity->getUserRoles()) < 1){
                    $entity->setUserRoles($role);
                }
                //
                $encoder = $this->container->get('security.encoder_factory')->getEncoder($entity);
                $passwordCodificado = $encoder->encodePassword(
                        $entity->getPassword(), $entity->getSalt()
                );               
                $entity->setPassword($passwordCodificado);
                // Guardar el nuevo usuario en la base de datos                
                $em->flush(); 
            }
        }
        $this->get('session')->getFlashBag()->add('success', 'Datos de usuarios actualizados');
        return $this->redirect('user');
    }
    
    /**
     * Definimos las rutas para el login:
     * @Route("/updatemapas", name="updatemapas")
     */
    public function actualizarproyectos() {        
        $em = $this->getDoctrine()->getManager();
        $config = $em->getRepository('SistemaAdminBundle:Config')->findAll();
        $connectionFactory = $this->container->get('doctrine.dbal.connection_factory');
        $connection = $connectionFactory->createConnection(
                array('pdo' => new \PDO("pgsql:host=10.10.20.207;dbname=zzidechaco_bck-capacitaciones", "admin_postgres", "admpostgres"))
        );
        $query = $connection->executeQuery("SELECT * FROM proyecto INNER JOIN sf_guard_user ON sf_guard_user.id = proyecto.user_id");
        $results = $query->fetchAll();
        
        foreach ($results as $mapa) {
            //ladybug_dump($mapa);die();
//            ladybug_dump($user);die();
            $usuario = $em->getRepository('SistemaUserBundle:User')->findOneByUsername($mapa["username"]);
            $proyecto = $em->getRepository('SistemaUserBundle:Mapa')->findOneByNombre($mapa["name"]);
            //$entity = $em->getRepository('SistemaUserBundle:Mapa')->findOneByUsername($user["username"]); 
            $nombre = $mapa["name"];
            $geometria = $mapa["geometry"];
            if ($geometria = "puntos"){
                $geometria = "point";
            } else if ($geometria = "poligonos"){
                $geometria = "polygon";
            } else {
                $geometria = "path"; 
            }
            $features = $mapa["features"];            
            $json = json_decode($features, true);
            $feat = json_encode($json);
            $i = 0;
            $atributos = null;
            if ($json["features"][0]["properties"]){
            foreach ($json["features"][0]["properties"] as $key => $value){                
                if (($key != "id") && ($key != "gid")){
                $atributos[$i]["nombre"] = $key;
                $atributos[$i]["tipo"] = "text";
                $i++;
                }
            }
            }
            //ladybug_dump($atributos); die();
            if (!$proyecto) {            
                $mapa = new \Sistema\UserBundle\Entity\Mapa();
                $mapa->setNombre($nombre);
                $mapa->setGeometria($geometria);
                $mapa->setActivo(true);
                $mapa->setAtributos($atributos);
                $archivo = fopen('files/mapas/'. $usuario->getUsername(). "_" .$mapa->getNombre(). ".json", "a");
                fwrite($archivo, $feat);
                fclose($archivo);
                $mapa->setFileName($usuario->getUsername(). "_" .$mapa->getNombre(). ".json");
                $mapa->setUpdatedAt(new \DateTime("now"));
                $mapa->setUsuario($usuario);
                // Guardar el nuevo usuario en la base de datos
                $em->persist($mapa);
                $em->flush();
            } else {
                $proyecto->setNombre($nombre);
                $proyecto->setGeometria($geometria);
                $proyecto->setActivo(true);
                $proyecto->setAtributos($atributos);
                $archivo = fopen('files/mapas/'. $usuario->getUsername(). "_" .$proyecto->getNombre(). ".json", "a");
                fwrite($archivo, $feat);
                fclose($archivo);
                $proyecto->setFileName($usuario->getUsername(). "_" .$proyecto->getNombre(). ".json");
                $proyecto->setUpdatedAt(new \DateTime("now"));            
                $proyecto->setUsuario($usuario);
                $em->flush();
            }
        }
        $this->get('session')->getFlashBag()->add('success', 'Datos de usuarios actualizados');
        return $this->redirect('user');
    }
    
    /**
     * Definimos las rutas para el login:
     * @Route("/sendmails", name="sendmails")
     */
    public function sendmails() {        
        $em = $this->getDoctrine()->getManager();
        $config = $em->getRepository('SistemaAdminBundle:Config')->findAll();
        $users = $em->getRepository('SistemaUserBundle:User')->findAll();
        foreach ($users as $user){
        $message = \Swift_Message::newInstance()
                ->setSubject($config[0]->getTitulo(). ' - Nueva versión')
                ->setFrom(array($config[0]->getMail() => $config[0]->getTitulo()))
                ->setTo($user->getEmail())
                ->setBody(
                    $this->renderView(                
                        'SistemaUserBundle:Registro:actualizacion.html.twig', 
                        array(
                            'name' => $user->getUsername(),
                            'titulo' => $config[0]->getTitulo(),
                            'usuario' => $user->getUsername(),
                            'password' => $user->getUsername(),
                            )
                    ),                    
                    'text/html'
                )       
            ;
            $this->get('mailer')->send($message);
        }    
        $this->get('session')->getFlashBag()->add('success', 'Datos de usuarios actualizados');
        return $this->redirect('user');
    }
}
