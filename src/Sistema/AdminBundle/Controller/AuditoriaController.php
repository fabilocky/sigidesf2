<?php

namespace Sistema\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\AdminBundle\Entity\Capa;
use Sistema\AdminBundle\Form\CapaType;
use Sistema\AdminBundle\Form\CapaFilterType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sistema\AdminBundle\Classes\GeoserverWrapper;

set_time_limit(10);

libxml_disable_entity_loader(false);

/**
 * Capa controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/auditor/auditoria")
 */
class AuditoriaController extends Controller {
    
    /**
     * Lists all Mapa entities.
     *
     * @Route("/", name="admin_auditoria")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {        
        $em = $this->getDoctrine()->getManager();
        $capas = $em->getRepository("SistemaUserBundle:UsuarioCapa")->findByEstado("En revision");

        return array(
            'capas'     => $capas,            
        );
    }
    
    /**
     * Lists all Mapa entities.
     *
     * @Route("/auditar", name="admin_auditoria_auditar")
     * @Method("POST")
     * @Template()
     */
    public function auditarAction(Request $request)
    {
        $idCapa = $request->get('idcapa');
        $epsg = $request->get('epsg');        
        $em = $this->getDoctrine()->getManager();
        $capa = $em->getRepository("SistemaUserBundle:UsuarioCapa")->find($idCapa);
        $config = $em->getRepository("SistemaAdminBundle:Config")->findAll();
        $metadatos = $capa->getMetadato();
        //ladybug_dump(explode(".", $capa->getShp())[0]);die();
        $delete = shell_exec("rm files/capas/".$capa->getShp(). ".json"); // elimino si existe
        $salida = shell_exec("ogr2ogr -f GeoJSON -dialect SQLITE -sql "."'SELECT * FROM '" . explode(".", $capa->getShp())[0] . "' LIMIT 100'" .
                " -dim 2 -s_srs EPSG:". $epsg .
                " -t_srs EPSG:4326 files/capas/".$capa->getShp(). 
                ".json files/capas/".$capa->getShp());
        $string = file_get_contents("files/capas/".$capa->getShp(). ".json");
        $string = utf8_encode($string);
        $json_a = json_decode($string, true);      
        $library = simplexml_load_string(file_get_contents("files/metadatos/".$capa->getMetadato()));
        $arrayMetadatos["titulo"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->citation->children('gmd', true)->CI_Citation->children('gmd', true)->title->children('gco', true)->CharacterString;
        $arrayMetadatos["fecha"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->citation->children('gmd', true)->CI_Citation->children('gmd', true)->date->children('gmd', true)->CI_Date->children('gmd', true)->date->children('gco', true)->DateTime;
        $arrayMetadatos["tipofecha"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->citation->children('gmd', true)->CI_Citation->children('gmd', true)->date->children('gmd', true)->CI_Date->children('gmd', true)->dateType->children('gmd', true)->CI_DateTypeCode->attributes();
        $arrayMetadatos["Resumencd"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->abstract->children('gco', true)->CharacterString;
        $arrayMetadatos["pcnombre"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->pointOfContact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->individualName->children('gco', true)->CharacterString;
        $arrayMetadatos["pcorganizacion"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->pointOfContact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->organisationName->children('gco', true)->CharacterString;
        $arrayMetadatos["pccargo"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->pointOfContact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->positionName->children('gco', true)->CharacterString;
        $arrayMetadatos["pcrol"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->pointOfContact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->role->children('gmd', true)->CI_RoleCode->attributes();
        $arrayMetadatos["pctelefono"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->pointOfContact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->phone->children('gmd', true)->CI_Telephone->children('gmd', true)->voice->children('gco', true)->CharacterString;
        $arrayMetadatos["pcfax"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->pointOfContact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->phone->children('gmd', true)->CI_Telephone->children('gmd', true)->facsimile->children('gco', true)->CharacterString;
        $arrayMetadatos["pcdireccion"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->pointOfContact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->address->children('gmd', true)->CI_Address->children('gmd', true)->deliveryPoint->children('gco', true)->CharacterString;
        $arrayMetadatos["pcciudad"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->pointOfContact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->address->children('gmd', true)->CI_Address->children('gmd', true)->city->children('gco', true)->CharacterString;
        $arrayMetadatos["pcprovincia"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->pointOfContact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->address->children('gmd', true)->CI_Address->children('gmd', true)->administrativeArea->children('gco', true)->CharacterString;
        $arrayMetadatos["pccodpostal"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->pointOfContact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->address->children('gmd', true)->CI_Address->children('gmd', true)->postalCode->children('gco', true)->CharacterString;
        $arrayMetadatos["pcpais"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->pointOfContact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->address->children('gmd', true)->CI_Address->children('gmd', true)->country->children('gco', true)->CharacterString;
        $arrayMetadatos["pcemail"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->pointOfContact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->address->children('gmd', true)->CI_Address->children('gmd', true)->electronicMailAddress->children('gco', true)->CharacterString;
        $arrayMetadatos["palabrasclave"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->descriptiveKeywords->children('gmd', true)->MD_Keywords->children('gmd', true)->keyword->children('gco', true)->CharacterString;
        $arrayMetadatos["tipopclave"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->descriptiveKeywords->children('gmd', true)->MD_Keywords->children('gmd', true)->type->children('gmd', true)->MD_KeywordTypeCode->attributes();
        $arrayMetadatos["tiporepesp"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->spatialRepresentationType->children('gmd', true)->MD_SpatialRepresentationTypeCode->attributes();
        $arrayMetadatos["denominador"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->spatialResolution->children('gmd', true)->MD_Resolution->children('gmd', true)->equivalentScale->children('gmd', true)->MD_RepresentativeFraction->children('gmd', true)->denominator->children('gco', true)->Integer;
        $arrayMetadatos["idiomacd"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->language->children('gmd', true)->LanguageCode->attributes()['codeListValue'];                
        $arrayMetadatos["caracterescd"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->characterSet->children('gmd', true)->MD_CharacterSetCode->attributes();
        $arrayMetadatos["temacd"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->topicCategory->children('gmd', true)->MD_TopicCategoryCode;
        $arrayMetadatos["desde"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->extent[0]->children('gmd', true)->EX_Extent->children('gmd', true)->temporalElement->children('gmd', true)->EX_TemporalExtent->children('gmd', true)->extent->children('gml', true)->TimePeriod->children('gml', true)->beginPosition;
        $arrayMetadatos["hasta"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->extent[0]->children('gmd', true)->EX_Extent->children('gmd', true)->temporalElement->children('gmd', true)->EX_TemporalExtent->children('gmd', true)->extent->children('gml', true)->TimePeriod->children('gml', true)->endPosition;
        $arrayMetadatos["loeste"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->extent[1]->children('gmd', true)->EX_Extent->children('gmd', true)->geographicElement->children('gmd', true)->EX_GeographicBoundingBox->children('gmd', true)->westBoundLongitude->children('gco', true)->Decimal;
        $arrayMetadatos["leste"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->extent[1]->children('gmd', true)->EX_Extent->children('gmd', true)->geographicElement->children('gmd', true)->EX_GeographicBoundingBox->children('gmd', true)->eastBoundLongitude->children('gco', true)->Decimal;
        $arrayMetadatos["lsur"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->extent[1]->children('gmd', true)->EX_Extent->children('gmd', true)->geographicElement->children('gmd', true)->EX_GeographicBoundingBox->children('gmd', true)->southBoundLatitude->children('gco', true)->Decimal;
        $arrayMetadatos["lnorte"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->extent[1]->children('gmd', true)->EX_Extent->children('gmd', true)->geographicElement->children('gmd', true)->EX_GeographicBoundingBox->children('gmd', true)->northBoundLatitude->children('gco', true)->Decimal;
        $arrayMetadatos["nomgeog"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->extent[1]->children('gmd', true)->EX_Extent->children('gmd', true)->description->children('gco', true)->CharacterString;
        $arrayMetadatos["infocomp"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->supplementalInformation->children('gco', true)->CharacterString;
        $arrayMetadatos["njerarquico"] = $library->children('gmd', true)->dataQualityInfo->children('gmd', true)->DQ_DataQuality->children('gmd', true)->scope->children('gmd', true)->DQ_Scope->children('gmd', true)->level->children('gmd', true)->MD_ScopeCode->attributes();
        $arrayMetadatos["linaje"] = $library->children('gmd', true)->dataQualityInfo->children('gmd', true)->DQ_DataQuality->children('gmd', true)->lineage->children('gmd', true)->LI_Lineage->children('gmd', true)->statement->children('gco', true)->CharacterString;
        $arrayMetadatos["codigoref"] = $library->children('gmd', true)->referenceSystemInfo->children('gmd', true)->MD_ReferenceSystem->children('gmd', true)->referenceSystemIdentifier->children('gmd', true)->RS_Identifier->children('gmd', true)->code->children('gco', true)->CharacterString;
        $arrayMetadatos["distnom"] = $library->children('gmd', true)->distributionInfo->children('gmd', true)->MD_Distribution->children('gmd', true)->distributionFormat->children('gmd', true)->MD_Format->children('gmd', true)->name->children('gco', true)->CharacterString;
        $arrayMetadatos["distversion"] =$library->children('gmd', true)->distributionInfo->children('gmd', true)->MD_Distribution->children('gmd', true)->distributionFormat->children('gmd', true)->MD_Format->children('gmd', true)->version->children('gco', true)->CharacterString;
        $arrayMetadatos["url"] = $library->children('gmd', true)->distributionInfo->children('gmd', true)->MD_Distribution->children('gmd', true)->transferOptions->children('gmd', true)->MD_DigitalTransferOptions->children('gmd', true)->onLine->children('gmd', true)->CI_OnlineResource->children('gmd', true)->linkage->children('gmd', true)->URL;
        $arrayMetadatos["descripcion"] = $library->children('gmd', true)->distributionInfo->children('gmd', true)->MD_Distribution->children('gmd', true)->transferOptions->children('gmd', true)->MD_DigitalTransferOptions->children('gmd', true)->onLine->children('gmd', true)->CI_OnlineResource->children('gmd', true)->description->children('gco', true)->CharacterString;
        $arrayMetadatos["identificador"] = $library->children('gmd', true)->fileIdentifier->children('gco', true)->CharacterString;
        $arrayMetadatos["idiomamd"] = $library->children('gmd', true)->language->children('gmd', true)->LanguageCode->attributes()['codeListValue'];
        $arrayMetadatos["normamd"] = $library->children('gmd', true)->metadataStandardName->children('gco', true)->CharacterString;
        $arrayMetadatos["versionmd"] = $library->children('gmd', true)->metadataStandardVersion->children('gco', true)->CharacterString;
        $arrayMetadatos["caracteresmd"] = $library->children('gmd', true)->characterSet->children('gmd', true)->MD_CharacterSetCode->attributes();
        $arrayMetadatos["fechamd"] = $library->children('gmd', true)->dateStamp->children('gco', true)->DateTime;
        $arrayMetadatos["nombremd"] =$library->children('gmd', true)->contact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->individualName->children('gco', true)->CharacterString;
        $arrayMetadatos["organizacionmd"] = $library->children('gmd', true)->contact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->organisationName->children('gco', true)->CharacterString;
        $arrayMetadatos["cargomd"] = $library->children('gmd', true)->contact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->positionName->children('gco', true)->CharacterString;
        $arrayMetadatos["rolmd"] = $library->children('gmd', true)->contact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->role->children('gmd', true)->CI_RoleCode->attributes();
        $arrayMetadatos["telefonomd"] = $library->children('gmd', true)->contact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->phone->children('gmd', true)->CI_Telephone->children('gmd', true)->voice->children('gco', true)->CharacterString;
        $arrayMetadatos["faxmd"] = $library->children('gmd', true)->contact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->phone->children('gmd', true)->CI_Telephone->children('gmd', true)->facsimile->children('gco', true)->CharacterString;
        $arrayMetadatos["direccionmd"] = $library->children('gmd', true)->contact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->address->children('gmd', true)->CI_Address->children('gmd', true)->deliveryPoint->children('gco', true)->CharacterString;
        $arrayMetadatos["ciudadmd"] = $library->children('gmd', true)->contact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->address->children('gmd', true)->CI_Address->children('gmd', true)->city->children('gco', true)->CharacterString;
        $arrayMetadatos["provinciamd"] = $library->children('gmd', true)->contact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->address->children('gmd', true)->CI_Address->children('gmd', true)->administrativeArea->children('gco', true)->CharacterString;
        $arrayMetadatos["codpostalmd"] = $library->children('gmd', true)->contact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->address->children('gmd', true)->CI_Address->children('gmd', true)->postalCode->children('gco', true)->CharacterString;
        $arrayMetadatos["paismd"] = $library->children('gmd', true)->contact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->address->children('gmd', true)->CI_Address->children('gmd', true)->country->children('gco', true)->CharacterString;
        $arrayMetadatos["emailmd"] = $library->children('gmd', true)->contact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->address->children('gmd', true)->CI_Address->children('gmd', true)->electronicMailAddress->children('gco', true)->CharacterString;                      
                
        $form = $this->createForm(new \Sistema\AdminBundle\Form\AuditoriaType(), null, array(
            'action' => $this->generateUrl('admin_auditoria_validar', array('idCapa' => $idCapa)),
            'method' => 'GET',
        ))
            ->add('epsg', 'text', array(
                'attr'=>array('style'=>'display:none;',
                    'value' => $epsg),
                'label_attr' => array(
                    'style'=>'display:none;'
                ),
                )) 
            ->add('save', 'submit', array(
                'label' => 'Enviar informe',
                'attr'  => array(                    
                    'class' => 'btn btn-success'
                )
                ));            
       
        
        return array(
            'capa'     => $capa,
            'atributos' => $json_a["features"][0]["properties"],
            'metadatos' => $arrayMetadatos,
            'form' => $form->createView(),
            'config' => $config[0]
        );
    }
    
    /**
     * Lists all Mapa entities.
     *
     * @Route("/validar/{idCapa}", name="admin_auditoria_validar")
     * @Method("GET")
     * @Template()
     */
    public function validarAction($idCapa, Request $request)
    {
        $resultado = $request->get('sistema_adminbundle_auditoria')["resultado"];
        $mail = $request->get('sistema_adminbundle_auditoria')["informe"];
        $em = $this->getDoctrine()->getManager();
        $capa = $em->getRepository("SistemaUserBundle:UsuarioCapa")->find($idCapa);
        $config = $em->getRepository('SistemaAdminBundle:Config')->findAll();
        if ($resultado == "Aprobado"){
        //Me conecto a postgres para controlar si la capa existe, si existe la elimino para actualizar
        $connectionFactory = $this->container->get('doctrine.dbal.connection_factory');
        $connection = $connectionFactory->createConnection(
            array('pdo' => new \PDO("pgsql:host=".$config[0]->getHostpgsql().";dbname=".$config[0]->getBdpgsql(), $config[0]->getUserpgsql(), $config[0]->getPasspgsql()))
        );
        $query = $connection->executeQuery("DROP TABLE IF EXISTS ". explode(".", $capa->getShp())[0]);
        $results = $query->fetchAll();        
        // fin control de postgres       
        
        // insertar metadatos
        $connection2 = $connectionFactory->createConnection(
            array('pdo' => new \PDO("pgsql:host=".$config[0]->getHostpgsql().";dbname=".$config[0]->getBdmetadatospgsql(), $config[0]->getUserpgsql(), $config[0]->getPasspgsql()))
        );
        $filemetadatos = file_get_contents("files/metadatos/".$capa->getMetadato());
        $library = simplexml_load_string($filemetadatos);
        $uuid = $library->children('gmd', true)->fileIdentifier->children('gco', true)->CharacterString;
        $fecha = date("Y-m-d") . 'T' . date("H:i:s");
        // controlo que existan los metadatos en la bd
        $controlquery = $connection2->executeQuery("SELECT * FROM metadata WHERE uuid = '$uuid'");
        $resultscontrol = $controlquery->fetchAll();
        // fin control de existencia
        if (empty($resultscontrol)) {// si no extiste metadato inserto
            $query1 = $connection2->executeQuery("SELECT MAX(id) AS id FROM metadata");
        $results1 = $query1->fetchAll();
        $id = $results1[0]["id"] + 1;
        $query2 = $connection2->executeQuery("INSERT INTO metadata"
               . "(id,uuid,schemaid,istemplate,isharvested,createdate,changedate,data,source,root,owner,groupowner)" 
               . "VALUES "
               . "('$id', '$uuid', 'iso19139', 'n','n', '$fecha', '$fecha', '$filemetadatos','dummy','gmd:MD_Metadata', '1', '1')");
        $results2 = $query2->fetchAll();
        $query3 = $connection2->executeQuery("INSERT INTO operationallowed"
               . "(groupid,metadataid,operationid)" 
               . "VALUES "
               . "('1', '$id', '0')");
        $results3 = $query2->fetchAll();
        } else { //si existe actualizo
            $updatequery = $connection2->executeQuery("UPDATE metadata SET data = '$filemetadatos' WHERE uuid = '$uuid'");
            $resultscontrol = $controlquery->fetchAll();
        }
        
        // fin inserto metadatos
        shell_exec($this->insertarBdShp2pgsql($request->get('sistema_adminbundle_auditoria')["epsg"], 'prueba', 'LATIN1', $capa, $config[0]));
        shell_exec($this->insertarBdPsql($capa, $config[0]));
        $options = array(
            'layer'       => explode(".", $capa->getShp())[0],
            'workspace'   => $config[0]->getWorkspacegs(),
            'datastore'   => $config[0]->getDatastoregs(),
            'description' => 'Capa publicada automaticamente',
            'srs'         => $request->get('sistema_adminbundle_auditoria')["epsg"]
        );
        $this->apiGeoserverWrapper('createlayer', $options, $config[0]);
        if ($capa->getCapa()){
            $capa->getCapa()->setActivo(true);
            $capa->setEstado("publicado");
            $capa->setInforme($mail);
        } else {
        $publicacion = new Capa();
        $publicacion->setTitulo($capa->getNombre());
        $publicacion->setCapa(explode(".", $capa->getShp())[0]);
        $publicacion->setActivo(true);
        $publicacion->setBase(false);        
        $publicacion->setCategoria($capa->getCategoria());
        $publicacion->setMetadatos($config[0]->getUrlgeonetwork()."/srv/es/main.home?uuid=".$uuid); 
        $servidor = $em->getRepository("SistemaAdminBundle:Servidor")->findOneBy(array("nombre" => "local"));
        $publicacion->setServidor($servidor);
        $em->persist($publicacion);
        $capa->setEstado("publicado");
        $capa->setCapa($publicacion);
        $capa->setInforme($mail);
        }              
        } else {
            $capa->setEstado("rechazado");            
            $capa->setInforme($mail);
        }
        $em->flush();  
        $message = \Swift_Message::newInstance()
                ->setSubject('Informe de solicitud de alta de capa')
                ->setFrom(array($config[0]->getMail() => $config[0]->getTitulo()))
                ->setTo($capa->getUsuario()->getEmail())
                ->setBody(
                    "informe de: " . $capa->getNombre() .
                        "<br>". $mail,
                    'text/html'
                )       
            ;
        $this->get('mailer')->send($message);
        return $this->redirect($this->generateUrl('admin_auditoria'));
    }
    
    public function insertarBdShp2pgsql($epsg, $arch, $encoding, $capa, $config) {
       
        if ($encoding) {
            $sql = "shp2pgsql -W ".$encoding." -s " . $epsg . " files/capas/" 
             . $capa->getShp() . " public." . explode(".", $capa->getShp())[0] . " > files/capas/" . explode(".", $capa->getShp())[0] . ".sql";
        } 
        return $sql;
    }
    
    public function insertarBdPsql($capa, $config) {
        return "psql -U ". $config->getUserpgsql() ." -h ". $config->getHostpgsql() . " ". $config->getBdpgsql() ." -w -f files/capas/" 
             . explode(".", $capa->getShp())[0] . ".sql > files/capas/" . explode(".", $capa->getShp())[0] . ".log 2>&1";
    }
    
    /**
     * Executes publicar capa action
     */  
    private function apiGeoserverWrapper($action, $options, $config) 
    {
        $res = null;
	$geoserver = new GeoserverWrapper($config->getUrlgeoserver(), $config->getUsergs(), $config->getPassgs());
        switch ($action) {
            case 'resrest':
                $res = $geoserver->resRest(
                    $options['url']
                );
                break;
//            case 'listworkspaces':
//                    print_r($geoserver->listWorkspaces());
//                    break;
//            case 'createworkspace':
//                    print_r($geoserver->createWorkspace($_REQUEST['workspace']));
//                    break;
//            case 'deleteworkspace':
//                    print_r($geoserver->deleteWorkspace($_REQUEST['workspace']));
//                    break;
//
//            case 'listdatastores':
//                    print_r($geoserver->listDataStores($_REQUEST['workspace']));
//                    break;
//            case 'createdatastore':
//                    print_r($geoserver->createShpDirDataStore($_REQUEST['datastore'], $_REQUEST['workspace'], $_REQUEST['location']));
//                    break;
//            case 'createdatastorepostgis':
//                    print_r($geoserver->createPostGISDataStore($_REQUEST['datastore'], $_REQUEST['workspace'], $_REQUEST['dbname'], $_REQUEST['dbuser'], $_REQUEST['dbpass'], $_REQUEST['dbhost']));
//                    break;
//            case 'deletedatastore':
//                    print_r($geoserver->deleteDataStore($_REQUEST['datastore'], $_REQUEST['workspace']));
//                    break;
//
//            case 'listlayers':
//                    print_r($geoserver->listLayers($_REQUEST['workspace'], $_REQUEST['datastore']));
//                    break;
            case 'createlayer':
                $geoserver->createLayer(
                    $options['layer'],
                    $options['workspace'],
                    $options['datastore'],
                    $options['description'],
                    $options['srs']
                );
                break;
//            case 'deletelayer':
//                    print_r($geoserver->deleteLayer($_REQUEST['layer'], $_REQUEST['workspace'], $_REQUEST['datastore']));
//                    break;
//            case 'viewlayer':
//                    if ($_REQUEST['format'] == 'LEGEND') {
//                            echo '<img alt="Embedded Image" src="data:image/png;base64,'.base64_encode($geoserver->viewLayerLegend($_REQUEST['layer'], $_REQUEST['workspace'])).'"/>';
//
//                    } else {
//                            print_r($geoserver->viewLayer($_REQUEST['layer'], $_REQUEST['workspace'], $_REQUEST['format']));
//                    }
//                    break;
            case 'liststylesbyname':
                $res = $geoserver->getStyleByName(
                    $options['style']
                );
                break;
            case 'getstyledefaultbylayer':
                $res = $geoserver->getStyleDefaultByLayer(
                    $options['layer']
                );
                break;
            case 'liststylesbylayer':
                $res = $geoserver->listStylesByLayerSLD(
                    $options['layer']
                );
                break;
//            case 'liststyles':
//                    print_r($geoserver->listStyles());
//                    break;
            case 'createstyle':
                $geoserver->createStyle(
                    $options['stylename'],
                    $options['sld']
                );
                break;
            case 'createupdatestyle':
                $geoserver->createUpdateStyle(
                    $options['stylename'],
                    $options['sld']
                );
                break;
            case 'updatestyle':
                $geoserver->updateStyle(
                    $options['stylename'],
                    $options['sld']
                );
                break;
//            case 'deletestyle':
//                    print_r($geoserver->deleteStyle($_REQUEST['stylename']));
//                    break;
            case 'assignstyle':
                $geoserver->addStyleToLayer(
                    $options['layer'],
                    $options['workspace'],
                    $options['stylename']
                );
                break;
            case 'setdefaultstyle':
                $geoserver->setDefaultStyleToLayer(
                    $options['layer'],
                    $options['workspace'],
                    $options['stylename']
                );
                break;
//            case 'wfs-t':
//                    print_r($geoserver->executeWFSTransaction(stripslashes($_REQUEST['transaction'])));
//                    break;
	}
        return $res;
    }
}