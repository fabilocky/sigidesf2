<?php

/**
 *  Description of comandosCom
 * 
 *  @author Gonzalo <gonkpo@gmail.com>
 */
//require 'json.hpack.php';

class ComandosCom
{
    protected $_username;
    protected $_password;
    protected $_host;
    protected $_dbdev;
    protected $_dbprod;

    public function __construct()
    {
        $this->_username = 'postgres';
        $this->_password = 'postgres';
        $this->_host = 'localhost';
        $this->_dbdev = 'zmapas';
        $this->_dbprod = 'zmapas';
        $this->_dbproyecto = 'zmapas';
    }
//    /*
//     * 
//     */
//    public function insertarBdUTF8Shp2pgsql($epsg, $arch) {
//        return "shp2pgsql -W UTF8 -s " . $epsg . " -I uploads/shapes/" 
//            . $arch . " public." . $arch . " > uploads/shapes/" . $arch . ".sql";
//    }
//    /*
//     * 
//     */
//    public function insertarBdUTF8Psql($arch) {
//        return "psql -U ".$this->_username." -h ".$this->_host." ".$this->_dbdev." -w -f uploads/shapes/" 
//             . $arch . ".sql > uploads/shapes/" . $arch . ".log 2>&1";
//    }
    /*
     * 
     */
    public function insertarBdShp2pgsql($epsg, $arch, $encoding, $tabla = null) {
        if (is_null($tabla)) {
            $tabla = $arch;
        }
        if ($encoding) {
            $sql = "shp2pgsql -W ".$encoding." -s " . $epsg . " -I uploads/shapes/" 
             . $arch . " public." . $tabla . " > uploads/shapes/" . $tabla . ".sql";
        } else{
            $sql = "shp2pgsql -s " . $epsg . " -I uploads/shapes/" 
             . $arch . " public." . $tabla . " > uploads/shapes/" . $tabla . ".sql";
        }
        return $sql;
    }
    /*
     * 
     */
    public function insertarBdPsql($arch) {
        return "psql -U ".$this->_username." -h ".$this->_host." ".$this->_dbdev." -w -f uploads/shapes/" 
             . $arch . ".sql > uploads/shapes/" . $arch . ".log 2>&1";
    }
    /*
     * 
     */
    public function insertarBdTemporalShp2pgsql($epsg, $arch) {
        return "shp2pgsql -s " . $epsg . " -I uploads/shapes/" 
             . $arch . " public.temporal > uploads/shapes/" . $arch . ".sql";
    }
    /*
     * 
     */
    public function insertarBdTemporalPsql($arch) {
        return "psql -U ".$this->_username." -h ".$this->_host." ".$this->_dbdev." -w -f uploads/shapes/" 
             . $arch . ".sql > uploads/shapes/" . $arch . ".log 2>&1";
    }
    /*
     * 
     */
    public function insertarBdTemporalUTF8Shp2pgsql($epsg, $arch) {
        return "shp2pgsql -W UTF8 -s " . $epsg . " -I uploads/shapes/" 
             . $arch . " public.temporal > uploads/shapes/" . $arch . ".sql";
    }
    /*
     * 
     */
    public function insertarBdTemporalUTF8Psql($arch) {
        return "psql -U ".$this->_username." -h ".$this->_host." ".$this->_dbdev." -w -f uploads/shapes/" 
             . $arch . ".sql > uploads/shapes/" . $arch . ".log 2>&1";
    }
//    /*
//     * 
//     */
//    public function generaSQLCapaAuditoria($capa) {
//        return "shp2pgsql -I uploads/shapes/" . $capa . " public." 
//                 . $capa . " > uploads/shapes/" . $capa . ".sql";
//    }
    /*
     * 
     */
    public function insertarSQLCapaAuditoria($arch) {
        return "psql -U ".$this->_username." -h ".$this->_host." ".$this->_dbprod." -w -f uploads/shapes/" 
             . $arch . ".sql > uploads/shapes/" . $arch . ".log 2>&1";
    }
    /**
     * Extraer capa
     */
    public function extractCapa($capa)
    {
        return "pgsql2shp -h ".$this->_host." -u ".$this->_username." -f uploads/descargas/" . $capa . " ".$this->_dbprod." " . $capa;
    }
    /**
     * Extraer capa
     */
    public function extractProyecto($capa)
    {
        return "pgsql2shp -h ".$this->_host." -u ".$this->_username." -f uploads/descargas/" . $capa . " ".$this->_dbproyecto." " . $capa;
    }
//    /**
//     * Extraer capa en formato KML
//     */
//    public function extractCapaKML($capa)
//    {
//        return "ogr2ogr -f 'KML' uploads/descargas/".$capa.".kml PG:'host=".$this->_host." user=".$this->_username." dbname=".$this->_dbprod."' ".$capa." -dsco NameField=name";
//    }
    /**
     * Extraer capa
     */
    public function extractAuditoriaCapa($capa)
    {
        return "pgsql2shp -h ".$this->_host." -u ".$this->_username." -f uploads/descargas/" . $capa . " ".$this->_dbdev." " . $capa;
    }
    /**
     * Genera GeoJSON
     */
    public function generaGeoJSON($capa)
    {
        return 'ogr2ogr -f "GeoJSON" uploads/geojson/' . $capa . '.json -s_srs EPSG:22175 -t_srs EPSG:4326 '
                     . 'PG:"host='.$this->_host.' user='.$this->_username.' dbname='.$this->_dbdev.' password='.$this->_password.'" -sql "select * '
                     . 'from ' 
                     . $capa . '"';
    }
    /**
     * Convert GPX to KML use https://github.com/mapbox/togeojson
     */
    public function convertGPXtoJSON($capa)
    {
        return 'togeojson uploads/gpx/'.$capa.'.gpx > uploads/geojson/'.$capa.'.json';
    }
    /**
     * Save Proyecto On Database
     */
    public function saveProyectoOnDatabase($file, $layerName)
    {
        return 'ogr2ogr -overwrite -f PostgreSQL -a_srs EPSG:4326'
            .' PG:"host='.$this->_host
            .' user='.$this->_username
            .' dbname='.$this->_dbproyecto
            .' password='.$this->_password.'"'
            .' '.$file
            .' -nln '.$layerName
            .' -lco GEOMETRY_NAME=the_geom'
            .' -lco GEOMETRY_AS_COLLECTION=YES'
        ;
    }
//    /**
//     * Comprimir GeoJSON
//     */
//    public function comprimirGeoJSON($capa)
//    {
//        $geojsonFile = sfConfig::get('app_path_geojson') . $capa . '.json';
//        $geojsonContent = file_get_contents($geojsonFile);
//        $objGeojson = json_decode($geojsonContent, true);
//        //$fileJSON = $objGeojson["features"][0]['properties'];
//        //var_dump($objGeojson);echo '<br><br>';
//        $geojsonComprimido = sfConfig::get('app_path_geojson') . 'comprimido.json';
//        $jsonComprimido = json_encode(json_hpack($objGeojson, 3));
//        file_put_contents($geojsonComprimido, $jsonComprimido);
//        //var_dump($jsonComprimido);
//    }
}
