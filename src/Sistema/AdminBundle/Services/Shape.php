<?php
namespace Sistema\AdminBundle\Services;

use Symfony\Component\HttpFoundation\File\UploadedFile;

use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Util\Transliterator;
use Vich\UploaderBundle\Naming\NamerInterface;
use Vich\UploaderBundle\Naming\ConfigurableInterface;
/**
 * OrignameNamer
 *
 * @author Ivan Borzenkov <ivan.borzenkov@gmail.com>
 */
class Shape implements NamerInterface, ConfigurableInterface
{
    private $container;
    
    public function __construct($container) {
        $this->container = $container;
    }

    /**
     * @var bool
     */
    private $transliterate = false;

    /**
     * @param array $options Options for this namer. The following options are accepted:
     *                         - transliterate: whether the filename should be transliterated or not.
     */
    public function configure(array $options)
    {
        $this->transliterate = isset($options['transliterate']) ? (bool) $options['transliterate'] : $this->transliterate;
    }

    /**
     * {@inheritDoc}
     */
    public function name($object, PropertyMapping $mapping)
    {
        $sc = $this->container->get('security.context');
        $user = $sc->getToken()->getUser()->getUsername();
        $file = $mapping->getFile($object);
        $name = $file->getClientOriginalName();
        

        if ($this->transliterate) {
            $name = Transliterator::transliterate($name);
        }

        /** @var $file UploadedFile */

        return strtolower(trim(preg_replace('[\s+]', '_', $user.'_'.$name)));
    }
}
