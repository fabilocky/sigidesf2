<?php

namespace Sistema\AdminBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Sistema\AdminBundle\Entity\Config;
use Sistema\AdminBundle\Entity\Servidor;
use Sistema\AdminBundle\Entity\Capa;

class LoadProveedor extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface {
    public function load(ObjectManager $manager)
    {       
        $capa = new \Sistema\AdminBundle\Entity\Proveedor();      
        $capa->setTipo("Bing");
        $capa->setMapid("Road");
        $capa->setNombre("Bing Calles");
        $capa->setActivo(true);       
        $manager->persist($capa);
        $manager->flush();
        $capa->setTipo("Bing");
        $capa->setMapid("AerialWithLabels");
        $capa->setNombre("Bing Aereo con Etiquetas");
        $capa->setActivo(true);       
        $manager->persist($capa);
        $manager->flush();
        $capa->setTipo("Bing");
        $capa->setMapid("Aerial");
        $capa->setNombre("Bing Aereo");
        $capa->setActivo(true);       
        $manager->persist($capa);
        $manager->flush();
    }
    
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 4; // el orden en el cual serán cargados los accesorios
    }
}
