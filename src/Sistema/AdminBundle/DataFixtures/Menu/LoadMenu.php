<?php

namespace Sistema\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Sistema\UserBundle\Entity\Menu;
use Sistema\UserBundle\Entity\Role;

class LoadMenu extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{
    
    
    public function load(ObjectManager $manager)
    {
//        $roles = array(
//            '0'  => array('roleName' => 'ADMIN'),
//            '1'  => array('roleName' => 'VISUALIZADOR'),
//            '2'  => array('roleName' => 'ADMIN_REGLAS'),
//            '3'  => array('roleName' => 'ADMIN_USUARIOS'),
//            '4'  => array('roleName' => 'ADMIN_PROYECTOS'),
//            '5'  => array('roleName' => 'ADMIN_PERMISOS'),
//            '6'  => array('roleName' => 'ADMIN_CAPAS'),
//            '7'  => array('roleName' => 'ADMIN_CATEGORIAS'),
//            '8'  => array('roleName' => 'ADMIN_SERVIDORES'),
//            '9'  => array('roleName' => 'ADMIN_PROVEEDORES'),
//            '10'  => array('roleName' => 'ADMIN_CONFIGURACION'),
//            '11'  => array('roleName' => 'ADMIN_ARBOL'),
//            '12'  => array('roleName' => 'ADMIN_EMPRESAS'),
//            '13'  => array('roleName' => 'ADMIN_LOCALIDADES'),
//            '14'  => array('roleName' => 'IMPRIMIR'),
//            '15'  => array('roleName' => 'EXPORTAR_DATOS'),
//            '16'  => array('roleName' => 'CARGAR_PLANCHETAS'),
//            '17'  => array('roleName' => 'CARGAR_ILICITOS'),
//            '18'  => array('roleName' => 'CARGAR_IMAGENES'),
//        );

//        foreach ($roles as $key => $r) {
//            $role = new Role();
//            $role->setRoleName($r['roleName']);
//            $role->setName();
//            $role->serialize();
//            $this->addReference($role->getName(), $role);
//            //$manager->persist($role);
//        }
        
//        $em = $this->getDoctrine()->getManager();
        $roleadmin = $manager->getRepository('SistemaUserBundle:Role')->findBy(array('role_name' => 'ADMIN'));
//        $roleusuarios = $manager->getRepository('SistemaUserBundle:Role')->findBy(array('role_name' => 'ADMIN_USUARIOS'));
//        $rolevisualizador = $manager->getRepository('SistemaUserBundle:Role')->findBy(array('role_name' => 'VISUALIZADOR'));
//        $rolereglas = $manager->getRepository('SistemaUserBundle:Role')->findBy(array('role_name' => 'ADMIN_REGLAS'));
//        $roleproyectos = $manager->getRepository('SistemaUserBundle:Role')->findBy(array('role_name' => 'ADMIN_PROYECTOS'));
//        $rolepermisos = $manager->getRepository('SistemaUserBundle:Role')->findBy(array('role_name' => 'ADMIN_PERMISOS'));
//        $rolecapas = $manager->getRepository('SistemaUserBundle:Role')->findBy(array('role_name' => 'ADMIN_CAPAS'));
//        $rolecategorias = $manager->getRepository('SistemaUserBundle:Role')->findBy(array('role_name' => 'ADMIN_CATEGORIAS'));
//        $roleservidores = $manager->getRepository('SistemaUserBundle:Role')->findBy(array('role_name' => 'ADMIN_SERVIDORES'));
//        $roleproveedores = $manager->getRepository('SistemaUserBundle:Role')->findBy(array('role_name' => 'ADMIN_PROVEEDORES'));
//        $roleconfig = $manager->getRepository('SistemaUserBundle:Role')->findBy(array('role_name' => 'ADMIN_CONFIGURACION'));
//        $rolelocalidades = $manager->getRepository('SistemaUserBundle:Role')->findBy(array('role_name' => 'ADMIN_LOCALIDADES'));
//        $rolearbol = $manager->getRepository('SistemaUserBundle:Role')->findBy(array('role_name' => 'ADMIN_ARBOL'));
//        $roleempresas = $manager->getRepository('SistemaUserBundle:Role')->findBy(array('role_name' => 'ADMIN_EMPRESAS'));
//        $menues = array (
//            '0' => array(
//                'titulo' => 'Usuarios',
//                'activo' => true,
//                'url'    => '/user',
//                'ruta'    => 'user',
//                'orden'  => 1,
//                'roles'  => array($roleadmin[0], $roleusuarios[0]),
//            ),
//            '1' => array(
//                'titulo' => 'Reglas/Proyectos',
//                'activo' => true,
//                'url'    => '/reglaproyecto/',
//                'ruta'    => 'admin_reglaproyecto',
//                'orden'  => 2,
//                'roles'  => array($roleadmin[0], $rolereglas[0]),
//            ),
//            '2' => array(
//                'titulo' => 'Reglas/Permisos',
//                'activo' => true,
//                'url'    => '/reglapermiso/',
//                'ruta'    => 'admin_reglapermiso',
//                'orden'  => 2,
//                'roles'  => array($roleadmin[0], $rolereglas[0]),
//            ),
//            '3' => array(
//                'titulo' => 'Proveedores',
//                'activo' => true,
//                'url'    => '/proveedor/',
//                'ruta'    => 'admin_proveedor',
//                'orden'  => 3,
//                'roles'  => array($roleadmin[0], $roleproveedores[0]),
//            ),
//            '4' => array(
//                'titulo' => 'Capas',
//                'activo' => true,
//                'url'    => '/capa/',
//                'ruta'    => 'admin_capa',
//                'orden'  => 3,
//                'roles'  => array($rolecapas[0], $roleadmin[0]),
//            ),
//            '5' => array(
//                'titulo' => 'Proyectos',
//                'activo' => true,
//                'url'    => '/proyecto/',
//                'ruta'    => 'admin_proyecto',
//                'orden'  => 3,
//                'roles'  => array($roleadmin[0], $roleproyectos[0]),
//            ),
//            '6' => array(
//                'titulo' => 'Configuración',
//                'activo' => true,
//                'url'    => '/config/',
//                'ruta'    => 'admin_config',
//                'orden'  => 4,
//                'roles'  => array($roleadmin[0], $roleconfig[0]),
//            ),
//            '7' => array(
//                'titulo' => 'Arbol',
//                'activo' => true,
//                'url'    => '/config/arbol/configurar',
//                'ruta'    => 'admin_arbol',
//                'orden'  => 4,
//                'roles'  => array($roleadmin[0], $rolearbol[0]),
//            ),
//            '8' => array(
//                'titulo' => 'Categorías',
//                'activo' => true,
//                'url'    => '/capa/',
//                'ruta'    => 'admin_categoria',
//                'orden'  => 5,
//                'roles'  => array($roleadmin[0], $rolecategorias[0]),
//            ),
//            '9' => array(
//                'titulo' => 'Servidores',
//                'activo' => true,
//                'url'    => '/servidor/',
//                'ruta'    => 'admin_servidor',
//                'orden'  => 6,
//                'roles'  => array($roleadmin[0], $roleservidores[0]),
//            ),
//            '10' => array(
//                'titulo' => 'Empresas',
//                'activo' => true,
//                'url'    => '/empresa/',
//                'ruta'    => 'admin_empresa',
//                'orden'  => 7,
//                'roles'  => array($roleadmin[0], $roleempresas[0]),
//            ),
//            '11' => array(
//                'titulo' => 'Localidades',
//                'activo' => true,
//                'url'    => '/localidad/',
//                'ruta'    => 'admin_localidad',
//                'orden'  => 8,
//                'roles'  => array($roleadmin[0], $rolelocalidades[0]),
//            ),
//            '12' => array(
//                'titulo' => 'Menu',
//                'activo' => true,
//                'url'    => '/admin/menu',
//                'ruta'    => 'admin_menu',
//                'orden'  => 9,
//                'roles'  => null,
//            ),
//            '13' => array(
//                'titulo' => 'Permisos',
//                'activo' => true,
//                'url'    => '/superadmin/role',
//                'ruta'    => 'role',
//                'orden'  => 1,
//                'roles'  => array($roleadmin[0], $rolepermisos[0]),
//            ),
//        );

//        foreach ($menues as $key => $m) {
//            $menu = new Menu();
//            $menu->setTitulo($m['titulo']);
//            $menu->setActivo($m['activo']);
//            $menu->setUrl($m['url']);
//            $menu->setRuta($m['ruta']);
//            if (!is_null($m['roles'])) {
//                foreach ($m['roles'] as $r) {
//                    $menu->addMenuRole($r);
//                }
//            }
//            $menu->setOrden($m['orden']);
//            //$menu->setAcceso($m['acceso']);
//            $manager->persist($menu);
//        }
//
//        $manager->flush();
    }

    public function getOrder()
    {
        return 7;
    }

    public function setContainer(ContainerInterface $container = null)
    {
    }
}