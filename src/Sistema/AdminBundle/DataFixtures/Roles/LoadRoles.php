<?php

namespace Sistema\AdminBundle\DataFixtures\Roles;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Sistema\UserBundle\Entity\Role;

class loadRoles extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface {

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null) {
        $this->container = $container;
    }

    public function load(ObjectManager $manager) {
        $roles = array(
            '0'  => array('roleName' => 'CONSULTAS'),            
        );

        foreach ($roles as $key => $r) {
            $role = new Role();
            $role->setRoleName($r['roleName']);
            $role->setName();
            $role->serialize();
            $this->addReference($role->getName(), $role);
            $manager->persist($role);
        }

        $manager->flush();
    }   
    
    public function getOrder() {
        return 5; // the order in which fixtures will be loaded
    }
}
