<?php

namespace Sistema\AdminBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Sistema\UserBundle\Entity\Role;

class loadRole extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface {

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null) {
        $this->container = $container;
    }

    public function load(ObjectManager $manager) {
        $roles = array(
            '0'  => array('roleName' => 'ADMIN'),
            '1'  => array('roleName' => 'USER'),
            '2'  => array('roleName' => 'AUDITOR'),
            '3'  => array('roleName' => 'ADMIN_USUARIOS'),            
        );

        foreach ($roles as $key => $r) {
            $role = new Role();
            $role->setRoleName($r['roleName']);
            $role->setName();
            $role->serialize();
            $this->addReference($role->getName(), $role);
            $manager->persist($role);
        }

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder() {
        return 5; // the order in which fixtures will be loaded
    }

}
