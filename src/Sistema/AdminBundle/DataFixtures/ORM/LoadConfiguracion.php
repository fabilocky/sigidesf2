<?php

namespace Sistema\AdminBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Sistema\AdminBundle\Entity\Config;
use Sistema\AdminBundle\Entity\Servidor;
use Sistema\AdminBundle\Entity\Capa;

class LoadConfiguracion extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface {
    
    public function load(ObjectManager $manager)
    {
        $configuracion = new Config();
        $configuracion->setZoom(8);        
        $configuracion->setCentergoogle('-6792336.8419, -3003404.2243');
        $configuracion->setCenter('5623411, 6961891');
        $configuracion->setTitulo('SIGIDE - SISTEMA DE GESTION DE IDES');
        $configuracion->setUrlgeoserver('http://idechaco.gob.ar/geoserver');
        $configuracion->setUrlwfs('http://idechaco.gob.ar/geoserver');
        $configuracion->setHost('localhost');
        $configuracion->setDbname('usuarios');
        $configuracion->setDbname('usuarios');
        $manager->persist($configuracion);       
        $manager->flush();
    }
    
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1; // el orden en el cual serán cargados los accesorios
    }
}
