<?php

namespace Sistema\AdminBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Sistema\AdminBundle\Entity\Config;
use Sistema\AdminBundle\Entity\Servidor;
use Sistema\AdminBundle\Entity\Capa;

class LoadCapa extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface {
    public function load(ObjectManager $manager)
    {       
        $capa = new Capa();
        $capa->setServidor($this->getReference('servidor'));
        $capa->setTitulo("OSM");
        $capa->setCapa("osm_auto:all");
        $capa->setBase(true);
        $capa->setActivo(true);       
        $manager->persist($capa);
        $manager->flush();
    }
    
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 3; // el orden en el cual serán cargados los accesorios
    }
}
