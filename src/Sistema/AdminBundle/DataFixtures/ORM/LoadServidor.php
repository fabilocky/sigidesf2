<?php

namespace Sistema\AdminBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Sistema\AdminBundle\Entity\Config;
use Sistema\AdminBundle\Entity\Servidor;
use Sistema\AdminBundle\Entity\Capa;

class LoadServidor extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface {
    public function load(ObjectManager $manager)
    {
        
        $servidor = new Servidor();
        $servidor->setNombre("OSM");
        $servidor->setUrl("http://129.206.228.72/cached/osm?");        
        $manager->persist($servidor);        
        $manager->flush();
        
        $this->addReference('servidor', $servidor);
    }
    
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 2; // el orden en el cual serán cargados los accesorios
    }
}
