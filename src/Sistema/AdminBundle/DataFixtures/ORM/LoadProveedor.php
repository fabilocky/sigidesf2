<?php

namespace Sistema\AdminBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Sistema\AdminBundle\Entity\Config;
use Sistema\AdminBundle\Entity\Servidor;
use Sistema\AdminBundle\Entity\Capa;

class LoadProveedor extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface {
    public function load(ObjectManager $manager)
    {       
        $capa = new \Sistema\AdminBundle\Entity\Proveedor();        
        $capa->setTipo("OSM");
        $capa->setMapid("osm");
        $capa->setNombre("osm");
        $capa->setActivo(true);       
        $manager->persist($capa);
        $manager->flush();
        $capa = new \Sistema\AdminBundle\Entity\Proveedor();        
        $capa->setTipo("Google");
        $capa->setMapid("HYBRID");
        $capa->setNombre("Google Hybrid");
        $capa->setActivo(true);       
        $manager->persist($capa);
        $manager->flush();
        $capa = new \Sistema\AdminBundle\Entity\Proveedor();        
        $capa->setTipo("Google");
        $capa->setMapid("SATELLITE");
        $capa->setNombre("Google Satellite");
        $capa->setActivo(true);       
        $manager->persist($capa);
        $manager->flush();
        $capa = new \Sistema\AdminBundle\Entity\Proveedor();        
        $capa->setTipo("Google");
        $capa->setMapid("ROADMAP");
        $capa->setNombre("Google Streets");
        $capa->setActivo(true);       
        $manager->persist($capa);
        $manager->flush();
        $capa = new \Sistema\AdminBundle\Entity\Proveedor();        
        $capa->setTipo("Google");
        $capa->setMapid("TERRAIN");
        $capa->setNombre("Google Terrain");
        $capa->setActivo(true);       
        $manager->persist($capa);
        $manager->flush();        
    }
    
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 4; // el orden en el cual serán cargados los accesorios
    }
}
