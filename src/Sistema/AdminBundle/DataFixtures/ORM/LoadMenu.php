<?php

namespace Sistema\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Sistema\UserBundle\Entity\Menu;

class LoadMenu extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $menues = array (
            '0' => array(
                'titulo' => 'Usuarios',
                'activo' => true,
                'url'    => '/user',
                'ruta'    => 'user',
                'orden'  => 1,
                'roles'  => array($this->getReference('ROLE_ADMIN'), $this->getReference('ROLE_ADMIN_USUARIOS')),
            ),
//            '1' => array(
//                'titulo' => 'Reglas/Proyectos',
//                'activo' => true,
//                'url'    => '/reglaproyecto/',
//                'ruta'    => 'admin_reglaproyecto',
//                'orden'  => 2,
//                'roles'  => array($this->getReference('ROLE_ADMIN'), $this->getReference('ROLE_ADMIN_REGLAS')),
//            ),
//            '2' => array(
//                'titulo' => 'Reglas/Permisos',
//                'activo' => true,
//                'url'    => '/reglapermiso/',
//                'ruta'    => 'admin_reglapermiso',
//                'orden'  => 2,
//                'roles'  => array($this->getReference('ROLE_ADMIN'), $this->getReference('ROLE_ADMIN_REGLAS')),
//            ),
//            '3' => array(
//                'titulo' => 'Proveedores',
//                'activo' => true,
//                'url'    => '/proveedor/',
//                'ruta'    => 'admin_proveedor',
//                'orden'  => 3,
//                'roles'  => array($this->getReference('ROLE_ADMIN'), $this->getReference('ROLE_ADMIN_PROVEEDORES')),
//            ),
//            '4' => array(
//                'titulo' => 'Capas',
//                'activo' => true,
//                'url'    => '/capa/',
//                'ruta'    => 'admin_capa',
//                'orden'  => 3,
//                'roles'  => array($this->getReference('ROLE_ADMIN'), $this->getReference('ROLE_ADMIN_CAPAS')),
//            ),
//            '5' => array(
//                'titulo' => 'Proyectos',
//                'activo' => true,
//                'url'    => '/proyecto/',
//                'ruta'    => 'admin_proyecto',
//                'orden'  => 3,
//                'roles'  => array($this->getReference('ROLE_ADMIN'), $this->getReference('ROLE_ADMIN_PROYECTOS')),
//            ),
//            '6' => array(
//                'titulo' => 'Configuración',
//                'activo' => true,
//                'url'    => '/config/',
//                'ruta'    => 'admin_config',
//                'orden'  => 4,
//                'roles'  => array($this->getReference('ROLE_ADMIN'), $this->getReference('ROLE_ADMIN_CONFIGURACION')),
//            ),
//            '7' => array(
//                'titulo' => 'Arbol',
//                'activo' => true,
//                'url'    => '/config/arbol/configurar',
//                'ruta'    => 'admin_arbol',
//                'orden'  => 4,
//                'roles'  => array($this->getReference('ROLE_ADMIN'), $this->getReference('ROLE_ADMIN_ARBOL')),
//            ),
//            '8' => array(
//                'titulo' => 'Categorías',
//                'activo' => true,
//                'url'    => '/capa/',
//                'ruta'    => 'admin_categoria',
//                'orden'  => 5,
//                'roles'  => array($this->getReference('ROLE_ADMIN'), $this->getReference('ROLE_ADMIN_CATEGORIAS')),
//            ),
//            '9' => array(
//                'titulo' => 'Servidores',
//                'activo' => true,
//                'url'    => '/servidor/',
//                'ruta'    => 'admin_servidor',
//                'orden'  => 6,
//                'roles'  => array($this->getReference('ROLE_ADMIN'), $this->getReference('ROLE_ADMIN_SERVIDORES')),
//            ),
//            '10' => array(
//                'titulo' => 'Empresas',
//                'activo' => true,
//                'url'    => '/empresa/',
//                'ruta'    => 'admin_empresa',
//                'orden'  => 7,
//                'roles'  => array($this->getReference('ROLE_ADMIN'), $this->getReference('ROLE_ADMIN_EMPRESAS')),
//            ),
//            '11' => array(
//                'titulo' => 'Localidades',
//                'activo' => true,
//                'url'    => '/localidad/',
//                'ruta'    => 'admin_localidad',
//                'orden'  => 8,
//                'roles'  => array($this->getReference('ROLE_ADMIN'), $this->getReference('ROLE_ADMIN_LOCALIDADES')),
//            ),
//            '12' => array(
//                'titulo' => 'Menu',
//                'activo' => true,
//                'url'    => '/admin/menu',
//                'ruta'    => 'admin_menu',
//                'orden'  => 9,
//                'roles'  => null,
//            ),
//            '13' => array(
//                'titulo' => 'Permisos',
//                'activo' => true,
//                'url'    => '/superadmin/role',
//                'ruta'    => 'role',
//                'orden'  => 1,
//                'roles'  => array($this->getReference('ROLE_ADMIN'), $this->getReference('ROLE_ADMIN_PERMISOS')),
//            ),
        );

        foreach ($menues as $key => $m) {
            $menu = new Menu();
            $menu->setTitulo($m['titulo']);
            $menu->setActivo($m['activo']);
            $menu->setUrl($m['url']);
            $menu->setRuta($m['ruta']);
            if (!is_null($m['roles'])) {
                foreach ($m['roles'] as $r) {
                    $menu->addMenuRole($r);
                }
            }
            $menu->setOrden($m['orden']);
            //$menu->setAcceso($m['acceso']);
            $manager->persist($menu);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 7;
    }

    public function setContainer(ContainerInterface $container = null)
    {
    }
}