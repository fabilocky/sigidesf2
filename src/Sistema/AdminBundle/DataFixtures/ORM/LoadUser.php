<?php

namespace Sistema\AdminBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Sistema\UserBundle\Entity\User;

class LoadUser extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        //Creando Admin !!
        $user = new User();
        $user->setUsername('admin');
        $user->setNombre('admin');
        $user->setApellido('admin');
        $user->setDni(11222333);
        $user->setEmail('admin@admin');
        $user->setFechanac(new \DateTime('1970-12-12'));
        $user->setTelefono(44444444);
        $user->setIsActive(true);
        $user->setSalt(md5(time()));
        $user->setPassword('admin');
        $user->addRole($this->getReference('ROLE_ADMIN'));

        // Completar las propiedades que el usuario no rellena en el formulario
        $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
        $passwordCodificado = $encoder->encodePassword(
            $user->getPassword(),
            $user->getSalt()
        );
        $user->setPassword($passwordCodificado);
        // Guardar el nuevo usuario en la base de datos
        $manager->persist($user);
        $manager->flush();

        $this->addReference("admin", $user);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 6; // the order in which fixtures will be loaded
    }
}
