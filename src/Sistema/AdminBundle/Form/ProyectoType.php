<?php

namespace Sistema\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * ProyectoType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class ProyectoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('descripcion')
//            ->add('usuarios', 'select2', array(
//                'class' => 'Sistema\UserBundle\Entity\User',
//                'url'   => 'Proyecto_autocomplete_usuarios',
//                'configs' => array(
//                    'multiple' => true,//required true or false
//                    'width'    => 'off',
//                ),
//                'attr' => array(
//                    'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
//                )
//            ))
            ->add('zoom')
            ->add('center')
            ->add('centergoogle')
            ->add('capasvinculacion', 'select2', array(
                'label' => "Capas - Vinculaciones",
                'class' => 'Sistema\AdminBundle\Entity\Capa',
                'url'   => 'Servidor_autocomplete_capa',
                'configs' => array(
                    'multiple' => true,//required true or false
                    'width'    => 'off',
                ),
                'attr' => array(
                    'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                )
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\AdminBundle\Entity\Proyecto'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_adminbundle_proyecto';
    }
}
