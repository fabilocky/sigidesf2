<?php

namespace Sistema\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * ProveedorType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class ProveedorType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tipo', 'choice', array(
                'choices' => array(
                'Google'=>'Google',
                'Bing'=>'Bing',                
                'OSM'=>'OSM',                  
    ),
                
            ))
            ->add('mapid', null, array(
                'attr' => array('placeholder' => 'Indique el tipo de mapa del proveedor. Ej: Para Bing maps "Road", "AerialWithLabels", "Aerial", etc..'),
            ))
            ->add('nombre', null, array(
                'attr' => array('placeholder' => 'Título a mostrar'),
            ))
            ->add('orden')
            ->add('activo')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\AdminBundle\Entity\Proveedor'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_adminbundle_proveedor';
    }
}
