<?php

namespace Sistema\AdminBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class AddServidorSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::PRE_SUBMIT   => 'preSubmit'
        );
    }

    private function addServidorForm($form, $servidor = null)
    {
        $formOptions = array(
            'class'       => 'SistemaAdminBundle:Servidor',
            // 'mapped'        => false,
            'property'    => 'url',
            'label'       => false,
            // 'empty_value' => 'Seleccionar diseño',
//            'data'        => 1,
            'attr'        => array(
                'class' => 'col-lg-4',
            ),
        );

        if ($servidor) {
            $formOptions['data'] = $servidor;
        }

        $form->add('servidor', null, $formOptions);
    }

    public function preSetData(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        if (null === $data) {
            return;
        }

        $accessor = PropertyAccess::getPropertyAccessor();

        $capa = $accessor->getValue($data, 'capa');
        $servidor = ($capa) ? $capa->getServidor() : null;

        $this->addServidorForm($form, $servidor);
    }

    public function preSubmit(FormEvent $event)
    {
        $form = $event->getForm();

        $this->addServidorForm($form);
    }
}
