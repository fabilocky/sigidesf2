<?php

namespace Sistema\AdminBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Doctrine\ORM\EntityRepository;
use Sistema\AdminBundle\Entity\Capa;

class AddCapaSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::POST_SET_DATA => 'preSetData',
            FormEvents::PRE_SUBMIT    => 'preSubmit'
        );
    }

    private function addCapaForm($form, $servidor_id)
    {
        $formOptions = array(
            'class'       => 'SistemaAdminBundle:Capa',
            // 'empty_value'   => 'Capa',
            'property'    => 'capa',
            'label'       => false,
            'attr'        => array(
                'class' => 'col-lg-4',
            ),
            'query_builder' => function (EntityRepository $repository) use ($servidor_id) {
                $qb = $repository->createQueryBuilder('capa')
                    ->where('capa.servidor = :servidor')
                ;
                if ($servidor_id) {
                    $qb->setParameter('servidor', $servidor_id);
                } else {
                    $qb->setParameter('servidor', 1);
                }
                return $qb;
            }
        );

        $form->add('capa', 'choice', $formOptions);
    }

    public function preSetData(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        if (null === $data) {
            return;
        }

        $accessor    = PropertyAccess::createPropertyAccessor();

        $capa  = $accessor->getValue($data, 'capa');
        $servidor_id = ($capa) ? $capa->getServidor()->getId() : null;

        $this->addCapaForm($form, $servidor_id);
    }

    public function preSubmit(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        $servidor_id = array_key_exists('servidor', $data) ? $data['servidor'] : null;

        $this->addCapaForm($form, $servidor_id);
    }
}
