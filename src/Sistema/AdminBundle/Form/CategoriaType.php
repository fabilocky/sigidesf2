<?php

namespace Sistema\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * CategoriaType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class CategoriaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('expandida')            
            ->add('pestania', 'select2', array(
                'class' => 'Sistema\AdminBundle\Entity\Pestania',
                'url'   => 'Pestania_autocomplete_pestania',
                'configs' => array(
                    'multiple' => true,//required true or false
                    'width'    => 'off',
                ),
                'attr' => array(
                    'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                )
            ))
            ->add('capas', 'select2', array(
                'class' => 'Sistema\AdminBundle\Entity\Capa',
                'url'   => 'Pestania_autocomplete_capa',
                'configs' => array(
                    'multiple' => true,//required true or false
                    'width'    => 'off',
                ),
                'attr' => array(
                    'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                )
            ))
//            ->add('children', 'select2', array(
//                'class' => 'Sistema\AdminBundle\Entity\Categoria',
//                'url'   => 'Categoria_autocomplete_children',
//                'label' => 'Categorias Hijos',
//                'configs' => array(
//                    'multiple' => true,//required true or false
//                    'width'    => 'off',
//                ),
//                'attr' => array(
//                    'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
//                )
//            ))
//            ->add('parent', 'select2', array(
//                'class' => 'Sistema\AdminBundle\Entity\Categoria',
//                'url'   => 'Categoria_autocomplete_parent',
//                'label' => 'Categoria Padre',
//                'configs' => array(
//                    'multiple' => false,//required true or false
//                    'width'    => 'off',
//                ),
//                'attr' => array(
//                    'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
//                )
//            ))            
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\AdminBundle\Entity\Categoria'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_adminbundle_categoria';
    }
}
