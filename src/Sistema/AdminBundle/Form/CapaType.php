<?php

namespace Sistema\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * CapaType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class CapaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('servidor', 'select2', array(
                'class' => 'Sistema\AdminBundle\Entity\Servidor',
                'url'   => 'Capa_autocomplete_servidor',
                'configs' => array(
                    'multiple' => false,//required true or false
                    'width'    => 'off',
                ),
                'attr' => array(
                    'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                )
            ))            
            ->add('capa', 'choice', array("label" => "Capa", 'required' => false, 'placeholder' => false))
            ->add('titulo')
            ->add('base', null, array("required" => false))
            ->add('activo', null, array("required" => false))
            ->add('inicio', null, array("required" => false))
            ->add('metadatos')
            ->add('categoria', 'select2', array(
                'class' => 'Sistema\AdminBundle\Entity\Categoria',
                'url'   => 'Capa_autocomplete_categoria',
                'configs' => array(
                    'multiple' => true,//required true or false
                    'width'    => 'off',
                ),
                'attr' => array(
                    'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                )
            ))            
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\AdminBundle\Entity\Capa'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_adminbundle_capa';
    }
}
