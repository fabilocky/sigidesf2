<?php

namespace Sistema\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class VinculoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('capas', 'select2', array(
                'label' => "Capa",
                'class' => 'Sistema\AdminBundle\Entity\Capa',
                'url'   => 'Servidor_autocomplete_capa',
                'configs' => array(
                    'multiple' => false,//required true or false
                    'width'    => 'off',
                ),
                'attr' => array(
                    'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                )
            ))
            ->add('atributo', null, array(
                'label' => "Atributo de la capa con clave unica",
                )
                    )            
            ;        
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            //'data_class' => 'AppBundle\Entity\Tag',
            'allow_extra_fields' => true,
            'validation_groups' => false,
            'cascade_validation' => false,
        ));
    }

    public function getName()
    {
        return 'vinculo';
    }
}