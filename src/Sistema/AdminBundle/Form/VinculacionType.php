<?php

namespace Sistema\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Count;

/**
 * VinculacionType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class VinculacionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
//            ->add('features', 'entity', array(
//                    'label' => false,                    
//                    'required' => false,                              
//                    'by_reference'  => false,
//                    'multiple' => true,
//                    'class' => 'SistemaAdminBundle:Feature'
//                ))
            ->add('archivos', 'collection', array(
                    'label' => false,
                    'type' => new ArchivoType(),
                    'required' => false,
                    'allow_add'     => true,
                    'allow_delete' => true,                
                    'by_reference'  => false,                    
                    'constraints' => new Count(
                            array('min' => 1, 'minMessage' => 'Debes cargar al menos 1 archivo')
                    ),
                ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\AdminBundle\Entity\Vinculacion'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_adminbundle_vinculacion';
    }
}
