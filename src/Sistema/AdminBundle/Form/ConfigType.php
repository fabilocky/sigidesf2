<?php

namespace Sistema\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * ConfigType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class ConfigType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('zoom', null, array(
                'label' => "Zoom del Mapa"
            ))         
            ->add('centergoogle', null, array(
                'label' => "Centro del Mapa"
            ))
            ->add('titulo')
            ->add('imageFile', 'vich_image', array(
            'required'      => false,
            'allow_delete'  => true, // not mandatory, default is true
            'download_link' => true, // not mandatory, default is true
            'label' => "Logo"
            ))
            ->add('urlgeoserver', null, array(
                'label' => "URL Geoserver"
            ))
            ->add('urlwfs', null, array(
                'label' => "URL WFS"
            ))                      
            ->add('workspacegs', null, array(
                'label' => "Espacio de Trabajo"
            ))
            ->add('datastoregs', null, array(
                'label' => "Almacen de Datos"
            ))
            ->add('proyecciongs', null, array(
                'label' => "Proyeccion"
            ))
            ->add('usergs', null, array(
                'label' => "Usuario"
            ))
            ->add('passgs', null, array(
                'label' => "Password"
            ))
            ->add('userpgsql', null, array(
                'label' => "Usuario"
            ))
            ->add('passpgsql', null, array(
                'label' => "Contraseña"
            ))
            ->add('hostpgsql', null, array(
                'label' => "Host"
            ))
            ->add('bdpgsql', null, array(
                'label' => "Nombre de la Base de Datos"
            ))
            ->add('bdmetadatospgsql', null, array(
                'label' => "Nombre de la Base de Datos de Geonetwork"
            ))
            ->add('urlgeonetwork', null, array(
                'label' => "URL de Geonetwork"
            ))
            ->add('mail')
            ->add('colorbannerp', 'text', array(
                'attr' => array(
                    "class" => "form-control"
                )
            ))
            ->add('colorbanners', 'text', array(
                'attr' => array(
                    "class" => "form-control"
                )
            ))
            ->add('vinculaciones', 'collection', array(
                'label' => false,
                'type' => new VinculoType(),
                'prototype' => true,
                'allow_add' => true,
                'allow_delete' => true,
                'options'   => array(
                    'required'  => false,
                    //'attr'      => array('class' => 'email-box'),
                )
                )
                    )
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\AdminBundle\Entity\Config'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_adminbundle_config';
    }
}
