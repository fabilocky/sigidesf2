<?php
namespace Sistema\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AuditoriaType
 *
 * @author fabian
 */
class AuditoriaType extends AbstractType{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('informe', 'textarea', array(
                'label' => 'Informe',
                'label_attr' => array(
                    'class' => 'form-label col-lg-3',
                ),
                'attr'  => array(
                    'autofocus' => 'autofocus',
                    'class' => 'form-control'
                )
            ))
            ->add('resultado', 'choice', array(
                "choices" => array(
                    "Aprobado" => "aprobado",
                    "Rechazado" => "rechazado",
                ),
                'label' => 'Resultado',
                'label_attr' => array(
                    'class' => 'form-label col-lg-3',
                ),
                'attr'  => array(
                    'autofocus' => 'autofocus',
                    'class' => 'form-control'
                )
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(            
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_adminbundle_auditoria';
    }
}
