<?php

namespace Sistema\VisualizadorBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\VisualizadorBundle\SistemaVisualizadorBundle;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller {

    /**
     * @Route("/", name="home")
     * @Template()
     */
    public function toolsAction(Request $request) {
        if ($request->getSession()->get('mapa')){
            $idmapa = $request->getSession()->get('mapa');
        }

        $em = $this->getDoctrine()->getManager();
        if (isset($idmapa)){
            $proyecto = $em->getRepository('SistemaUserBundle:Mapa')->find($idmapa);
        } else {
            $proyecto = null;
        }
        //ladybug_dump($proyecto);die();
        if (true === $this->container->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            $capas = $em->getRepository('SistemaAdminBundle:Capa')->findAll();
            $nombre = "Superadmin";
            $mapasProyectos = null;
        } elseif (true === $this->container->get('security.context')->isGranted('ROLE_USER')) {
            $capas = $em->getRepository('SistemaAdminBundle:Capa')->findAll();
            $sc = $this->container->get('security.context');
            $usuario = $sc->getToken()->getUser();
            //obtiene los proyectos de usuario y compartidos
            $grupos = $usuario->getGrupos();
            $mapasu = array(); // para proyectos de usuario
            $mapasg = array(); // para proyectos compartidos
            foreach ($grupos as $grupo){
                $mapas = $grupo->getMapas();
                foreach ($mapas as $mapa){
                    $mapasg[]=$mapa;
                }
            }
            $mapas = $usuario->getMapas();
            foreach ($mapas as $mapa){
                    $mapasu[]=$mapa;
            }
            $mapasProyectos["usuario"]=$mapasu;
            $mapasProyectos["grupo"]=$mapasg;
            if ($sc->getToken()->getUser() == "anon."){
                $nombre = "anonimo";
            } else {
                $nombre = $sc->getToken()->getUser()->getUsername();
            }

        } else {
            $capas = $em->getRepository('SistemaAdminBundle:Capa')->findAll();
            $nombre = "Anonimo";
            $mapasProyectos = null;
        }

        $config = $em->getRepository('SistemaAdminBundle:Config')->findAll();
        $pestanias = $em->getRepository('SistemaAdminBundle:Pestania')->findAll();
        $proveedores = $em->getRepository('SistemaAdminBundle:Proveedor')->findBy(array(), array('orden' => 'ASC'));
        $proveedordefault = $em->getRepository('SistemaAdminBundle:Proveedor')->findOneByDefaultmap("1");


        foreach ($pestanias as $pestania){
            $datas = array();
            $i = 0;
        if ($pestania->getArbol()){
            $arbol = $pestania->getArbol()->getArbol();
            $categorias = $this->read_tree_recursively(json_decode($arbol));
            $categoriasp = $pestania->getCategorias();
            foreach ($categorias as $categoria) {
            $datas[$i]["id"] = $categoria["id"];
            $datas[$i]["parent"] = $categoria["parent"];
            $datas[$i]["name"] = $categoria["name"];
            $objcategoria = $em->getRepository('SistemaAdminBundle:Categoria')->find($categoria["id"]);
            if ($objcategoria){

            if ($objcategoria->getExpandida() == "1"){
                $datas[$i]["expanded"] = 'true';
            } else {
                $datas[$i]["expanded"] = 'false';
            }
            foreach ($objcategoria->getCapas() as $capacateg) {
                if (in_array($capacateg, $capas)) {
                    $datas[$i]["capas"][] = $capacateg;
                }
            }
            $i++;
            } else {
                unset($datas[$i]);
            }
        }

        } else {
            $categorias = $pestania->getCategorias();
            foreach ($categorias as $categoria) {
            $datas[$i]["id"] = $categoria->getId();
            $datas[$i]["parent"] = 0;
            $datas[$i]["name"] = $categoria->getNombre();
            if ($categoria->getExpandida() == "1"){
                $datas[$i]["expanded"] = 'true';
            } else {
                $datas[$i]["expanded"] = 'false';
            }
            foreach ($categoria->getCapas() as $capacateg) {
                if (in_array($capacateg, $capas)) {
                    $datas[$i]["capas"][] = $capacateg;
                }
            }
            $i++;
        }

        }
        $arboles[$pestania->getNombre()] = $this->BuildTree($datas);
        //unset($datas);
        }

        //ladybug_dump($mapa);die();
        return array(
            'capas' => $capas,
            'pestanias' => $pestanias,
            'proveedores' => $proveedores,
            'proveedordefault' => $proveedordefault,
            'config' => $config[0],
            'categorias' => $categorias,
            //'tree' => $this->BuildTree($datas),
            'arboles' => $arboles,
            'bases' => $this->BuildBases($capas),
            'user' => $nombre,
            'mapa' => $proyecto,
            'mapasProyectos' => $mapasProyectos,
            'vinculaciones' => $config[0]->getVinculaciones(),
        );
    }

    function BuildTree($datas, $parent = 0, $limit = 0) {

        if ($limit > 1000)
            return ''; // Make sure not to have an endless recursion
        $tree = '';
        for ($i = 0, $ni = count($datas); $i < $ni; $i++) {

            if ($datas[$i]['parent'] == $parent) {
                $tree .= '{text: "' . $datas[$i]['name'] . '", expanded:  ' . $datas[$i]['expanded'] . ', children: [';
                if (array_key_exists("capas", $datas[$i])){
                foreach ($datas[$i]["capas"] as $capa) {
                    if ($capa->getBase() == false && $capa->getActivo() == true) {
                        $tree .= '{nodeType: "gx_layer", layer: "' . $capa->getTitulo() . '", text: "' . $capa->getTitulo() . '" },';
                    }
                }
                }
                //$tree .= $datas[$i]['name'];
                $tree .= $this->BuildTree($datas, $datas[$i]['id'], $limit++);
                $tree .= ']},';
            }
        }
        //$tree .= '</ul>';
        return $tree;
    }

    function BuildTree2($datas, $parent = 0, $limit = 0) {
        if ($limit > 1000)
            return ''; // Make sure not to have an endless recursion
        $tree = '';
        foreach ($datas as $data) {
            if (is_array($data) && $data['parent'] == $parent) {
                $tree .= '{text: "' . $data['name'] . '", children: [';

                if (array_key_exists("capas", $data)){

                foreach ($data["capas"] as $capa) {
                    if ($capa->getBase() == false && $capa->getActivo() == true) {
                        $tree .= '{nodeType: "gx_layer", layer: "' . $capa->getTitulo() . '", text: "' . $capa->getTitulo() . '" },';
                    }
                }
                }
                $tree .= $this->BuildTree2($data, $data['id'], $limit++);
                $tree .= ']},';
            }
        }
        return $tree;
    }

    function BuildBases($capas) {
        $bases = '';
        foreach ($capas as $key => $capa) {
            if ($capa->getBase() == true && $capa->getActivo() == true) {
                $bases .= '{nodeType: "gx_layer", layer: "' . $capa->getTitulo() . '", text: "' . $capa->getTitulo() . '" },';
            }
        }
        return $bases;
    }

    function Tree2() {
        $em = $this->getDoctrine()->getManager();
        $configurar = $em->getRepository('SistemaAdminBundle:Config')->findAll();
        $configurar = $configurar[0];
        $json = $configurar->getArbol();
        $array = json_decode($json["tree"], true);
        foreach ($array as $entity) {
            if ($this->inArrayR($entity->getNombre(), $array, true)) {

            } else {
                $array[] = array("name" => $entity->getNombre(), "is_open" => false, "text" => $entity->getNombre());
            }
        }
        $json = json_encode($array);
    }

    /**
     * @Route("/selectproyect", name="selectproyect")
     * @Template()
     */
    public function selectproyectAction() {
        $em = $this->getDoctrine()->getManager();
        if (true === $this->container->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            $proyectos = $em->getRepository('SistemaAdminBundle:Proyecto')->findAll();
            $nombre = "Superadmin";
        } else {
            $proyectos = array();
            $sc = $this->container->get('security.context');
            $user = $sc->getToken()->getUser();
            $nombre = $user->getUsername();
            if ($user->getReglaproyecto()) {
                $proyectosreglas = $user->getReglaproyecto()->getProyecto();
                foreach ($proyectosreglas as $p) {
                        array_push($proyectos, $p);
                    }
                }
            if ($user->getProyecto()) {
                $proyectosdirectos = $user->getProyecto();
                foreach ($proyectosdirectos as $p) {
                        array_push($proyectos, $p);
                }
            }
            }

        return array(
            'proyectos' => $proyectos,
            'user' => $nombre,
        );
    }

    /**
     * Finds and displays a Presupuesto entity.
     *
     * @Route("/procesar/{id}", name="procesar_proyecto")
     * @Method("GET")
     * @Template()
     */
    public function ProcesarproyectoAction($id, Request $request)
    {
        $session = $request->getSession();

    // guarda un atributo para reutilizarlo durante una
    // petición posterior del usuario
        $session->set('proy', $id);

        return $this->redirect($this->generateUrl('home'));
    }

    /**
     * Finds and displays a Presupuesto entity.
     *
     * @Route("/cerrarproyecto", name="cerrar_proyecto")
     * @Method("GET")
     * @Template()
     */
    public function CerrarproyectoAction(Request $request)
    {
        $session = $request->getSession();
        //ladybug_dump($session);die();
    // guarda un atributo para reutilizarlo durante una
    // petición posterior del usuario
        $session->remove('mapa');
        $session->invalidate();

        return $this->redirect($this->generateUrl('home'));
    }

    function read_tree_recursively($items, $parent_id = 0, $result = array(), $level = 0) {
        foreach ($items as $child) {
            $result[$child->id] = array(
                'id' => $child->id,
                'parent_id' => $parent_id,
                'level' => $level,
                'parent' => $parent_id,
                'name' => $child->name,
            );

            if (!empty($child->children)) {
                $result = $this->read_tree_recursively($child->children, $child->id, $result, $level + 1);
            }
        }
        return $result;
    }

    /**
     * Autocomplete a Capa entity.
     *
     * @Route("/consultar/metadatos", name="consultar_metadatos")
     */
    public function getUuidCapa() {
        $capa = $this->getRequest()->get('capa');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('SistemaAdminBundle:Capa')->findOneByTitulo($capa);
        $response = new JsonResponse();
        $response->setData($entity->getMetadatos());
        return $response;
    }
    
    /**
     * Lists all Mapa entities.
     *
     * @Route("/vermetadatos/{idcapa}", name="ver_metadatos_visor")
     * @Method("get")
     * @Template()
     */
    public function vermetadatosAction(Request $request)
    {
        $idCapa = $request->get('idcapa');                
        $em = $this->getDoctrine()->getManager();
        $capa = $em->getRepository("SistemaUserBundle:UsuarioCapa")->find($idCapa);
        $config = $em->getRepository("SistemaAdminBundle:Config")->findAll();
        $metadatos = $capa->getMetadato();       
        
        $library = simplexml_load_string(file_get_contents("files/metadatos/".$capa->getMetadato()));
        $arrayMetadatos["titulo"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->citation->children('gmd', true)->CI_Citation->children('gmd', true)->title->children('gco', true)->CharacterString;
        $arrayMetadatos["fecha"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->citation->children('gmd', true)->CI_Citation->children('gmd', true)->date->children('gmd', true)->CI_Date->children('gmd', true)->date->children('gco', true)->DateTime;
        $arrayMetadatos["tipofecha"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->citation->children('gmd', true)->CI_Citation->children('gmd', true)->date->children('gmd', true)->CI_Date->children('gmd', true)->dateType->children('gmd', true)->CI_DateTypeCode->attributes();
        $arrayMetadatos["Resumencd"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->abstract->children('gco', true)->CharacterString;
        $arrayMetadatos["pcnombre"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->pointOfContact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->individualName->children('gco', true)->CharacterString;
        $arrayMetadatos["pcorganizacion"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->pointOfContact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->organisationName->children('gco', true)->CharacterString;
        $arrayMetadatos["pccargo"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->pointOfContact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->positionName->children('gco', true)->CharacterString;
        $arrayMetadatos["pcrol"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->pointOfContact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->role->children('gmd', true)->CI_RoleCode->attributes();
        $arrayMetadatos["pctelefono"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->pointOfContact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->phone->children('gmd', true)->CI_Telephone->children('gmd', true)->voice->children('gco', true)->CharacterString;
        $arrayMetadatos["pcfax"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->pointOfContact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->phone->children('gmd', true)->CI_Telephone->children('gmd', true)->facsimile->children('gco', true)->CharacterString;
        $arrayMetadatos["pcdireccion"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->pointOfContact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->address->children('gmd', true)->CI_Address->children('gmd', true)->deliveryPoint->children('gco', true)->CharacterString;
        $arrayMetadatos["pcciudad"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->pointOfContact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->address->children('gmd', true)->CI_Address->children('gmd', true)->city->children('gco', true)->CharacterString;
        $arrayMetadatos["pcprovincia"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->pointOfContact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->address->children('gmd', true)->CI_Address->children('gmd', true)->administrativeArea->children('gco', true)->CharacterString;
        $arrayMetadatos["pccodpostal"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->pointOfContact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->address->children('gmd', true)->CI_Address->children('gmd', true)->postalCode->children('gco', true)->CharacterString;
        $arrayMetadatos["pcpais"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->pointOfContact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->address->children('gmd', true)->CI_Address->children('gmd', true)->country->children('gco', true)->CharacterString;
        $arrayMetadatos["pcemail"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->pointOfContact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->address->children('gmd', true)->CI_Address->children('gmd', true)->electronicMailAddress->children('gco', true)->CharacterString;
        $arrayMetadatos["palabrasclave"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->descriptiveKeywords->children('gmd', true)->MD_Keywords->children('gmd', true)->keyword->children('gco', true)->CharacterString;
        $arrayMetadatos["tipopclave"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->descriptiveKeywords->children('gmd', true)->MD_Keywords->children('gmd', true)->type->children('gmd', true)->MD_KeywordTypeCode->attributes();
        $arrayMetadatos["tiporepesp"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->spatialRepresentationType->children('gmd', true)->MD_SpatialRepresentationTypeCode->attributes();
        $arrayMetadatos["denominador"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->spatialResolution->children('gmd', true)->MD_Resolution->children('gmd', true)->equivalentScale->children('gmd', true)->MD_RepresentativeFraction->children('gmd', true)->denominator->children('gco', true)->Integer;
        $arrayMetadatos["idiomacd"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->language->children('gmd', true)->LanguageCode->attributes()['codeListValue'];                
        $arrayMetadatos["caracterescd"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->characterSet->children('gmd', true)->MD_CharacterSetCode->attributes();
        $arrayMetadatos["temacd"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->topicCategory->children('gmd', true)->MD_TopicCategoryCode;
        $arrayMetadatos["desde"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->extent[0]->children('gmd', true)->EX_Extent->children('gmd', true)->temporalElement->children('gmd', true)->EX_TemporalExtent->children('gmd', true)->extent->children('gml', true)->TimePeriod->children('gml', true)->beginPosition;
        $arrayMetadatos["hasta"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->extent[0]->children('gmd', true)->EX_Extent->children('gmd', true)->temporalElement->children('gmd', true)->EX_TemporalExtent->children('gmd', true)->extent->children('gml', true)->TimePeriod->children('gml', true)->endPosition;
        $arrayMetadatos["loeste"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->extent[1]->children('gmd', true)->EX_Extent->children('gmd', true)->geographicElement->children('gmd', true)->EX_GeographicBoundingBox->children('gmd', true)->westBoundLongitude->children('gco', true)->Decimal;
        $arrayMetadatos["leste"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->extent[1]->children('gmd', true)->EX_Extent->children('gmd', true)->geographicElement->children('gmd', true)->EX_GeographicBoundingBox->children('gmd', true)->eastBoundLongitude->children('gco', true)->Decimal;
        $arrayMetadatos["lsur"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->extent[1]->children('gmd', true)->EX_Extent->children('gmd', true)->geographicElement->children('gmd', true)->EX_GeographicBoundingBox->children('gmd', true)->southBoundLatitude->children('gco', true)->Decimal;
        $arrayMetadatos["lnorte"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->extent[1]->children('gmd', true)->EX_Extent->children('gmd', true)->geographicElement->children('gmd', true)->EX_GeographicBoundingBox->children('gmd', true)->northBoundLatitude->children('gco', true)->Decimal;
        $arrayMetadatos["nomgeog"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->extent[1]->children('gmd', true)->EX_Extent->children('gmd', true)->description->children('gco', true)->CharacterString;
        $arrayMetadatos["infocomp"] = $library->children('gmd', true)->identificationInfo->children('gmd', true)->MD_DataIdentification->children('gmd', true)->supplementalInformation->children('gco', true)->CharacterString;
        $arrayMetadatos["njerarquico"] = $library->children('gmd', true)->dataQualityInfo->children('gmd', true)->DQ_DataQuality->children('gmd', true)->scope->children('gmd', true)->DQ_Scope->children('gmd', true)->level->children('gmd', true)->MD_ScopeCode->attributes();
        $arrayMetadatos["linaje"] = $library->children('gmd', true)->dataQualityInfo->children('gmd', true)->DQ_DataQuality->children('gmd', true)->lineage->children('gmd', true)->LI_Lineage->children('gmd', true)->statement->children('gco', true)->CharacterString;
        $arrayMetadatos["codigoref"] = $library->children('gmd', true)->referenceSystemInfo->children('gmd', true)->MD_ReferenceSystem->children('gmd', true)->referenceSystemIdentifier->children('gmd', true)->RS_Identifier->children('gmd', true)->code->children('gco', true)->CharacterString;
        $arrayMetadatos["distnom"] = $library->children('gmd', true)->distributionInfo->children('gmd', true)->MD_Distribution->children('gmd', true)->distributionFormat->children('gmd', true)->MD_Format->children('gmd', true)->name->children('gco', true)->CharacterString;
        $arrayMetadatos["distversion"] =$library->children('gmd', true)->distributionInfo->children('gmd', true)->MD_Distribution->children('gmd', true)->distributionFormat->children('gmd', true)->MD_Format->children('gmd', true)->version->children('gco', true)->CharacterString;
        $arrayMetadatos["url"] = $library->children('gmd', true)->distributionInfo->children('gmd', true)->MD_Distribution->children('gmd', true)->transferOptions->children('gmd', true)->MD_DigitalTransferOptions->children('gmd', true)->onLine->children('gmd', true)->CI_OnlineResource->children('gmd', true)->linkage->children('gmd', true)->URL;
        $arrayMetadatos["descripcion"] = $library->children('gmd', true)->distributionInfo->children('gmd', true)->MD_Distribution->children('gmd', true)->transferOptions->children('gmd', true)->MD_DigitalTransferOptions->children('gmd', true)->onLine->children('gmd', true)->CI_OnlineResource->children('gmd', true)->description->children('gco', true)->CharacterString;
        $arrayMetadatos["identificador"] = $library->children('gmd', true)->fileIdentifier->children('gco', true)->CharacterString;
        $arrayMetadatos["idiomamd"] = $library->children('gmd', true)->language->children('gmd', true)->LanguageCode->attributes()['codeListValue'];
        $arrayMetadatos["normamd"] = $library->children('gmd', true)->metadataStandardName->children('gco', true)->CharacterString;
        $arrayMetadatos["versionmd"] = $library->children('gmd', true)->metadataStandardVersion->children('gco', true)->CharacterString;
        $arrayMetadatos["caracteresmd"] = $library->children('gmd', true)->characterSet->children('gmd', true)->MD_CharacterSetCode->attributes();
        $arrayMetadatos["fechamd"] = $library->children('gmd', true)->dateStamp->children('gco', true)->DateTime;
        $arrayMetadatos["nombremd"] =$library->children('gmd', true)->contact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->individualName->children('gco', true)->CharacterString;
        $arrayMetadatos["organizacionmd"] = $library->children('gmd', true)->contact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->organisationName->children('gco', true)->CharacterString;
        $arrayMetadatos["cargomd"] = $library->children('gmd', true)->contact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->positionName->children('gco', true)->CharacterString;
        $arrayMetadatos["rolmd"] = $library->children('gmd', true)->contact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->role->children('gmd', true)->CI_RoleCode->attributes();
        $arrayMetadatos["telefonomd"] = $library->children('gmd', true)->contact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->phone->children('gmd', true)->CI_Telephone->children('gmd', true)->voice->children('gco', true)->CharacterString;
        $arrayMetadatos["faxmd"] = $library->children('gmd', true)->contact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->phone->children('gmd', true)->CI_Telephone->children('gmd', true)->facsimile->children('gco', true)->CharacterString;
        $arrayMetadatos["direccionmd"] = $library->children('gmd', true)->contact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->address->children('gmd', true)->CI_Address->children('gmd', true)->deliveryPoint->children('gco', true)->CharacterString;
        $arrayMetadatos["ciudadmd"] = $library->children('gmd', true)->contact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->address->children('gmd', true)->CI_Address->children('gmd', true)->city->children('gco', true)->CharacterString;
        $arrayMetadatos["provinciamd"] = $library->children('gmd', true)->contact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->address->children('gmd', true)->CI_Address->children('gmd', true)->administrativeArea->children('gco', true)->CharacterString;
        $arrayMetadatos["codpostalmd"] = $library->children('gmd', true)->contact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->address->children('gmd', true)->CI_Address->children('gmd', true)->postalCode->children('gco', true)->CharacterString;
        $arrayMetadatos["paismd"] = $library->children('gmd', true)->contact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->address->children('gmd', true)->CI_Address->children('gmd', true)->country->children('gco', true)->CharacterString;
        $arrayMetadatos["emailmd"] = $library->children('gmd', true)->contact->children('gmd', true)->CI_ResponsibleParty->children('gmd', true)->contactInfo->children('gmd', true)->CI_Contact->children('gmd', true)->address->children('gmd', true)->CI_Address->children('gmd', true)->electronicMailAddress->children('gco', true)->CharacterString;                      
                
        $form = $this->createForm(new \Sistema\AdminBundle\Form\AuditoriaType(), null, array(
            'action' => $this->generateUrl('admin_auditoria_validar', array('idCapa' => $idCapa)),
            'method' => 'GET',
        ))             
            ->add('save', 'submit', array(
                'label' => 'Enviar informe',
                'attr'  => array(                    
                    'class' => 'btn btn-success'
                )
                ));            
       
        
        return array(
            'capa'     => $capa,            
            'metadatos' => $arrayMetadatos,
            'form' => $form->createView(),
            'config' => $config[0]
        );
    }
}
