<?php

namespace Sistema\VisualizadorBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sistema\VisualizadorBundle\SistemaVisualizadorBundle;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Vinculacion controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/vinculacion")
 */
class VinculacionController extends Controller {

    /**
     * @Route("/", name="mapavinculacion")
     * @Template()
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();
        $config = $em->getRepository('SistemaAdminBundle:Config')->findAll();
        $entity = new \Sistema\AdminBundle\Entity\Vinculacion();
        $form = $this->createForm(new \Sistema\AdminBundle\Form\VinculacionType(), $entity, array(
            'action' => $this->generateUrl('admin_vinculacion_create'),
            'method' => 'POST',
        ));

        $form
            ->add(
                'save', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label'              => 'Guardar',
                'attr'               => array('class' => 'btn btn-success col-lg-3')
                )
            )
            
        ;
        return array(
            'form' => $form->createView(),
            'capas' => $config[0]->getCapas(),
            'config' => $config[0]
        );
    }
    
    /**
     * @Route("/vincular", name="vincular")
     * @Template()
     */
    public function vincularAction() {
        $features = $_GET["features"];
        $entity = new \Sistema\AdminBundle\Entity\Vinculacion();
        $form = $this->createForm(new \Sistema\AdminBundle\Form\VinculacionType(), $entity, array(
            'action' => $this->generateUrl('admin_vinculacion_create'),
            'method' => 'POST',
        ));

        $form
            ->add(
                'save', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label'              => 'Guardar',
                'attr'               => array('class' => 'btn btn-success col-lg-3')
                )
            )
            
        ;
        return array(
            'form' => $form->createView(),
            'features' => $features
        );
    }
    
    /**
     * @Route("/consultar", name="consultar")
     * @Template()
     */
    public function consultarAction() {
        $archivos = array();
        //$features = $_GET["features"];
        $features = explode(",", $_GET["features"]);
        //var_dump($features);
        $em = $this->getDoctrine()->getManager();
        foreach ($features as $feature){
            $entities = $em->getRepository("SistemaAdminBundle:Feature")->findBy(
            array('fid' => $feature)
            );
            foreach ($entities as $entity){
                $archis=$entity->getVinculacion()->getArchivos();
                foreach ($archis as $archivo){
                    $archivos[] = $archivo;
                }
            }
        }        
        return array(            
            'archivos' => $archivos
        );
    }
    
    /**
     * @Route("/consulta", name="consultavinculacion")
     * @Template()
     */
    public function consultaAction() {
        $em = $this->getDoctrine()->getManager();
        $config = $em->getRepository('SistemaAdminBundle:Config')->findAll();
        return array(
            'capas' => $config[0]->getCapas(),
            'config' => $config[0]
        );
    }

    /**
     * Finds and displays a precio Tipo Producto entity.
     *
     * @Route("/buscarjson/buscar", name="buscar_json")
     */
    public function buscarJson() {        
        $em = $this->getDoctrine()->getManager();        
        $array = array();
        $arrayfiles = array();        
        $arrayfeatures = '{
              "type": "FeatureCollection", 
              "features": [
                {"geometry": {
                    "type": "GeometryCollection", 
                    "geometries": [';
        $search = $this->getRequest()->get('search');               
        $search = explode(" ", $search);
        $search = $search[0];
        $activas = $this->getRequest()->get('activas');
        $activas = explode(",", $activas);        
        unset($activas[0]);
        $coma = 0;
            foreach ($activas as $activa){
            if ($activa != "Selection" && $activa != "Points"){
            $json = file_get_contents('http://'.$_SERVER['SERVER_NAME'].'/wfsAC?service=WFS&version=1.0.0&request=GetFeature&typeName='.$activa.'&maxFeatures=100000&outputFormat=application%2Fjson&srsName=EPSG:900913');
            $features = json_decode($json, true);            
            foreach ($features["features"] as $feature) {
                foreach ($feature["properties"] as $key => $value) {
                    if (strpos($value, $search) !== false) {
                        if ($coma != 0){
                            $arrayfeatures.=",";
                        }
                        $array[] = $feature["properties"];                        
                        $arrayfeatures = $arrayfeatures .json_encode($feature["geometry"]);
                        $coma++;
                    }
                }
            }
            
    }
            }
        $arrayfeatures = $arrayfeatures . ']
                }, 
                "type": "Feature", 
                "properties": {}}
              ]
           }'; 
        $html = '';       
        
        $i = 1;
        $ind = 14;
        if ($coma != 0){
            $response = json_encode(array($html,$arrayfeatures));
            return new JsonResponse($response);
        } else {
            return new JsonResponse("null");
        }                        
    }
    
    /**
     * Finds and displays a precio Tipo Producto entity.
     *
     * @Route("/buscarjson/buscarfid", name="buscarfid_json")
     */
    public function buscarfidJson() {        
        $em = $this->getDoctrine()->getManager();        
        $array = array();
        $arrayfiles = array();        
        $arrayfeatures = '{
              "type": "FeatureCollection", 
              "features": [
                {"geometry": {
                    "type": "GeometryCollection", 
                    "geometries": [';
        $search = $this->getRequest()->get('search');               
        $search = explode(" ", $search);
        $search = $search[0]; 
        $capa = explode(".", $search);
        $capa = $capa[0]; 
        $coma = 0;
            $json = file_get_contents('http://'.$_SERVER['SERVER_NAME'].'/wfsAC?service=WFS&version=1.0.0&request=GetFeature&typeName='.$capa.'&maxFeatures=100000&outputFormat=application%2Fjson&srsName=EPSG:900913&featureID='.$search);
            $features = json_decode($json, true);            
            foreach ($features["features"] as $feature) {               
                
                    if ($feature["id"] == $search) {
                        if ($coma != 0){
                            $arrayfeatures.=",";
                        }
                        $array[] = $feature["properties"];                        
                        $arrayfeatures = $arrayfeatures .json_encode($feature["geometry"]);
                        $coma++;
                    }
                
            }
            
        
        $arrayfeatures = $arrayfeatures . ']
                }, 
                "type": "Feature", 
                "properties": {}}
              ]
           }'; 
        $html = '';       
        
        $i = 1;
        $ind = 14;
        if ($coma != 0){
            $response = json_encode(array($html,$arrayfeatures));
            return new JsonResponse($response);
        } else {
            return new JsonResponse("null");
        }                        
    }
    
    /**
     * Finds and displays a precio Tipo Producto entity.
     *
     * @Route("/ajaxform/form", name="ajax_form")
     */
    public function ajaxForm() {
        $fid = $this->getRequest()->get('fid');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository("SistemaAdminBundle:Vinculacion")->findOneBy(
            array('fid' => $fid)
            );
        if ($entity){
            $form = $this->createForm(new \Sistema\AdminBundle\Form\VinculacionType(), $entity, array(
            'action' => $this->generateUrl('admin_vinculacion_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));
        } else {
            $entity = new \Sistema\AdminBundle\Entity\Vinculacion();
            $form = $this->createForm(new \Sistema\AdminBundle\Form\VinculacionType(), $entity, array(
            'action' => $this->generateUrl('admin_vinculacion_create'),
            'method' => 'POST',
        ));
        }        

        $form
            ->add(
                'save', 'submit', array(
                'translation_domain' => 'MWSimpleCrudGeneratorBundle',
                'label'              => 'Guardar',
                'attr'               => array('class' => 'btn btn-success col-lg-3')
                )
            )
            
        ;
        
        $renderedTmpl = $this->container->get('templating')->render('SistemaVisualizadorBundle:Vinculacion:formAjax.html.twig', array(
        'form'=> $form->createView(),
                ));

        $response = new JsonResponse();
        $response->setData(array("form_data" => $renderedTmpl));
        return $response;
    }
    
    /**
     * Finds and displays a precio Tipo Producto entity.
     *
     * @Route("/ajaxshow/show", name="ajax_show")
     */
    public function ajaxShow() {
        $fid = $this->getRequest()->get('fid');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository("SistemaAdminBundle:Vinculacion")->findOneBy(
            array('fid' => $fid)
            );
        if ($entity){
            $renderedTmpl = $this->container->get('templating')->render('SistemaVisualizadorBundle:Vinculacion:ajaxShow.html.twig', array(
        'entity'=> $entity,
                ));
            $response = new JsonResponse();
        $response->setData(array("form_data" => $renderedTmpl));
        return $response;
        } else {
            $response = new JsonResponse();
        $response->setData(array("form_data" => ''));
        return $response;
        }        
    }
    
    /**
     * @Route("/postvinculacion", name="postvinculacion")
     * @Template()
     */
    public function postvinculacionAction() {
        
        return array();
    }
}
