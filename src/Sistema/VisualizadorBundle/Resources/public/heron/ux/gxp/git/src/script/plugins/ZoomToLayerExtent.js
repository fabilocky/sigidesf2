/**
 * Copyright (c) 2008-2011 The Open Planning Project
 * 
 * Published under the GPL license.
 * See https://github.com/opengeo/gxp/raw/master/license.txt for the full text
 * of the license.
 */

/**
 * @requires plugins/ZoomToExtent.js
 */

/** api: (define)
 *  module = gxp.plugins
 *  class = ZoomToLayerExtent
 */

/** api: (extends)
 *  plugins/ZoomToExtent.js
 */
Ext.namespace("gxp.plugins");

/** api: constructor
 *  .. class:: ZoomToLayerExtent(config)
 *
 *    Plugin for zooming to the extent of a non-vector layer
 */
gxp.plugins.ZoomToLayerExtent = Ext.extend(gxp.plugins.ZoomToExtent, {
    
    /** api: ptype = gxp_zoomtolayerextent */
    ptype: "gxp_zoomtolayerextent",
    
    /** api: config[menuText]
     *  ``String``
     *  Text for zoom menu item (i18n).
     */
    menuText: "Zoom to layer extent",

    /** api: config[tooltip]
     *  ``String``
     *  Text for zoom action tooltip (i18n).
     */
    tooltip: "Zoom to layer extent",
    
    /** private: property[iconCls]
     */
    iconCls: "gxp-icon-zoom-to",

    /** private: method[destroy]
     */
    destroy: function() {
        this.selectedRecord = null;
        gxp.plugins.ZoomToLayerExtent.superclass.destroy.apply(this, arguments);
    },

    /** api: method[extent]
     */
    extent: function() {
        var layer = this.selectedRecord.getLayer(),
            dataExtent;
        if (OpenLayers.Layer.Vector) {
            dataExtent = layer instanceof OpenLayers.Layer.Vector &&
                layer.getDataExtent();
        }
        //hola(layer);
        var x = new XMLHttpRequest();
x.open("GET", "http://adc/geoserver/IDEChaco/wms?&request=GetCapabilities", true);
x.onreadystatechange = function () {
  if (x.readyState == 4 && x.status == 200)
  {
    var doc = x.responseXML;
    var title = doc.getElementsByTagName("Layer")[0].getElementsByTagName("Layer");
    for (i = 0; i < title.length; i++) {
        if (layer.name == title[i].getElementsByTagName("Name")[0].innerHTML){
            console.log(layer.name);
            console.log(title[i].getElementsByTagName("Name")[0].innerHTML);
            var bbox = title[i].getElementsByTagName("BoundingBox");
            console.log(bbox[0].attributes.minx.value);
            console.log(layer.maxExtent);
            var bounds = new OpenLayers.Bounds(bbox[0].attributes.minx.value, bbox[0].attributes.miny.value, bbox[0].attributes.maxx.value, bbox[0].attributes.maxy.value);
            //layer.maxExtent = new OpenLayers.Bounds(bbox[0].attributes.minx.value, bbox[0].attributes.miny.value, bbox[0].attributes.maxx.value, bbox[0].attributes.maxy.value);
            var proj = new OpenLayers.Projection("EPSG:4326");
            var proj1 = new OpenLayers.Projection("EPSG:3857");

var bounds2 = bounds.transform(proj, proj1);
console.log(bounds2);
layer.maxExtent = bounds2;
        }
    
}
    
    // …
  }
};
x.send(null);

        
        return layer.maxExtent;
    },

    /** api: method[addActions]
     */
    addActions: function() {
        var actions = gxp.plugins.ZoomToLayerExtent.superclass.addActions.apply(this, arguments);
        actions[0].disable();

        this.target.on("layerselectionchange", function(record) {
            this.selectedRecord = record;
            actions[0].setDisabled(
                !record || !record.get('layer')
            );
        }, this);

        return actions;
    }
        
});

Ext.preg(gxp.plugins.ZoomToLayerExtent.prototype.ptype, gxp.plugins.ZoomToLayerExtent);

function hola(layer)
{
var readr = new Ext.data.XmlReader(
{ record: 'Layer'},
[
{name: 'name', mapping: 'Name'}
,{name: 'minx', mapping: 'LatLonBoundingBox > @minx'}
,{name: 'miny', mapping: 'LatLonBoundingBox > @miny'}
,{name: 'maxx', mapping: 'LatLonBoundingBox > @maxx'}
,{name: 'maxy', mapping: 'LatLonBoundingBox > @maxy'}
]);

var myStore = new Ext.data.Store({
url : "http://adc/geoserver/IDEChaco/wms?&request=GetCapabilities" ,
reader : readr
});
myStore.load({'layer' : layer, callback: function(r, options, success)
{
var rindex = this.find('name', layer.name, 0, true, true);
var rec = this.getAt(rindex);
console.log(new OpenLayers.Bounds(rec.data.minx, rec.data.miny, rec.data.maxx, rec.data.maxy))
layer.maxExtent = new OpenLayers.Bounds(rec.data.minx, rec.data.miny, rec.data.maxx, rec.data.maxy);
} });
};