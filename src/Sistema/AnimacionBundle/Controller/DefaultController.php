<?php

namespace Sistema\AnimacionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/animaciones")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }
    
    /**
     * @Route("/googleearth", name="googleearth")
     * @Template()
     */
    public function googleearthAction()
    {
        $em = $this->getDoctrine()->getManager();
        $config = $em->getRepository('SistemaAdminBundle:Config')->findAll();
        return array('config' => $config[0]);
    }
}
