/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** api: example[querybuilder]
 *  Edit and execute WFS Queries
 *  ----------------------------
 *  Use the GXP QueryPanel to build and execute WFS spatial and filter-queries.
 */

/** This config assumes the DefaultOptionsWorld.js to be included first!! */
//Heron.options.map.settings.zoom = 7;
//Heron.options.map.settings.center = '-57.700, -28.700',

Ext.namespace("Heron.examples");

Heron.examples.searchPanelConfig = {
    xtype: 'hr_multisearchcenterpanel',
    height: 600,
    hropts: [
        {
            searchPanel: {
                xtype: 'hr_searchbydrawpanel',
                name: __('Search by Drawing'),
                header: false
            },
            resultPanel: {
                xtype: 'hr_featuregridpanel',
                id: 'hr-featuregridpanel',
                header: false,
                autoConfig: true,
                autoConfigMaxSniff: 100,
                exportFormats: ['XLS', 'GMLv2', 'GeoJSON', 'WellKnownText', 'Shapefile'],
                gridCellRenderers: Heron.options.gridCellRenderers,
                hropts: {
                    zoomOnRowDoubleClick: true,
                    zoomOnFeatureSelect: false,
                    zoomLevelPointSelect: 8,
                    zoomToDataExtent: false
                }
            }
        },
        {
            searchPanel: {
                xtype: 'hr_gxpquerypanel',
                name: __('Build your own searches'),
                description: 'This search uses both search within Map extent and/or your own attribute criteria',
                header: false,
                border: false,
                caseInsensitiveMatch: true,
                autoWildCardAttach: true
            },
            resultPanel: {
                xtype: 'hr_featuregridpanel',
                id: 'hr-featuregridpanel',
                header: false,
                border: false,
                autoConfig: true,
                exportFormats: [],
                gridCellRenderers: Heron.options.gridCellRenderers,
                hropts: {
                    zoomOnRowDoubleClick: true,
                    zoomOnFeatureSelect: false,
                    zoomLevelPointSelect: 8,
                    zoomToDataExtent: true
                }
            }
        },
        {
            searchPanel: {
                xtype: 'hr_formsearchpanel',
                name: 'Buscar en Manzanas',
                header: false,
                protocol: new OpenLayers.Protocol.WFS({
                    version: "1.1.0",
                    url: "http://localhost:8080/geoserver/ows?",
                    srsName: "EPSG:3857",
                    featureType: "manzanas",
                    //featureNS: "http://census.gov"
                }),
                downloadFormats: [
                    {
                        name: 'CSV',
                        outputFormat: 'csv',
                        fileExt: '.csv'
                    },
                    {
                        name: 'GML (version 2.1.2)',
                        outputFormat: 'text/xml; subtype=gml/2.1.2',
                        fileExt: '.gml'
                    },
//                    {
//                        name: 'ESRI Shapefile (zipped)',
//                        outputFormat: 'SHAPE-ZIP',
//                        fileExt: '.zip'
//                    },
                    {
                        name: 'GeoJSON',
                        outputFormat: 'json',
                        fileExt: '.json'
                    }
                ],
                items: [
                    {
                        xtype: "textfield",
                        name: "manzana__like",
                        value: '207',
                        fieldLabel: "  manzana"
                    },
                    {
                        xtype: "label",
                        id: "helplabel",
                        html: 'Busca una manzana por número de manzana',
                        style: {
                            fontSize: '10px',
                            color: '#AAAAAA'
                        }
                    }
                ],
                hropts: {
                    onSearchCompleteZoom: 10,
                    autoWildCardAttach: true,
                    caseInsensitiveMatch: true,
                    logicalOperator: OpenLayers.Filter.Logical.AND
                }
            },
            resultPanel: {
                xtype: 'hr_featuregridpanel',
                id: 'hr-featuregridpanel',
                header: false,
                autoConfig: true,
                exportFormats: ['XLS', 'GMLv2', 'GeoJSON', 'WellKnownText', 'Shapefile'],
                gridCellRenderers: Heron.options.gridCellRenderers,
                hropts: {
                    zoomOnRowDoubleClick: true,
                    zoomOnFeatureSelect: false,
                    zoomLevelPointSelect: 8,
                    zoomToDataExtent: false
                }
            }
        }
    ]
};

// See ToolbarBuilder.js : each string item points to a definition
// in Heron.ToolbarBuilder.defs. Extra options and even an item create function
// can be passed here as well. By providing a "create" function your own toolbar
// item can be added.
// For menu's and other standard ExtJS Toolbar items, the "any" type can be
// used. There you need only pass the options, similar as in the function
// ExtJS Toolbar.add().
Heron.options.map.toolbar = [
    {type: "featureinfo", options: {
            popupWindow: {
                width: 360,
                height: 200,
                featureInfoPanel: {
                    displayPanels: ['Table'],
                    // Export to download file. Option values are 'CSV', 'XLS', default is no export (results in no export menu).
                    exportFormats: ['CSV', 'XLS', 'GMLv2', 'GeoJSON', 'WellKnownText', 'Shapefile'],
                    // Export to download file. Option values are 'CSV', 'XLS', default is no export (results in no export menu).
                    // exportFormats: ['CSV', 'XLS'],
                    maxFeatures: 10
                }
            }
        }},
    {type: "-"},
    {type: "pan"},
    {type: "zoomin"},
    {type: "zoomout"},
    {type: "zoomvisible"},
    //{type: "coordinatesearch", options: {onSearchCompleteZoom: 8}},
    {type: "-"},
    {type: "zoomprevious"},
    {type: "zoomnext"},
    {
        type: "namesearch",
        // Optional options, see NominatimSearchCombo.js, here we restrict search to The Netherlands.
        options: {
            url: "http://nominatim.openstreetmap.org/search?format=json",
            xtype: 'hr_geocodercombo',
            queryParam: "q",
            zoom: 18,
            emptyText: 'Buscar Dirección',
        }
    },
    {type: "-"},
    {
        // Instead of an internal "type", or using the "any" type
        // provide a create factory function.
        // MapPanel and options (see below) are always passed
        // From MapPanel we can access the OL Map to add Controls.
        create: function (mapPanel, options) {
            var map = mapPanel.getMap();

            var getFeatureControl = new OpenLayers.Control.WMSGetFeatureInfo({
                url: "http://localhost:8080/geoserver/IDEChaco/wms",
                drillDown: false, // Or true if you want drill down (see the docs)
                hover: false, // Or true if you want but bear in mind this could get chatty
                infoFormat: 'application/json',
                format: new OpenLayers.Format.JSON,
                //layers: queryableMapLayers,
                eventListeners: {
                    getfeatureinfo: function (event) {
                        //$('#myModal').modal('show');
                        var json = JSON.parse(event.text);
                        //console.log(json.features[0]);
                        var w = new Ext.Window({
    title : "iframe",
    width : 400,
    height: 400,
    items : [{         
            xtype : "tabpanel",
        items: [{
                title: 'Tab 1',
                html: 'hola'+ json.features[0].id,
                cls: 'card1'
            }, {
                title: 'Tab 2',
                html: '2' + json.features[1].id,
                cls: 'card2'
            }, {
                title: 'The Latest',
        html: '<iframe src =\"http://adc/app_dev.php/admin/capa/\"></iframe>',
        id: 'feedTab',
        iconCls: 'team',
        scroll : false
            }]
    }]
});
            w.show();
                        var popup = new GeoExt.Popup({
                            title: "My Popup",
                            location: map.getLonLatFromViewPortPx(event.xy),
                            map: map,
                            anchored: true,
                            width: 200,
                            contentUrl: "http://ironsummitmedia.github.io/startbootstrap-creative/",
                            collapsible: true
                        });
                        //myWin.show();
                    },
                    beforegetfeatureinfo: function (event) {
                        // Code here to set the content of queryableMapLayers
                        // The event object will contain xy of mouse click
                    },
                    nogetfeatureinfo: function (event) {
                        // Code here if no queryable layers are found
                    }
                }
            })

            var control = new OpenLayers.Control();
            OpenLayers.Util.extend(control, {
                draw: function () {
                    // this Handler.Box will intercept the shift-mousedown
                    // before Control.MouseDefault gets to see it
                    this.box = new OpenLayers.Handler.Box(control,
                            {"done": this.notice},
                    {keyMask: OpenLayers.Handler.MOD_SHIFT});
                    this.box.activate();
                },
                notice: function (bounds) {
                    var ll = map.getLonLatFromPixel(new OpenLayers.Pixel(bounds.left, bounds.bottom));
                    var ur = map.getLonLatFromPixel(new OpenLayers.Pixel(bounds.right, bounds.top));
                    alert(ll.lon.toFixed(4) + ", " +
                            ll.lat.toFixed(4) + ", " +
                            ur.lon.toFixed(4) + ", " +
                            ur.lat.toFixed(4));
                }
            });

            // Define Vector Layer once (see Ext namespace def above)
            // Global var for Vector drawing features
            Ext.namespace("MyToolbarItems.vectorLayer");
            MyToolbarItems.vectorLayer = new OpenLayers.Layer.Vector("MyDrawing");
            map.addLayers([MyToolbarItems.vectorLayer]);


            return new GeoExt.Action({
                text: "Cargar Datos",
                control: getFeatureControl,
                map: map,
                // button options
                toggleGroup: "toolGroup",
                enableToggle: true,
                pressed: false,
                allowDepress: true,
                tooltip: "Cargar Datos",
                // check item options
                //group: "draw"
            })
        }
    },
    {
        type: "searchcenter",
        // Options for SearchPanel window
        options: {
            show: false,
            searchWindow: {
                title: __('Multiple Searches'),
                x: 100,
                y: undefined,
                width: 360,
                height: 440,
                items: [
                    Heron.examples.searchPanelConfig
                ]
            }
        }
    },
    {
        //type: "any",
        options: {
            tooltip: __('Open new window in Google StreetView'),
            iconCls: "icon-streetview",
            enableToggle: true,
            pressed: false,
            id: "streetview",
            toggleGroup: "toolGroup",
            popupOptions: {
                title: __('Street View'),
                anchored: false,
                anchorPosition: 'auto',
                width: 300,
                height: 300,
                collapsible: true,
                draggable: true
            }
        },
        create: function (mapPanel, options) {

            var ClickControl = OpenLayers.Class(OpenLayers.Control, {
                defaults: {
                    pixelTolerance: 1,
                    stopSingle: true
                },
                initialize: function (options) {
                    this.handlerOptions = OpenLayers.Util.extend(
                            {}, this.defaults
                            );
                    OpenLayers.Control.prototype.initialize.apply(this, arguments);
                    this.handler = new OpenLayers.Handler.Click(
                            this, {click: this.trigger}, this.handlerOptions
                            );
                },
                trigger: function (event) {
                    openPopup(this.map.getLonLatFromViewPortPx(event.xy));
                }

            });

            var popup;

            function openPopup(location) {
                if (!location) {
                    location = mapPanel.map.getCenter();
                }
                if (popup && popup.anc) {
                    popup.close();
                }

                var popupOptions = {
                    location: location,
                    map: mapPanel,
                    items: [new gxp.GoogleStreetViewPanel()]
                };

                Ext.apply(popupOptions, options.popupOptions);
                popup = new GeoExt.Popup(popupOptions);
                popup.show();
            }

            options.control = new ClickControl({
                trigger: function (e) {
                    openPopup(this.map.getLonLatFromViewPortPx(e.xy));
                }
            });

            return new GeoExt.Action(options);
        }
    },
];